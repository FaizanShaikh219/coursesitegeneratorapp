package csg.style;
/**
 *
 * @author Faizan Shaikh
 */
import csg.CourseSiteGeneratorApp;
import csg.workspace.CourseSiteGeneratorWorkspace;
import csg.workspace.MeetingTimesWorkspace;
import csg.workspace.ScheduleWorkspace;
import csg.workspace.SiteWorkspace;
import csg.workspace.SyllabusWorkspace;
import csg.workspace.OfficeHoursWorkspace;
import djf.components.AppStyleComponent;
import javafx.scene.control.TableView;


public class CourseSiteGeneratorStyle extends AppStyleComponent {
    
    private CourseSiteGeneratorApp app;
    public static String CLASS_HEADER_PANE = "app_generic_button";
    public static String CLASS_PANE_TITLE = "app_generic_title";
    public static String CLASS_GENERIC_PANE = "app_generic_pane";
    public static String CLASS_GENERIC_BACKGROUND = "app_generic_background_pane";
    public static String CLASS_TA_TABLE = "app_ta_table";
    public static String CLASS_TA_TABLE_COLUMN = "app_ta_table_column";

    public CourseSiteGeneratorStyle(CourseSiteGeneratorApp app) {
        this.app = app;

        super.initStylesheet(app);
        app.getGUI().initFileToolbarStyle();
        initCourseWorkspaceStyle();
        
        initTAWorkspaceStyle();
        initScheduleWorkspaceStyle();
        initMeetingWorkspaceStyle();
        initSyllabusWorkspaceStyle();
    }

    private void initCourseWorkspaceStyle() {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        SiteWorkspace siteWorkspace = workspace.getCourseWorkspace();
        
        siteWorkspace.getStyleTitleLabel().getStyleClass().add(CLASS_PANE_TITLE);
        siteWorkspace.getInstructorLabel().getStyleClass().add(CLASS_PANE_TITLE);
        siteWorkspace.getBannerInfoTitleLabel().getStyleClass().add(CLASS_PANE_TITLE);
        siteWorkspace.getPageTitleLabel().getStyleClass().add(CLASS_PANE_TITLE);
        siteWorkspace.getBannerPane().getStyleClass().add(CLASS_GENERIC_PANE);
        siteWorkspace.getInsPane().getStyleClass().add(CLASS_GENERIC_PANE);
        siteWorkspace.getStyleInfoPane().getStyleClass().add(CLASS_GENERIC_PANE);
        siteWorkspace.getPageBox().getStyleClass().add(CLASS_GENERIC_PANE);
        siteWorkspace.getComponentPane().getStyleClass().add(CLASS_GENERIC_BACKGROUND);
        
    }
    
    
    private void initTAWorkspaceStyle() {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        OfficeHoursWorkspace taWorkspace = workspace.getTAWorkspace();
        
        taWorkspace.getComponentPane().getStyleClass().add(CLASS_GENERIC_BACKGROUND);
        
        taWorkspace.getTaTablePane().getStyleClass().add(CLASS_GENERIC_PANE);
        taWorkspace.getOfficeHoursPane().getStyleClass().add(CLASS_GENERIC_PANE);
        
        taWorkspace.getOfficeHoursTitleLabel().getStyleClass().add(CLASS_PANE_TITLE);
        taWorkspace.getTaTitleLabel().getStyleClass().add(CLASS_PANE_TITLE);
        
        TableView taTable = taWorkspace.getTaTable();
        taTable.getStyleClass().add(CLASS_TA_TABLE);
        

    }
    private void initScheduleWorkspaceStyle() {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        ScheduleWorkspace scheduleWorkspace = workspace.getScheduleWorkspace();
        scheduleWorkspace.getAddItemLabel().getStyleClass().add(CLASS_PANE_TITLE);
        scheduleWorkspace.getBoundariesLabel().getStyleClass().add(CLASS_PANE_TITLE);
        scheduleWorkspace.getScheduleItemsLabel().getStyleClass().add(CLASS_PANE_TITLE);
        scheduleWorkspace.getComponentPane().getStyleClass().add(CLASS_GENERIC_BACKGROUND);
        scheduleWorkspace.getBoundariesPane().getStyleClass().add(CLASS_GENERIC_PANE);
        scheduleWorkspace.getScheduleItemPane().getStyleClass().add(CLASS_GENERIC_PANE);
        //scheduleWorkspace.getSchedulePane().getStyleClass().add(CLASS_GENERIC_PANE);
        scheduleWorkspace.getScheduleBox().getStyleClass().add(CLASS_GENERIC_PANE);
    }

    private void initMeetingWorkspaceStyle() {
CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        MeetingTimesWorkspace recitationWorkspace = workspace.getMeetingTimesWorkspace();
        
        recitationWorkspace.getComponentPane().getStyleClass().add(CLASS_GENERIC_BACKGROUND);
        recitationWorkspace.getRecitationsPane().getStyleClass().add(CLASS_GENERIC_PANE);
        recitationWorkspace.getLecturesPane().getStyleClass().add(CLASS_GENERIC_PANE);
        recitationWorkspace.getLabsPane().getStyleClass().add(CLASS_GENERIC_PANE);
        //recitationWorkspace.getAddRecitationPane().getStyleClass().add(CLASS_GENERIC_PANE);
        //recitationWorkspace.getAddRecitationLabel().getStyleClass().add(CLASS_PANE_TITLE);
        recitationWorkspace.getRecitationsLabel().getStyleClass().add(CLASS_PANE_TITLE);
        recitationWorkspace.getLecturesLabel().getStyleClass().add(CLASS_PANE_TITLE); 
        recitationWorkspace.getLabsLabel().getStyleClass().add(CLASS_PANE_TITLE); 
        recitationWorkspace.getLecBox().getStyleClass().add(CLASS_GENERIC_PANE);
        recitationWorkspace.getRecBox().getStyleClass().add(CLASS_GENERIC_PANE);
        recitationWorkspace.getLabBox().getStyleClass().add(CLASS_GENERIC_PANE);
    }

    private void initSyllabusWorkspaceStyle() {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        SyllabusWorkspace syllabusWorkspace = workspace.getSyllabusWorkspace();    
        syllabusWorkspace.getComponentPane().getStyleClass().add(CLASS_GENERIC_BACKGROUND);
        syllabusWorkspace.getDesLabel().getStyleClass().add(CLASS_PANE_TITLE); 
        syllabusWorkspace.getDesPane().getStyleClass().add(CLASS_GENERIC_PANE);
        syllabusWorkspace.getTopLabel().getStyleClass().add(CLASS_PANE_TITLE); 
        syllabusWorkspace.getTopPane().getStyleClass().add(CLASS_GENERIC_PANE);
        syllabusWorkspace.getPreLabel().getStyleClass().add(CLASS_PANE_TITLE); 
        syllabusWorkspace.getPrePane().getStyleClass().add(CLASS_GENERIC_PANE);
        syllabusWorkspace.getOutLabel().getStyleClass().add(CLASS_PANE_TITLE); 
        syllabusWorkspace.getOutPane().getStyleClass().add(CLASS_GENERIC_PANE);
        syllabusWorkspace.getBookLabel().getStyleClass().add(CLASS_PANE_TITLE); 
        syllabusWorkspace.getBookPane().getStyleClass().add(CLASS_GENERIC_PANE);
        syllabusWorkspace.getGCLabel().getStyleClass().add(CLASS_PANE_TITLE); 
        syllabusWorkspace.getGCPane().getStyleClass().add(CLASS_GENERIC_PANE);
        syllabusWorkspace.getGNLabel().getStyleClass().add(CLASS_PANE_TITLE); 
        syllabusWorkspace.getGNPane().getStyleClass().add(CLASS_GENERIC_PANE);
        syllabusWorkspace.getADLabel().getStyleClass().add(CLASS_PANE_TITLE); 
        syllabusWorkspace.getADPane().getStyleClass().add(CLASS_GENERIC_PANE);
        syllabusWorkspace.getSALabel().getStyleClass().add(CLASS_PANE_TITLE); 
        syllabusWorkspace.getSAPane().getStyleClass().add(CLASS_GENERIC_PANE);
    }
    
}
