package csg.workspace.controller;
/**
 *
 * @author Faizan Shaikh
 */
import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import static csg.CourseSiteGeneratorProp.CONFIRM_TA_DELETE_MESSAGE;
import static csg.CourseSiteGeneratorProp.CONFIRM_TA_DELETE_TITLE;
import static csg.CourseSiteGeneratorProp.CONFIRM_TIME_CHANGE_MESSAGE;
import static csg.CourseSiteGeneratorProp.CONFIRM_TIME_CHANGE_TITLE;
import static csg.CourseSiteGeneratorProp.INVALID_TA_EMAIL_MESSAGE;
import static csg.CourseSiteGeneratorProp.INVALID_TA_EMAIL_TITLE;
import static csg.CourseSiteGeneratorProp.INVALID_TIME_MESSAGE;
import static csg.CourseSiteGeneratorProp.INVALID_TIME_TITLE;
import static csg.CourseSiteGeneratorProp.MISSING_TA_EMAIL_MESSAGE;
import static csg.CourseSiteGeneratorProp.MISSING_TA_EMAIL_TITLE;
import static csg.CourseSiteGeneratorProp.MISSING_TA_NAME_MESSAGE;
import static csg.CourseSiteGeneratorProp.MISSING_TA_NAME_TITLE;
import static csg.CourseSiteGeneratorProp.TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE;
import static csg.CourseSiteGeneratorProp.TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE;
import csg.data.CourseSiteGeneratorData;
import csg.data.OHTimeSlot;
import csg.data.TAData;
import csg.data.TAData.TimeSlotPosition;
import csg.data.TeachingAssistant;
import csg.workspace.CourseSiteGeneratorWorkspace;
import csg.workspace.OfficeHoursWorkspace;
import csg.workspace.transactions.TATransactions;
import djf.ui.MsgDialog;
import djf.ui.ChooseDialog;
import java.time.DayOfWeek;
import java.util.Optional;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;
import properties_manager.PropertiesManager;


public class TAController {
    
    private CourseSiteGeneratorApp app;
    
    public TAController(CourseSiteGeneratorApp app) {
        this.app = app;   
    }
    
    public void handleSelectTA() {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        OfficeHoursWorkspace taWorkspace = workspace.getTAWorkspace();

        TeachingAssistant ta = (TeachingAssistant) taWorkspace.getTaTable().getSelectionModel().getSelectedItem();
                
        if (ta != null) {
            taWorkspace.getAddTATextField().setText(ta.getName());
            taWorkspace.getAddEmailTextField().setText(ta.getEmail());
            taWorkspace.getRemoveTAButton().setDisable(false);
        }
    }
    
    
    public void handleOfficeHoursToggle() {
        
        
        //Get the table
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        OfficeHoursWorkspace taWorkspace = workspace.getTAWorkspace();
        
        
        if (taWorkspace.isTableLocked()) {
            return;
        }
        
        TableView<OHTimeSlot> officeHoursTable = taWorkspace.getOfficeHoursTable();
        
        TableView<TeachingAssistant> taTable = taWorkspace.getTaTable();
        TeachingAssistant ta = taTable.getSelectionModel().getSelectedItem();
        
        if (ta == null) {
            return;
        }
 
        String taName = ta.getName();
         
        //get selected cell position
        TablePosition position = officeHoursTable.getFocusModel().getFocusedCell();
       
        //first two columns are start time and end time, no need to select them.
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();  
                 
        if (position.getTableColumn() == null)
            return;
        
        String columnHeader = position.getTableColumn().getText();

        if (!props.getPropertyOptionsList(CourseSiteGeneratorProp.DAYS_OF_WEEK).contains(columnHeader)) {
            return;
        }

        CourseSiteGeneratorData data = (CourseSiteGeneratorData)app.getDataComponent();
        TAData taData = data.getTAData();
        
        TimeSlotPosition timeSlot = taData.getTimeSlotPosition(position.getRow(), position.getTableColumn().getText());
        TATransactions.ToggleOfficeHoursTransaction transaction = new TATransactions.ToggleOfficeHoursTransaction(app, timeSlot, ta);
        workspace.getTransactionProcessor().addTransaction(transaction);      
       
        officeHoursTable.refresh();
        
        Platform.runLater(() -> officeHoursTable.getSelectionModel().clearSelection());   
        
    }
    
    /**
     * What to do when user attempts to change end hour boundary.
     */
    public void handleOfficeHourEndTimeChange() {
        CourseSiteGeneratorData data = (CourseSiteGeneratorData)app.getDataComponent();
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        OfficeHoursWorkspace taWorkspace = workspace.getTAWorkspace();
        TAData taData = data.getTAData();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ToggleGroup tg=taWorkspace.getToggleGroup();
        String sel=((RadioButton)tg.getSelectedToggle()).getText();
        
        int oldStartHour = taData.getStartHour();
        int oldEndHour = taData.getEndHour();
        
        if (taWorkspace.getEndTime().getValue() == null) return;

        int newStartHour = oldStartHour;
        int newEndHour = OHTimeSlot.getTimeValue((String)taWorkspace.getEndTime().getValue())[0];
        
        if (oldEndHour == newEndHour)
           return;
        
        //if user trys to set an impossible time
        if (newEndHour <= oldStartHour) {
            MsgDialog dialog = MsgDialog.getSingleton();
	    dialog.show(props.getProperty(INVALID_TIME_TITLE), props.getProperty(INVALID_TIME_MESSAGE));  
            taWorkspace.getEndTime().setValue(OHTimeSlot.formatTime(oldEndHour, 00));
            return;
            
        }
        //confirm the change
        ChooseDialog yesNoDialog = ChooseDialog.getSingleton();
        yesNoDialog.show(props.getProperty(CONFIRM_TIME_CHANGE_TITLE), props.getProperty(CONFIRM_TIME_CHANGE_MESSAGE));
        
        String selection = yesNoDialog.getSelection();
        if (selection.equals(ChooseDialog.YES)) {
            
           TATransactions.ChangeOpenOfficeHourTransaction transaction 
                   = new TATransactions.ChangeOpenOfficeHourTransaction(app, taData.getOfficeHoursState(), newStartHour, newEndHour, oldStartHour, oldEndHour,sel);
           
            workspace.getTransactionProcessor().addTransaction(transaction);
        }
        else {     
            taWorkspace.getStartTime().setValue(OHTimeSlot.formatTime(oldEndHour, 00));
        }
    }
    
    /**
     * What to do when user attempts to change start hour boundary.
     */
    public void handleOfficeHourStartTimeChange() {
        CourseSiteGeneratorData data = (CourseSiteGeneratorData)app.getDataComponent();
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        OfficeHoursWorkspace taWorkspace = workspace.getTAWorkspace();
        TAData taData = data.getTAData();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ToggleGroup tg=taWorkspace.getToggleGroup();
        String sel=((RadioButton)tg.getSelectedToggle()).getText();
        int oldStartHour = taData.getStartHour();
        int oldEndHour = taData.getEndHour();
        
        if (taWorkspace.getStartTime().getValue() == null) return;
        
        int newStartHour = OHTimeSlot.getTimeValue((String)taWorkspace.getStartTime().getValue())[0];
        int newEndHour = oldEndHour;
        
        if (oldStartHour == newStartHour)
           return;
        
        //if user trys to set an impossible time
        if (newStartHour >= oldEndHour) {
            MsgDialog dialog = MsgDialog.getSingleton();
	    dialog.show(props.getProperty(INVALID_TIME_TITLE), props.getProperty(INVALID_TIME_MESSAGE));  
            taWorkspace.getStartTime().setValue(OHTimeSlot.formatTime(oldStartHour, 00));
            return;
            
        }
        
        ChooseDialog yesNoDialog = ChooseDialog.getSingleton();
        yesNoDialog.show(props.getProperty(CONFIRM_TIME_CHANGE_TITLE), props.getProperty(CONFIRM_TIME_CHANGE_MESSAGE));
        
        String selection = yesNoDialog.getSelection();
        if (selection.equals(ChooseDialog.YES)) {
            
           TATransactions.ChangeOpenOfficeHourTransaction transaction 
                   = new TATransactions.ChangeOpenOfficeHourTransaction(app, taData.getOfficeHoursState(), newStartHour, newEndHour, oldStartHour, oldEndHour,sel);
            workspace.getTransactionProcessor().addTransaction(transaction);

        }
        else {     
            
            taWorkspace.getStartTime().setValue(OHTimeSlot.formatTime(oldStartHour, 00));
    
        }
    }
    
    /**
     * What to do when user selects clear TA Button.
     */
    public void handleClearTA() {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        OfficeHoursWorkspace taWorkspace = workspace.getTAWorkspace();
        
        taWorkspace.getTaTable().getSelectionModel().clearSelection();
        taWorkspace.getAddEmailTextField().clear();
        taWorkspace.getAddTATextField().clear();
        taWorkspace.getRemoveTAButton().setDisable(true);
    }
   
    /**
     * What to do when user attempts to remove TA.
     */
    public void handleRemoveTA() {

        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        //confirm the deletion
        ChooseDialog yesNoDialog = ChooseDialog.getSingleton();
        yesNoDialog.show(props.getProperty(CONFIRM_TA_DELETE_TITLE), props.getProperty(CONFIRM_TA_DELETE_MESSAGE));
        
        String selection = yesNoDialog.getSelection();
        if (!selection.equals(ChooseDialog.YES)) {
            return;
        }
        
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        OfficeHoursWorkspace taWorkspace = workspace.getTAWorkspace();
        TeachingAssistant ta = (TeachingAssistant) taWorkspace.getTaTable().getSelectionModel().getSelectedItem();
        
        if (ta != null) {
            
            TATransactions.RemoveTATransaction transaction = 
                new TATransactions.RemoveTATransaction(app, ta);
            workspace.getTransactionProcessor().addTransaction(transaction);
            handleClearTA();
        }
    }
    
    /**
     * What to do when user attempts to edit TA.
     */
    //public void handleEditTA() {
        /*CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        TAWorkspace taWorkspace = workspace.getTAWorkspace();
        TeachingAssistant ta = (TeachingAssistant) taWorkspace.getTaTable().getSelectionModel().getSelectedItem();
        
        String oldName = ta.getName();
        
        TextField nameTextField = taWorkspace.getAddTATextField();
        TextField emailTextField = taWorkspace.getAddEmailTextField();
        String newName = nameTextField.getText();
        String newEmail = emailTextField.getText();
        
        
        CourseSiteGeneratorData data = (CourseSiteGeneratorData)app.getDataComponent();
        TAData taData = data.getTAData();
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        Pattern pattern = Pattern.compile("[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        
       
        if (newEmail.isEmpty()) {
            
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));            
        }
        else if (newEmail.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	   dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));                        
        }
        else if (!pattern.matcher(newEmail).matches()) {
 	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(INVALID_TA_EMAIL_TITLE), props.getProperty(INVALID_TA_EMAIL_MESSAGE));            
        }
        else if (taData.containsTA(newName) && !oldName.equals(newName)) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));                                    
        }
        else {
            System.out.println(ta.getName()+"\t"+ta.getEmail()+"\t"+ta.getTimeSlots()+"\t"+ta.getType());
            TATransactions.EditTATransaction transaction = new TATransactions.EditTATransaction(app, newName, newEmail, ta.getName(), ta.getEmail());
            workspace.getTransactionProcessor().addTransaction(transaction);
            System.out.println(ta.getName()+"\t"+ta.getEmail()+"\t"+ta.getTimeSlots()+"\t"+ta.getType());
            taWorkspace.getRemoveTAButton().setDisable(true);
            taWorkspace.getTaTable().getSelectionModel().clearSelection();
           
            nameTextField.setText("");
            emailTextField.setText("");
            
            //Send focus back to first textfield
            nameTextField.requestFocus();
            taWorkspace.getTaTable().refresh();
        }  */
    //}

    /**
     * What to do when user attempts to add TA.
     */
    public void handleAddTA() {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        OfficeHoursWorkspace taWorkspace = workspace.getTAWorkspace();
        
        TextField nameTextField = taWorkspace.getAddTATextField();
        TextField emailTextField = taWorkspace.getAddEmailTextField();
        ToggleGroup tg=taWorkspace.getToggleGroup();
        String sel=((RadioButton)tg.getSelectedToggle()).getText();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        
        CourseSiteGeneratorData data = (CourseSiteGeneratorData)app.getDataComponent();
        TAData taData = data.getTAData();
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        Pattern pattern = Pattern.compile("[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        
        if (name.isEmpty()) {
	   Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Error");
                alert.setHeaderText("Add TA Error");
                alert.setContentText("Name Field Empty");

                alert.showAndWait();
                }
        else if (email.isEmpty()) {
	    Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Error");
                alert.setHeaderText("Add TA Error");
                alert.setContentText("Email required");

                alert.showAndWait();
                }
        //check email formatting.
        else if (!pattern.matcher(email).matches()) {
 	    Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Error");
                alert.setHeaderText("Add TA Error");
                alert.setContentText("Illegal email");

                alert.showAndWait();
                }
        else if (taData.containsTA(name)||!taData.isLegalNewEmail(email)) {
	    Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Error");
                alert.setHeaderText("Add TA Error");
                alert.setContentText("TA already exists");

                alert.showAndWait();
                }
        //Add the TA
        else {
            TeachingAssistant ta=new TeachingAssistant(name,email,sel);
            TATransactions.AddTATransaction transaction = new TATransactions.AddTATransaction(app, ta);
     
            workspace.getTransactionProcessor().addTransaction(transaction);
           
            nameTextField.setText("");
            emailTextField.setText("");
            
            //Send focus back to first textfield
            nameTextField.requestFocus();
            
        }
    }

    public void highlight() {
         CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        OfficeHoursWorkspace taWorkspace = workspace.getTAWorkspace();
        
        
        if (taWorkspace.isTableLocked()) {
            return;
        }
        
        TableView<OHTimeSlot> officeHoursTable = taWorkspace.getOfficeHoursTable();
        
        TableView<TeachingAssistant> taTable = taWorkspace.getTaTable();
        TeachingAssistant ta = taTable.getSelectionModel().getSelectedItem();
        
        if (ta == null) {
            return;
        }
        //OfficeHoursData data = (OfficeHoursData) app.getDataComponent();
        //AppGUIModule gui = app.getGUIModule();
        //TableView<TimeSlot>officeHoursTableView = (TableView)gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        //ObservableList<TimeSlot> searchRow=officeHoursTableView.getSelectionModel().getSelectedItems();
        
        //TableView<TeachingAssistant> taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        //TeachingAssistantPrototype ta =(TeachingAssistantPrototype) taTableView.getSelectionModel().getSelectedItem();
        
        for(int i=2;i<officeHoursTable.getColumns().size();i++){
            //System.out.println("1");
            ((TableColumn)(officeHoursTable.getColumns().get(i))).setCellFactory(col->{
                return new TableCell<DayOfWeek,String>(){
                    @Override
                    protected void updateItem(String tan,boolean em){
                        //System.out.println(tan+"\n");
                        //super.updateItem(tan, em);
                        if(tan==null){
                            
                        }
                        else{
                            if(tan.contains(ta.getName())){
                                //System.out.println("3");
                                
                                setStyle("-fx-background-color:salmon");
                                setText(tan);
                                //setTextFill(Color.RED);
                            }
                            else{
                                setText(tan);
                            }
                        }
                    }
                };
                
            });
        }
        
        //data.highlightcell(ta);
        officeHoursTable.refresh();
        //System.out.println(searchRow.size());
        //data.highlightcell(ta);    
    }

    public void openDialogBox(String text) {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        OfficeHoursWorkspace taWorkspace = workspace.getTAWorkspace();
        TableView<TeachingAssistant> taTable = taWorkspace.getTaTable();
        TeachingAssistant ta = taTable.getSelectionModel().getSelectedItem();
        
        taTable.setStyle(" -fx-selection-bar-non-focused: salmon;");
        //AppNodesBuilder ohBuilder = app.getGUI().getNodesBuilder();
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("");
        dialog.setHeaderText("Edit TA");

// Set the icon (must be included in the project).
        //dialog.setGraphic(new ImageView(this.getClass().getResource("login.png").toString()));

// Set the button types.
        ButtonType loginButtonType = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

// Create the username and password labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField name = new TextField();
        name.setText(ta.getName());
        TextField email = new TextField();
        email.setText(ta.getEmail());
        String tempname=ta.getName();
        String tempemail=ta.getEmail();
        ToggleGroup tg = new ToggleGroup();
        RadioButton u= new RadioButton();
        u.setText("Undergraduate");
        RadioButton g= new RadioButton();
        g.setText("Graduate");
        u.setToggleGroup(tg);
        g.setToggleGroup(tg);
        //tg.selectToggle(ta.getType());
        if(ta.getType().equals("Undergraduate"))
            u.setSelected(true);
        else
            g.setSelected(true);
        grid.add(new Label("Name:"), 0, 0);
        grid.add(name, 1, 0);
        grid.add(new Label("Email:"), 0, 1);
        grid.add(email, 1, 1);
        grid.add(new Label("Type:"), 0, 2);
        grid.add(u, 1, 2);
        grid.add(g, 2, 2);
// Enable/Disable login button depending on whether a username was entered.
        Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        //loginButton.setDisable(true);
        
// Do some validation (using the Java 8 lambda syntax).
        name.textProperty().addListener((observable, oldValue, newValue) -> {
            //this.initFoolproofDesign("c");
            //this.processTypeTA("n", name.getText());
            loginButton.setDisable(newValue.trim().isEmpty());
        });
        
        dialog.getDialogPane().setContent(grid);

// Request focus on the username field by default.
        Platform.runLater(() -> name.requestFocus());

// Convert the result to a username-password-pair when the login button is clicked.
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return new Pair<>(name.getText(), email.getText());
            }
           return null;
        });

        Optional<Pair<String, String>> result = dialog.showAndWait();

        result.ifPresent(e -> {
            TAData data = ((CourseSiteGeneratorData) app.getDataComponent()).getTAData();
            //AppGUI gui = app.getGUI();
            
            if(!e.getKey().contains(" ")){
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Error");
                alert.setHeaderText("Edit TA Error");
                alert.setContentText("First and Last name required");

                alert.showAndWait();
                return;
                     
            }
            if(e.getKey().equals(ta.getName())){
                if(e.getValue().equals(ta.getEmail())){
                    String type=((RadioButton)tg.getSelectedToggle()).getText();
TATransactions.EditTATransaction eta=new TATransactions.EditTATransaction(app, e.getKey(), e.getValue(), ta, type,ta.getName(),ta.getEmail(),ta.getType());                    
workspace.getTransactionProcessor().addTransaction(eta);
                    //ta.setType(type);
                    workspace.getTAWorkspace().getTaTable().refresh();
                    workspace.getTAWorkspace().getOfficeHoursTable().refresh();
                    
                    return;
                }
                if(data.isLegalNewEmail(e.getValue())){
                    String type=((RadioButton)tg.getSelectedToggle()).getText();
                    //ta.setType(type);
                    //ta.setEmail(e.getValue());
TATransactions.EditTATransaction eta=new TATransactions.EditTATransaction(app, e.getKey(), e.getValue(), ta, type,ta.getName(),ta.getEmail(),ta.getType());                    
workspace.getTransactionProcessor().addTransaction(eta);
                    workspace.getTAWorkspace().getTaTable().refresh();
                    workspace.getTAWorkspace().getOfficeHoursTable().refresh();
                    return;
                }
                else{
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Error");
                    alert.setHeaderText("Edit TA Error");
                    alert.setContentText("Invalid Email");

                    alert.showAndWait();
                    return;
                }
            }
            if(e.getValue().equals(ta.getEmail())){
                if(e.getKey().equals(ta.getName())){
                    String type=((RadioButton)tg.getSelectedToggle()).getText();
                    //ta.setType(type);
                    TATransactions.EditTATransaction eta=new TATransactions.EditTATransaction(app, e.getKey(), e.getValue(), ta, type,ta.getName(),ta.getEmail(),ta.getType());
                    workspace.getTransactionProcessor().addTransaction(eta);
                    workspace.getTAWorkspace().getTaTable().refresh();
                    workspace.getTAWorkspace().getOfficeHoursTable().refresh();
                    return;
                }
                if(data.isLegalNewName(e.getKey())){
                    String type=((RadioButton)tg.getSelectedToggle()).getText();
                    //ta.setType(type);
                    //ta.setName(e.getKey());
                    //data.changeOHNames(e.getKey(), ta);
TATransactions.EditTATransaction eta=new TATransactions.EditTATransaction(app, e.getKey(), e.getValue(), ta, type,ta.getName(),ta.getEmail(),ta.getType());                    workspace.getTransactionProcessor().addTransaction(eta);
                    workspace.getTAWorkspace().getTaTable().refresh();
                    workspace.getTAWorkspace().getOfficeHoursTable().refresh();
                    workspace.getTAWorkspace().getOfficeHoursTable().refresh();
                    return;
                }
                else{
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Error");
                    alert.setHeaderText("Edit TA Error");
                    alert.setContentText("Invalid Name");

                    alert.showAndWait();
                    return;
                }
            }
            if(!e.getKey().equals(ta.getName())&&!e.getValue().equals(ta.getEmail())){
                if(data.isLegalNewTA(e.getKey(), e.getValue())){
                    String type=((RadioButton)tg.getSelectedToggle()).getText();
                    //ta.setType(type);
                    //ta.setEmail(e.getValue());
                    //ta.setName(e.getKey());
                    //data.changeOHNames(e.getKey(), ta);
TATransactions.EditTATransaction eta=new TATransactions.EditTATransaction(app, e.getKey(), e.getValue(), ta, type,ta.getName(),ta.getEmail(),ta.getType());                    workspace.getTransactionProcessor().addTransaction(eta);
                    workspace.getTAWorkspace().getTaTable().refresh();
                    workspace.getTAWorkspace().getOfficeHoursTable().refresh();
                    return;
                }
                else{
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Error");
                    alert.setHeaderText("Edit TA Error");
                    alert.setContentText("Invalid TA");

                    alert.showAndWait();
                    return;
                }
            }
            
            /*if(e.getKey().contains(" ")&&data.isLegalNewTA(e.getKey(), e.getValue())){
                String type=((RadioButton)tg.getSelectedToggle()).getText();
                System.out.println(ta.getName()+"\t"+ta.getEmail()+"\t"+ta.getSlots()+"\t"+ta.getType());
                ta.setType(type);
                System.out.println(ta.getType());
               
                 data.changeOHNames(e.getKey(), ta);
                  ta.setEmail(e.getValue());
                ta.setName(e.getKey());
//data.editTA(e.getKey(), e.getValue(),ta, type);
                //TATransactions.EditTATransaction et=new TATransactions.EditTATransaction(app,e.getKey(),e.getValue(),ta,type);
                //workspace.getTransactionProcessor().addTransaction(et); 
                System.out.println(ta.getName()+"\t"+ta.getEmail()+"\t"+ta.getSlots()+"\t"+ta.getType());
                //workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
            //workspace.getRecitationWorkspace().getRecitationTable().refresh();
                workspace.getTAWorkspace().getTaTable().refresh();
            }
            else if(e.getKey().equals(ta.getName())&&e.getValue().equals(ta.getEmail())){
                String type=((RadioButton)tg.getSelectedToggle()).getText();
                System.out.println(ta.getName()+"\t"+ta.getEmail()+"\t"+ta.getSlots()+"\t"+ta.getType());
                ta.setType(type);
                System.out.println(ta.getType());
               
                 data.changeOHNames(e.getKey(), ta);
                  ta.setEmail(e.getValue());
                ta.setName(e.getKey());
//data.editTA(e.getKey(), e.getValue(),ta, type);
                //TATransactions.EditTATransaction et=new TATransactions.EditTATransaction(app,e.getKey(),e.getValue(),ta,type);
                //workspace.getTransactionProcessor().addTransaction(et); 
                System.out.println(ta.getName()+"\t"+ta.getEmail()+"\t"+ta.getSlots()+"\t"+ta.getType());
                //workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
            //workspace.getRecitationWorkspace().getRecitationTable().refresh();
                workspace.getTAWorkspace().getTaTable().refresh();
            }
            else if(e.getKey().equals(ta.getName())&&!e.getValue().equals(ta.getEmail())&&!data.isLegalNewEmail(e.getValue())){
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Error");
                alert.setHeaderText("Edit TA Error");
                alert.setContentText("Invalid Email");

                alert.showAndWait();
            }
            else if(!e.getKey().equals(ta.getName())&&e.getValue().equals(ta.getEmail())&&!data.isLegalNewName(e.getKey())){
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Error");
                alert.setHeaderText("Edit TA Error");
                alert.setContentText("Invalid Name");

                alert.showAndWait();
            }
            
            else{
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Error");
                alert.setHeaderText("Edit TA Error");
                alert.setContentText("Invalid Information");

                alert.showAndWait();
            }*/
            
            
        });    }

    public void processTypeTA(String t,String prop) {
        TAData data = ((CourseSiteGeneratorData) app.getDataComponent()).getTAData();
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        OfficeHoursWorkspace taWorkspace = workspace.getTAWorkspace();
        taWorkspace.getAddTAButton().setDisable(true);
        if(t.equals("n")){
            if(!data.isLegalNewName(prop)){
                taWorkspace.getAddTATextField().setStyle("-fx-text-fill: red;");
                
            }
            else{
                taWorkspace.getAddTATextField().setStyle("-fx-text-fill: black;");
                
            }
        }
        else{
            if(!data.isLegalNewEmail(prop)){
                taWorkspace.getAddEmailTextField().setStyle("-fx-text-fill: red;");
                
            }
            else{
                taWorkspace.getAddEmailTextField().setStyle("-fx-text-fill: black;");
                
            }
        }
        if(data.isLegalNewTA(taWorkspace.getAddTATextField().getText(),taWorkspace.getAddEmailTextField().getText())){
            taWorkspace.getAddTAButton().setDisable(false);
        }
        
    }

    public void processSelectAllTAs() {
        TAData data = ((CourseSiteGeneratorData) app.getDataComponent()).getTAData();
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        
        
        data.selectTAs("All");
        OfficeHoursWorkspace ohw=workspace.getTAWorkspace();
        ohw.getOfficeHoursTable().refresh();
        
    }

    public void processSelectGradTAs() {
        TAData data = ((CourseSiteGeneratorData) app.getDataComponent()).getTAData();
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        data.selectTAs("Graduate");
        OfficeHoursWorkspace ohw=workspace.getTAWorkspace();
        ohw.getOfficeHoursTable().refresh();
        
    }

    public void processSelectUndergradTAs() {
        TAData data = ((CourseSiteGeneratorData) app.getDataComponent()).getTAData();
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        data.selectTAs("Undergraduate");
        OfficeHoursWorkspace ohw=workspace.getTAWorkspace();
        ohw.getOfficeHoursTable().refresh();
        
    }
}
