package csg.workspace.controller;

import csg.CourseSiteGeneratorApp;
import csg.data.CourseSiteGeneratorData;
import csg.data.SyllabusData;
import csg.workspace.CourseSiteGeneratorWorkspace;
import csg.workspace.SyllabusWorkspace;

/**
 *
 * @author Faizan Shaikh
 */
public class SyllabusController {
    private CourseSiteGeneratorApp app;
    
    public SyllabusController(CourseSiteGeneratorApp app) {
        this.app = app;   
    }

    public void handleDes() {
        SyllabusWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getSyllabusWorkspace();
        SyllabusData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSyllabusData();
        String oh = workspace.getDesText();
        
        if (oh != null) {
            data.setDes(oh);
        }
    }

    public void handleTop() {
SyllabusWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getSyllabusWorkspace();
        SyllabusData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSyllabusData();
        String oh = workspace.getTopText();
        
        if (oh != null) {
            data.setTop(oh);
        }    }

    public void handlePre() {
SyllabusWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getSyllabusWorkspace();
        SyllabusData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSyllabusData();
        String oh = workspace.getPreText();
        
        if (oh != null) {
            data.setPre(oh);
        }    }

    public void handleOut() {
SyllabusWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getSyllabusWorkspace();
        SyllabusData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSyllabusData();
        String oh = workspace.getOutText();
        
        if (oh != null) {
            data.setOut(oh);
        }    }

    public void handleText() {
SyllabusWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getSyllabusWorkspace();
        SyllabusData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSyllabusData();
        String oh = workspace.getTextText();
        
        if (oh != null) {
            data.setText(oh);
        }    }

    public void handleGC() {
SyllabusWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getSyllabusWorkspace();
        SyllabusData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSyllabusData();
        String oh = workspace.getGCText();
        
        if (oh != null) {
            data.setGC(oh);
        }    }

    public void handleGN() {
SyllabusWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getSyllabusWorkspace();
        SyllabusData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSyllabusData();
        String oh = workspace.getGNText();
        
        if (oh != null) {
            data.setGN(oh);
        }    }

    public void handleAD() {
SyllabusWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getSyllabusWorkspace();
        SyllabusData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSyllabusData();
        String oh = workspace.getADText();
        
        if (oh != null) {
            data.setAD(oh);
        }    }

    public void handleSA() {
SyllabusWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getSyllabusWorkspace();
        SyllabusData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSyllabusData();
        String oh = workspace.getSAText();
        
        if (oh != null) {
            data.setSA(oh);
        }    
    }
    
}
