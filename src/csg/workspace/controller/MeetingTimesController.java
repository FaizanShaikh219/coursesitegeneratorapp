package csg.workspace.controller;
/**
 *
 * @author Faizan Shaikh
 */
import csg.CourseSiteGeneratorApp;
import csg.data.CourseSiteGeneratorData;
import csg.data.Labs;
import csg.data.Lectures;
import csg.data.MeetingTimesData;
import csg.data.Recitations;
import csg.workspace.CourseSiteGeneratorWorkspace;
import csg.workspace.MeetingTimesWorkspace;
import csg.workspace.transactions.LabRemoveTransaction;
import csg.workspace.transactions.LecTransaction;
import csg.workspace.transactions.LabTransaction;
import csg.workspace.transactions.LecRemoveTransaction;
import csg.workspace.transactions.MTEditTransactions;
import csg.workspace.transactions.RecRemoveTransaction;
import csg.workspace.transactions.RecTransaction;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;



public class MeetingTimesController {
    private CourseSiteGeneratorApp app;
    public MeetingTimesController(CourseSiteGeneratorApp app) {
        this.app=app;
    }

    public void handleAddLec() {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        MeetingTimesWorkspace mtWorkspace = workspace.getMeetingTimesWorkspace();
        
        CourseSiteGeneratorData data = (CourseSiteGeneratorData)app.getDataComponent();
        MeetingTimesData mtData = data.getMeetingTimesData();
        
        Lectures lec=new Lectures();
        LecTransaction lecTransaction = new LecTransaction(app,lec);
        workspace.getTransactionProcessor().addTransaction(lecTransaction);
    }

    public void handleAddLab() {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        MeetingTimesWorkspace mtWorkspace = workspace.getMeetingTimesWorkspace();
        
        CourseSiteGeneratorData data = (CourseSiteGeneratorData)app.getDataComponent();
        MeetingTimesData mtData = data.getMeetingTimesData();
        
        Labs lab=new Labs();
        LabTransaction labTransaction = new LabTransaction(app,lab);
        workspace.getTransactionProcessor().addTransaction(labTransaction);
    }

    public void handleAddRec() {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        MeetingTimesWorkspace mtWorkspace = workspace.getMeetingTimesWorkspace();
        
        CourseSiteGeneratorData data = (CourseSiteGeneratorData)app.getDataComponent();
        MeetingTimesData mtData = data.getMeetingTimesData();
        
        Recitations rec=new Recitations();
        RecTransaction recTransaction = new RecTransaction(app,rec);
        workspace.getTransactionProcessor().addTransaction(recTransaction);
    }

    public void handleRemoveLec() {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        MeetingTimesWorkspace mtWorkspace = workspace.getMeetingTimesWorkspace();
        TableView<Lectures> lecTable = mtWorkspace.getLectureTable();
        Lectures lec=lecTable.getSelectionModel().getSelectedItem();
        if (lec == null) {
            return;
        }
        LecRemoveTransaction lecremTransaction = new LecRemoveTransaction(app,lec);
        workspace.getTransactionProcessor().addTransaction(lecremTransaction);
        
    }

    public void handleRemoveLab() {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        MeetingTimesWorkspace mtWorkspace = workspace.getMeetingTimesWorkspace();
        TableView<Labs> labTable = mtWorkspace.getLabTable();
        Labs lab=labTable.getSelectionModel().getSelectedItem();
        if (lab == null) {
            return;
        }
        LabRemoveTransaction labremTransaction = new LabRemoveTransaction(app,lab);
        workspace.getTransactionProcessor().addTransaction(labremTransaction);
    }

    public void handleRemoveRec() {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        MeetingTimesWorkspace mtWorkspace = workspace.getMeetingTimesWorkspace();
        TableView<Recitations> recTable = mtWorkspace.getRecitationTable();
        Recitations rec=recTable.getSelectionModel().getSelectedItem();
        if (rec == null) {
            return;
        }
        RecRemoveTransaction recremTransaction = new RecRemoveTransaction(app,rec);
        workspace.getTransactionProcessor().addTransaction(recremTransaction);
    }

    

    public void editLec(TableColumn.CellEditEvent<Lectures, String> t,String type) {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        MeetingTimesWorkspace mtWorkspace = workspace.getMeetingTimesWorkspace();
        MTEditTransactions.EditLecTransaction editLecTransaction=new MTEditTransactions.EditLecTransaction(app,t,type);
        workspace.getTransactionProcessor().addTransaction(editLecTransaction);
    }

    public void editLab(TableColumn.CellEditEvent<Labs, String> t, String type) {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        MTEditTransactions.EditLabTransaction editLabTransaction=new MTEditTransactions.EditLabTransaction(app,t,type);
        workspace.getTransactionProcessor().addTransaction(editLabTransaction);
    }
    public void editRec(TableColumn.CellEditEvent<Recitations, String> t, String type) {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        MTEditTransactions.EditRecTransaction editRecTransaction=new MTEditTransactions.EditRecTransaction(app,t,type);
        workspace.getTransactionProcessor().addTransaction(editRecTransaction);
    }
    
}
