package csg.workspace.controller;

import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import static csg.CourseSiteGeneratorProp.MISSING_SCHEDULE_CRITERIA_MESSAGE;
import static csg.CourseSiteGeneratorProp.MISSING_SCHEDULE_CRITERIA_TITLE;
import static csg.CourseSiteGeneratorProp.MISSING_SCHEDULE_DATE_MESSAGE;
import static csg.CourseSiteGeneratorProp.MISSING_SCHEDULE_DATE_TITLE;
import static csg.CourseSiteGeneratorProp.MISSING_SCHEDULE_ITEMTITLE_MESSAGE;
import static csg.CourseSiteGeneratorProp.MISSING_SCHEDULE_ITEMTITLE_TITLE;
import static csg.CourseSiteGeneratorProp.MISSING_SCHEDULE_LINK_MESSAGE;
import static csg.CourseSiteGeneratorProp.MISSING_SCHEDULE_LINK_TITLE;
import static csg.CourseSiteGeneratorProp.MISSING_SCHEDULE_TIME_MESSAGE;
import static csg.CourseSiteGeneratorProp.MISSING_SCHEDULE_TIME_TITLE;
import static csg.CourseSiteGeneratorProp.MISSING_SCHEDULE_TOPIC_MESSAGE;
import static csg.CourseSiteGeneratorProp.MISSING_SCHEDULE_TOPIC_TITLE;
import static csg.CourseSiteGeneratorProp.MISSING_SCHEDULE_TYPE_MESSAGE;
import static csg.CourseSiteGeneratorProp.MISSING_SCHEDULE_TYPE_TITLE;
import static csg.CourseSiteGeneratorProp.SCHEDULE_ITEM_NOT_UNIQUE_MESSAGE;
import static csg.CourseSiteGeneratorProp.SCHEDULE_ITEM_NOT_UNIQUE_TITLE;
import static csg.CourseSiteGeneratorProp.SCHEDULE_OUTSIDE_OF_RANGE_MESSAGE;
import static csg.CourseSiteGeneratorProp.SCHEDULE_OUTSIDE_OF_RANGE_TITLE;
import csg.data.CourseSiteGeneratorData;
import csg.data.ScheduleData;
import csg.data.ScheduleItem;
import csg.workspace.CourseSiteGeneratorWorkspace;
import csg.workspace.ScheduleWorkspace;
import csg.workspace.transactions.ScheduleTransactions;
import java.time.LocalDate;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import properties_manager.PropertiesManager;

/**
 *
 * @author Faizan Shaikh
 */

public class ScheduleController {
     private CourseSiteGeneratorApp app;
    private boolean workspaceControlLock;
    
    public ScheduleController(CourseSiteGeneratorApp app) {
        this.app = app;   
        workspaceControlLock = false;
    }  
    public void handleAddItem() {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        ScheduleWorkspace scheduleWorkspace = workspace.getScheduleWorkspace();

        PropertiesManager props = PropertiesManager.getPropertiesManager();

        String typeValue = scheduleWorkspace.getTypeBox().getValue();
        String typeName = null;
        
        for (String propertyName : props.getPropertyOptionsList(CourseSiteGeneratorProp.SCHEDULE_ITEM_PROPERTY_NAMES)) {
            String propValue = props.getProperty(propertyName);
            
            if (propValue.equals(typeValue)) {
                typeName = propertyName;
                break;
            }
        }
        
        String title = scheduleWorkspace.getTitleTextField().getText();
        LocalDate date = scheduleWorkspace.getItemDatePicker().getValue();
        
        //String time = scheduleWorkspace.getTimeTextField().getText();
        String topic = scheduleWorkspace.getTopicTextField().getText();
        String link = scheduleWorkspace.getLinkTextField().getText();
        //String criteria = scheduleWorkspace.getCriteriaTextField().getText();
        
        if (typeName == null || typeName.isEmpty()) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Cannot add");
            alert.setContentText("Type required");

            alert.showAndWait();
            return;
        }
        else if (date == null) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Cannot add");
            alert.setContentText("Date required");

            alert.showAndWait();
            return;
        }        
        
        
        /*else if (title == null || title.isEmpty()) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Cannot add");
            alert.setContentText("Title required");

            alert.showAndWait();
            return;
        }
        else if (topic == null || topic.isEmpty()) {      
            if (!scheduleWorkspace.getTopicTextField().isDisabled()) {
                Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Cannot add");
            alert.setContentText("Topic required");

            alert.showAndWait();
                return;
            }
        }
        else if (link == null || link.isEmpty()) {          
            if (!scheduleWorkspace.getLinkTextField().isDisabled()) {

                Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Cannot add");
            alert.setContentText("Link required");

            alert.showAndWait();
                return;
            }
        }*/
        
        
        
        //Check if schedule item already exists with specified information 
        ScheduleData scheduleData = ((CourseSiteGeneratorData)app.getDataComponent()).getScheduleData();
        
        if (date.isBefore(scheduleWorkspace.getStartDatePicker().getValue()) || date.isAfter(scheduleWorkspace.getEndDatePicker().getValue())) {
            
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Cannot add");
            alert.setContentText("Date out of bounds");

            alert.showAndWait();
            return;
        }
        
        if (scheduleData.containsScheduleItem(typeName, date, title, topic, link)) {
            
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Cannot add");
            alert.setContentText("Already Exists");

            alert.showAndWait();
            return;
        }  
        
        ScheduleTransactions.AddItemTransaction transaction = 
                new ScheduleTransactions.AddItemTransaction(app, typeName, title, date, topic, link);
        
        workspace.getTransactionProcessor().addTransaction(transaction);     
        handleClearItem();
    }

    public void handleEditItem() {
                 CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        ScheduleWorkspace scheduleWorkspace = workspace.getScheduleWorkspace();

        PropertiesManager props = PropertiesManager.getPropertiesManager();

        String typeValue = scheduleWorkspace.getTypeBox().getValue();
        String typeName = null;
        
        for (String propertyName : props.getPropertyOptionsList(CourseSiteGeneratorProp.SCHEDULE_ITEM_PROPERTY_NAMES)) {
            String propValue = props.getProperty(propertyName);
            
            if (propValue.equals(typeValue)) {
                typeName = propertyName;
                break;
            }
        }

        String title = scheduleWorkspace.getTitleTextField().getText();
        LocalDate date = scheduleWorkspace.getItemDatePicker().getValue();
        //String time = scheduleWorkspace.getTimeTextField().getText();
        String topic = scheduleWorkspace.getTopicTextField().getText();
        String link = scheduleWorkspace.getLinkTextField().getText();
        //String criteria = scheduleWorkspace.getCriteriaTextField().getText();
        
        if (typeName == null || typeName.isEmpty()) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Cannot add");
            alert.setContentText("Type required");

            alert.showAndWait();return;
        }
        else if (date == null) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Cannot add");
            alert.setContentText("date required");

            alert.showAndWait();return;
        }        
        
        
        else if (title == null || title.isEmpty()) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Cannot add");
            alert.setContentText("title required");

            alert.showAndWait();
            return;            
        }
        else if (topic == null || topic.isEmpty()) {
            
            if (!scheduleWorkspace.getTopicTextField().isDisabled()) {
                Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Cannot add");
            alert.setContentText("topic required");

            alert.showAndWait();return;
            }
        }
        else if (link == null || link.isEmpty()) {
            
            if (!scheduleWorkspace.getLinkTextField().isDisabled()) {

                Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Cannot add");
            alert.setContentText("link required");

            alert.showAndWait();return;
            }
        }
        
        //Check if schedule item already exists with specified information 
        ScheduleData scheduleData = ((CourseSiteGeneratorData)app.getDataComponent()).getScheduleData();
        
        if (scheduleData.containsScheduleItem(typeName, date, title, topic, link)) {
            
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Cannot add");
            alert.setContentText("Already Exists");

            alert.showAndWait();return;
        }  

        
        ScheduleItem newItem = new ScheduleItem(typeName, date, title, topic,  link);
        ScheduleItem oldItem = scheduleWorkspace.getScheduleTable().getSelectionModel().getSelectedItem();

        
        ScheduleTransactions.EditItemTransaction transaction = 
            new ScheduleTransactions.EditItemTransaction(app, newItem, oldItem);
        
        workspace.getTransactionProcessor().addTransaction(transaction);        
        handleClearItem();
    }

    public void handleClearItem() {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        ScheduleWorkspace scheduleWorkspace = workspace.getScheduleWorkspace();
        scheduleWorkspace.getTitleTextField().clear();
        scheduleWorkspace.getItemDatePicker().setValue(null);
        //scheduleWorkspace.getTimeTextField().clear();
        scheduleWorkspace.getTopicTextField().clear();
        scheduleWorkspace.getLinkTextField().clear();
        //scheduleWorkspace.getCriteriaTextField().clear();
        scheduleWorkspace.getTypeBox().setValue(null);
        scheduleWorkspace.getRemoveButton().setDisable(true);
        scheduleWorkspace.getScheduleTable().getSelectionModel().clearSelection();
        
    }

    public void tendToEdit(ScheduleItem selectedItem) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        ScheduleWorkspace scheduleWorkspace = workspace.getScheduleWorkspace();
        scheduleWorkspace.getItemDatePicker().setValue(selectedItem.getDate());
        scheduleWorkspace.getTypeBox().setValue(props.getProperty(selectedItem.getType()));
        scheduleWorkspace.getTopicTextField().setText(selectedItem.getTopic());
        scheduleWorkspace.getLinkTextField().setText(selectedItem.getLink());
        scheduleWorkspace.getTitleTextField().setText(selectedItem.getTitle());
        scheduleWorkspace.getRemoveButton().setDisable(false);
    }

    public void removeItem() {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        ScheduleWorkspace scheduleWorkspace = workspace.getScheduleWorkspace();
        ScheduleItem toberemove=scheduleWorkspace.getScheduleTable().getSelectionModel().getSelectedItem();
        ScheduleTransactions.RemoveItemTransaction remItem = new ScheduleTransactions.RemoveItemTransaction(app,toberemove);
        workspace.getTransactionProcessor().addTransaction(remItem);        
        handleClearItem();
    }

    public void handleDateChange() {
        CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        ScheduleWorkspace sworkspace = workspace.getScheduleWorkspace();
        LocalDate sld =sworkspace.getStartDatePicker().getValue();
        LocalDate eld =sworkspace.getEndDatePicker().getValue();
        ScheduleTransactions.DateChangeTransaction change = new ScheduleTransactions.DateChangeTransaction(app,sld,eld);
        workspace.getTransactionProcessor().addTransaction(change);        
        //sworkspace.getStartDatePicker().setValue(eld);
    }
}

