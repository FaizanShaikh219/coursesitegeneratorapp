package csg.workspace.controller;
/**
 *
 * @author Faizan Shaikh
 */
import org.apache.commons.io.FileUtils;

import csg.CourseSiteGeneratorApp;
import csg.data.SiteData;
import csg.data.CourseSiteGeneratorData;
import csg.file.CourseSiteGeneratorFiles;
import csg.workspace.CourseSiteGeneratorWorkspace;
import csg.workspace.SiteWorkspace;
import static csg.workspace.SiteWorkspace.DEFAULT_EXPORT_DIRECTORY;
import static djf.settings.AppPropertyType.LOAD_WORK_TITLE;
import static djf.settings.AppStartupConstants.PATH_WORK;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;


public class SiteController {
    private CourseSiteGeneratorApp app;
     public static final String DEFAULT_EXPORT_DIRECTORY = "/work/export/";
      public static final String DEFAULT_FAKEEXPORT_DIRECTORY = "/work/public_html";
    public SiteController(CourseSiteGeneratorApp app) {
        this.app = app;   
    }
    
    public void handlePageToggle() {

        
    }

    public void handleSelectTemplate() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();

        DirectoryChooser dc = new DirectoryChooser();
        dc.setInitialDirectory(new File(data.getTemplateDirectory()));
	dc.setTitle(props.getProperty(LOAD_WORK_TITLE));
        File selectedDir = dc.showDialog(app.getGUI().getWindow());
        
        if (selectedDir != null) {
            data.setTemplateDir(selectedDir.getPath());
            //data.reloadSitePages();
        }
    }

    public void handleSelectFavicon() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();
 
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK+"/images/"));
	fc.setTitle(props.getProperty(LOAD_WORK_TITLE));
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());
        if (selectedFile != null) { 
            data.setFaviconPath(selectedFile.getPath());
        }
    }
    public void handleSelectNavbar() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();
 
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK+"/images/"));
	fc.setTitle(props.getProperty(LOAD_WORK_TITLE));
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());
        if (selectedFile != null) { 
            data.setNavbarPath(selectedFile.getPath());
        }
    }

    public void handleSelectLeftFooter() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();

        
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK+"/images/"));
	fc.setTitle(props.getProperty(LOAD_WORK_TITLE));
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());
        
        if (selectedFile != null) {
            data.setLeftFooterPath(selectedFile.getPath());
        }
    }

    public void handleSelectRightFooter() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();
  
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK+"/images/"));
	fc.setTitle(props.getProperty(LOAD_WORK_TITLE));
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());
        
        if (selectedFile != null) {
            data.setRightFooterPath(selectedFile.getPath());
        }
    }

    public void handleSubjectChange() {
        SiteWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();

        String subject = workspace.getSubjectBox().getValue();
        if (subject != null) {
            data.setSubject(subject);
            data.setML(workspace.getMajorCodes());
        }
        
    }

    public void handleNumberChange() {
        SiteWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();

        String number = workspace.getNumberBox().getValue();
        //if (number != null) {
            data.setNumber(number);
        //}
    }

    public void handleYearChange() {
        SiteWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();

        String year = workspace.getYearBox().getValue();
        
        if (year != null) {
            data.setYear(year);
        }
    }

    public void handleSemesterChange() {
        SiteWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();

        String semester = workspace.getSemesterBox().getValue();
        
        if (semester != null) {
            data.setSemester(semester);
        }
    }
    public void handleInsNameChange() {
        SiteWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();

        String name = workspace.getInsName().getText();
        //System.out.println("y");
        if (name != null) {
        //    System.out.println("y");
            data.setInsName(name);
        }
    }
    public void handleInsEmailChange() {
        SiteWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();

        String email = workspace.getInsEmail().getText();//.getSemesterBox().getValue();
        
        if (email != null) {
            data.setInsEmail(email);//  .setSemester(semester);
        }
    }
    public void handleInsRoomChange() {
        SiteWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();

        String room = workspace.getInsRoom().getText();
        
        if (room != null) {
            data.setInsRoom(room);
        }
    }
    public void handleInsHomeChange() {
        SiteWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();

        String home = workspace.getInsHome().getText();//.getSemesterBox().getValue();
        
        if (home != null) {
            data.setInsHome(home);//.setSemester(semester);
        }
    }
    public void handleInsOHChange() {
        SiteWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();

        String oh = workspace.getInsOHText();//.getText();//.getSemesterBox().getValue();
        
        if (oh != null) {
            data.setInsOH(oh);//.setSemester(semester);
        }
    }

    public void handleSelectExportDir() {
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();

        DirectoryChooser dc = new DirectoryChooser();
        
        String exportDir = data.getExportDirectory();
        if (exportDir == null || exportDir.isEmpty() || !(new File(exportDir)).exists()) {
            exportDir = System.getProperty("user.dir") + CourseSiteGeneratorFiles.DEFAULT_EXPORT_DIRECTORY;
            
        }        
        
  
        dc.setInitialDirectory(new File(exportDir));
	dc.setTitle(props.getProperty(LOAD_WORK_TITLE));
        File selectedDir = dc.showDialog(app.getGUI().getWindow());
        
        if (selectedDir != null) {
            data.setExportDir(selectedDir.getPath());
            app.getGUI().updateExportControl(true);
        }
        
    }

    public void handleStyleChange() {
        SiteWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();

        String style = workspace.getStyleBox().getValue();
        if (style != null) {
            data.setStyleSheetName(style);
        }
         
    
    }

    public void checkEmail(String text) {
        SiteWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();
        if(!data.isLegalEmail(text)){
                workspace.getInsEmail().setStyle("-fx-text-fill: red;");
                
            }
            else{
                workspace.getInsEmail().setStyle("-fx-text-fill: black;");
                
            }
    }

    public ArrayList addL() {
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();
        return data.getSubL();
    }

    public void callList(ArrayList sub) {
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();
        data.setSubL(sub);
    }

    public void handleExport() {
                SiteWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        SiteData data = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();
        if(!data.getSubject().isEmpty()&&!data.getNumber().isEmpty()&&!data.getSemester().isEmpty()&&!data.getYear().isEmpty()){
        data.setExportDir(System.getProperty("user.dir")+DEFAULT_EXPORT_DIRECTORY.toString()+workspace.getSubjectBox().getValue()+"_"+workspace.getNumberBox().getValue().toString()
                            +"_"+workspace.getSemesterBox().getValue()+"_"+workspace.getYearBox().getValue()+"/public_html");
        }
    }
      
}
