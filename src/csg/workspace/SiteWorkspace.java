package csg.workspace;
/**
 *
 * @author Faizan Shaikh
 */
import csg.workspace.controller.SiteController;
import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import csg.data.SiteData;
import csg.data.CourseSiteGeneratorData;

import djf.components.AppDataComponent;
import static djf.settings.AppPropertyType.ADD_ITEM;
import static djf.settings.AppPropertyType.REMOVE_ITEM;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.OverrunStyle;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;


public class SiteWorkspace implements Tab {
    
    public static int LIST_START_YEAR = Calendar.getInstance().get(Calendar.YEAR);
    public static int LIST_END_YEAR = Calendar.getInstance().get(Calendar.YEAR) + 1;
    private int maxCourseNumber=150;
    //private ArrayList numlist;
    //private ArrayList sublist;
    String type="";
    private SiteController siteController;
    private VBox courseBox;
    //private HBox templateBox;
    private CourseSiteGeneratorApp app;
    private ComboBox subjectBox;
    private ComboBox numberBox;
    private ComboBox semesterBox;
    private ComboBox yearBox;
    private ObservableList<String> subList;
    //private ObservableList<String> numList;
    private TextField titleTextField;
    private TextArea ohText;
    private TextField insName;
    private TextField insEmail;
    private TextField insHome;
    private TextField insRoom;
    private String ohdata;
    private Label exportLabel;
    //private Button chooseExportPathButton;
    private List majorCodes;
    private Button expandText;
    private Button faviconButton;
    private Button navbarButton;
    private Button leftFooterButton;
    private Button rightFooterButton;
    private ImageView faviconImage;
    private ImageView navbarImage;
    private ImageView leftFooterImage;
    private ImageView rightFooterImage;
    private CheckBox home;
    private CheckBox syllabus;
    private CheckBox sch;
    private CheckBox hws;
    private ChoiceBox styleBox;
    public static final String DEFAULT_EXPORT_DIRECTORY = "/work/export/";
    private Label bannerInfoTitleLabel;
    private Label pageTitleLabel;
    private Label styleTitleLabel;
    private Label instructor;
    private GridPane bannerPane;
    private GridPane pageBox;
    private GridPane styleInfoPane;
    private GridPane insPane;
    public String [] sub;
    public ArrayList<String> subL;
     public String [] num;
    public ArrayList<String> numL;
    //private TableView<SitePage> sitePageTable;
    //public SiteFiles s;
    
    public SiteWorkspace(CourseSiteGeneratorApp app) {
       SiteData courseData = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();
        this.app = app;   
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        siteController = new SiteController(app);
        
       
        courseBox = new VBox(10);
        //courseBox.setEffect(new DropShadow(10, Color.BLACK));
        courseBox.setPadding(new Insets(10, 50, 10, 50));
        ohdata="";
        
        
        
        subL=new ArrayList();
        bannerPane = new GridPane();
        getBannerPane().setHgap(10);
        getBannerPane().setVgap(10);
        
        subjectBox = new ComboBox();
        numberBox = new ComboBox();
        hws=new CheckBox();
        syllabus = new CheckBox();
        sch=new CheckBox();
        home=new CheckBox();
        home.setSelected(true);
        sch.setSelected(true);
        syllabus.setSelected(true);
        hws.setSelected(true);
        
        semesterBox = new ComboBox();
        subjectBox.setEditable(true);
        numberBox.setEditable(true);
        yearBox = new ComboBox();
        titleTextField = new TextField();
        semesterBox.setEditable(true);
        yearBox.setEditable(true);
        exportLabel = new Label(System.getProperty("user.dir")+DEFAULT_EXPORT_DIRECTORY.toString());
        exportLabel.setMaxWidth(230);
        exportLabel.setMinWidth(230);
        exportLabel.setTextOverrun(OverrunStyle.LEADING_ELLIPSIS);
        //if()
            courseData.setExportDir(System.getProperty("user.dir")+DEFAULT_EXPORT_DIRECTORY.toString());
        //chooseExportPathButton = new Button(props.getProperty(CourseSiteGeneratorProp.CHANGE_BUTTON));
        bannerInfoTitleLabel = new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_INFO_TITLE.toString()));
        getBannerPane().add(bannerInfoTitleLabel, 1, 1);
        getBannerPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_SUBJECT_TEXT.toString())), 1, 2);
        getBannerPane().add(subjectBox, 2, 2);
        getBannerPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_NUMBER_TEXT)), 3, 2);
        getBannerPane().add(numberBox, 4, 2);      
        getBannerPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_SEMESTER_TEXT)), 1, 3);
        getBannerPane().add(semesterBox, 2, 3);
        getBannerPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_YEAR_TEXT)), 3, 3);
        getBannerPane().add(yearBox, 4, 3);    
        getBannerPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_TITLE_TEXT)), 1, 4);
        getBannerPane().add(titleTextField, 2, 4, 3, 1);       
        //getBannerPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_INSTRUCTOR_TEXT)), 1, 5);
        //getBannerPane().add(instructorTextField, 2, 5, 3, 1);
        //getBannerPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_INSTRUCTOR_SITE_TEXT)), 1, 6);
        //getBannerPane().add(siteTextField, 2, 6, 3, 1);   
        getBannerPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_SITE_EXPORT_TEXT)), 1, 5);
        getBannerPane().add(exportLabel, 2, 5);   
        //getBannerPane().add(chooseExportPathButton, 2, 6);  
        
        List semesters = props.getPropertyOptionsList(CourseSiteGeneratorProp.COURSE_SEMESTERS);
        semesterBox.getItems().addAll(semesters);
        //props.getPropertyOptionsList(CourseSiteGeneratorProp.COURSE_MAJOR_CODES).add("AI");
        majorCodes = props.getPropertyOptionsList(CourseSiteGeneratorProp.COURSE_MAJOR_CODES);
        subjectBox.getItems().addAll(majorCodes);
        try{
        //File fileToBeModified = new File("siteText.txt");
        String oldContent = "";
        BufferedReader reader = new BufferedReader(new FileReader("siteText.txt"));
        String line = reader.readLine();
        while (line != null) 
                    {
                        //System.out.println(line);
                        oldContent = oldContent + line + "";
                        //System.out.println(oldContent);
                        line = reader.readLine();
                    }
        System.out.println(oldContent);
        if(!oldContent.trim().isEmpty()){
            sub=oldContent.split(" ");
            for(int j=0;j<sub.length;j++){
               subL.add(sub[j]);
            }
            subList=getSubjectBox().getItems();
            for(int i=0;i<subL.size();i++){
            if(!subList.contains((subL.get(i)).toString())){
                subList.add((subL.get(i)).toString());
            }
        }//siteData.setSubL(subL);
        }
        }catch(IOException e){
        e.printStackTrace();
        }
        
        //siteController.callList(sf.getSub());
        //subjectBox.getItems().addAll(siteController.addL());
        //sublist.addAll(majorCodes);
        yearBox.getItems().addAll(buildYearList());
        List list=buildCourseList();
        numberBox.getItems().addAll(list);
        
        //getComponentPane().add(getBannerPane(), 0, 0);
        courseBox.getChildren().add(getBannerPane());
        
        
        
        //PropertiesManager props = PropertiesManager.getPropertiesManager();
        //SiteData courseData = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();
        
        //templateBox = new HBox(10);
        pageBox = new GridPane();
        pageBox.setVgap(10);
        
        //selectTemplateButton = new Button(props.getProperty(CourseSiteGeneratorProp.COURSE_SELECT_TEMPLATE_BUTTON));
        pageTitleLabel = new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_TEMPLATE_TITLE));
        pageBox.add(getPageTitleLabel(), 0, 0);
        //pageBox.add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_TEMPLATE_MESSAGE)), 0, 1);
        pageBox.add(home, 0, 2);
        pageBox.add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_HOME_TEXT)), 1, 2);
        pageBox.add(syllabus, 0, 3);
        pageBox.add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_SCHEDULE_TEXT)), 1, 3);
        pageBox.add(sch, 0, 4);
        pageBox.add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_SYLLABUS_TEXT)), 1, 4);
        pageBox.add(hws, 0, 5);
        pageBox.add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_HW_TEXT)), 1, 5);
        //getComponentPane().add(getPageBox(), 4, 0);
        courseBox.getChildren().add(getPageBox());   
        
        
        
        //PropertiesManager props = PropertiesManager.getPropertiesManager();
        //CourseData courseData = ((CourseSiteGeneratorData)app.getDataComponent()).getCourseData();
        
        styleInfoPane = new GridPane();
        getStyleInfoPane().setHgap(10);
        getStyleInfoPane().setVgap(10);
          
        faviconButton = new Button(props.getProperty(CourseSiteGeneratorProp.CHANGE_BUTTON.toString()));
        navbarButton = new Button(props.getProperty(CourseSiteGeneratorProp.CHANGE_BUTTON.toString()));
        leftFooterButton = new Button(props.getProperty(CourseSiteGeneratorProp.CHANGE_BUTTON.toString()));
        rightFooterButton = new Button(props.getProperty(CourseSiteGeneratorProp.CHANGE_BUTTON.toString()));
        faviconImage = new ImageView();
        navbarImage = new ImageView();
        leftFooterImage = new ImageView();
        rightFooterImage = new ImageView();
        
        faviconImage.setPreserveRatio(true);
        navbarImage.setPreserveRatio(true);
        leftFooterImage.setPreserveRatio(true);
        rightFooterImage.setPreserveRatio(true);
        
        faviconImage.setFitWidth(200);
        navbarImage.setFitWidth(200);
        leftFooterImage.setFitWidth(200);
        rightFooterImage.setFitWidth(200);
        
        styleBox = new ChoiceBox();
        styleBox.setItems(courseData.getStylesheets());
        
        styleTitleLabel = new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_STYLE_TITLE.toString()));
        getStyleInfoPane().add(styleTitleLabel, 1, 1);
        getStyleInfoPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_FAVICON_TEXT.toString())), 1, 2);
        getStyleInfoPane().add(faviconImage, 2, 2, 2, 1);
        getStyleInfoPane().add(faviconButton, 4, 2);
        
        getStyleInfoPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_NAVBAR_TEXT.toString())), 1, 3);
        getStyleInfoPane().add(navbarImage, 2, 3, 2, 1);
        getStyleInfoPane().add(navbarButton, 4, 3);
        
        
        getStyleInfoPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_LEFT_FOOTER_TEXT.toString())), 1, 4);
        getStyleInfoPane().add(leftFooterImage, 2, 4, 2, 1);
        getStyleInfoPane().add(leftFooterButton, 4, 4);

        getStyleInfoPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_RIGHT_FOOTER_TEXT.toString())), 1, 5);
        getStyleInfoPane().add(rightFooterImage, 2, 5, 2, 1);
        getStyleInfoPane().add(rightFooterButton, 4, 5);

        getStyleInfoPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_STYLESHEET_TEXT.toString())), 1, 6);
        getStyleInfoPane().add(styleBox, 2, 6);
        getStyleInfoPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_STYLE_SHEET_INFO_TEXT.toString())), 1, 7, 4, 1);
        
       //getComponentPane().add(getStyleInfoPane(), 0, 1);
        courseBox.getChildren().add(getStyleInfoPane());
        
        insPane = new GridPane();
        getInsPane().setHgap(10);
        getInsPane().setVgap(10);
        insName = new TextField();
        insEmail = new TextField();
        insRoom = new TextField();
        insHome = new TextField();
        ohText=new TextArea();
        ohText.setPrefColumnCount(5);
        ohText.setPrefRowCount(5);
        expandText=new Button();
        //PropertiesManager props = PropertiesManager.getPropertiesManager();
	
	// LOAD THE ICON FROM THE PROVIDED FILE
        
        //ohText.setMinHeight(4.0);
        //ohText.setMinWidth(10.0);
        instructor = new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_INSTRUCTOR_LABEL.toString()));
        //getInsPane().add(instructor, 1, 1);
        getInsPane().add(instructor, 1, 1);
        getInsPane().add(insName, 2, 2);
        getInsPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_INSTRUCTOR_NAME.toString())), 1, 2);
        getInsPane().add(insEmail, 2, 3);
        getInsPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_INSTRUCTOR_EMAIL.toString())), 1, 3);
        getInsPane().add(insHome, 2, 4);
        getInsPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_INSTRUCTOR_HOME.toString())), 1, 4);
        getInsPane().add(insRoom, 2, 5);
        getInsPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_INSTRUCTOR_ROOM.toString())), 1, 5);
        getInsPane().add(expandText, 2, 6);
        getInsPane().add(new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_INSTRUCTOR_OH.toString())), 1, 6);
        //getInsPane().add(ohText, 1, 7);
        
        ohText.setVisible(false);
        ohText.setPadding(new Insets(5,5,5,5));
        courseBox.getChildren().add(getInsPane());
        
        
        
        initControls();
        
        
    }
    
  
    /**
     * initialize the functionality of all the controls
     */
    private void initControls() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        SiteData courseData = ((CourseSiteGeneratorData)app.getDataComponent()).getSiteData();
        titleTextField.textProperty().bindBidirectional(courseData.getTitleProperty());
        String imagePathplus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(ADD_ITEM);
        String imagePathminus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(REMOVE_ITEM);
        Image buttonImage1 = new Image(imagePathplus);
        Image buttonImage2 = new Image(imagePathminus);
	insEmail.textProperty().addListener(e->{
            siteController.checkEmail(insEmail.getText());
        });
        ImageView buttonView1 = new ImageView(buttonImage1);
        ImageView buttonView2 = new ImageView(buttonImage2);
        buttonView1.setFitHeight(20);
        buttonView1.setFitWidth(20);
        buttonView2.setFitHeight(20);
        buttonView2.setFitWidth(20);
        expandText.setGraphic(buttonView1);
        expandText.setOnMouseClicked(e->{
            if(ohText.isVisible())
            {
                ohText.setVisible(false);
                expandText.setGraphic(buttonView1);
                ohdata=ohText.getText();
                courseBox.getChildren().remove(ohText);
            }
            else{
                ohText.setVisible(true);
                expandText.setGraphic(buttonView2);
                ohText.setText(ohdata);
                courseBox.getChildren().add(ohText);
            }
        });
        ohText.textProperty().addListener(e->{ 
            ohdata=ohText.getText();
            siteController.handleInsOHChange();
            
                    });
        subjectBox.setOnAction(e -> {
            siteController.handleSubjectChange();
            siteController.handleExport();
             exportLabel.setText(courseData.getExportDirectory());
        });
        numberBox.setOnAction(e -> {
            siteController.handleNumberChange();
            siteController.handleExport();
             exportLabel.setText(courseData.getExportDirectory());
            // this.reloadWorkspace(app.getDataComponent());
                });
        semesterBox.setOnAction(e -> {
            siteController.handleSemesterChange();
            siteController.handleExport();
             exportLabel.setText(courseData.getExportDirectory());
                });
        yearBox.setOnAction(e -> {
            siteController.handleYearChange();
            siteController.handleExport();
             exportLabel.setText(courseData.getExportDirectory());
                });
        styleBox.setOnAction(e -> siteController.handleStyleChange());
        insName.textProperty().addListener(e->siteController.handleInsNameChange());//.setOnKeyTyped(e->courseController.handleInsNameChange());//.setOnAction(e -> courseController.handleInsNameChange());
        insEmail.textProperty().addListener(e -> siteController.handleInsEmailChange());
        insRoom.textProperty().addListener(e -> siteController.handleInsRoomChange());
        insHome.textProperty().addListener(e -> siteController.handleInsHomeChange());
        //ohText.setOn.setOnAction(e -> courseController.handleStyleChange());
        //chooseExportPathButton.setOnAction(e -> {
        //    siteController.handleSelectExportDir();
        //    exportLabel.setText(courseData.getExportDirectory());
        //});
        //selectTemplateButton.setOnAction(e -> courseController.handleSelectTemplate());
        faviconButton.setOnAction(e -> {         
            siteController.handleSelectFavicon();
            faviconImage.setImage(new Image(new File(courseData.getFaviconPath()).toURI().toString()));
        });
        navbarButton.setOnAction(e -> {         
            siteController.handleSelectNavbar();
            navbarImage.setImage(new Image(new File(courseData.getFaviconPath()).toURI().toString()));
        });
        leftFooterButton.setOnAction(e -> {
            siteController.handleSelectLeftFooter();
            leftFooterImage.setImage(new Image(new File(courseData.getLeftFooterPath()).toURI().toString()));    
        });
        rightFooterButton.setOnAction(e -> {   
            siteController.handleSelectRightFooter();
            rightFooterImage.setImage(new Image(new File(courseData.getRightFooterPath()).toURI().toString()));         
        });           
    }
    //public void getExportLabel
    /**
     * load workspace with new data
     * @param dataComponent the new data to load
     */
    public void reloadWorkspace(AppDataComponent dataComponent) {
        
        SiteData courseData = ((CourseSiteGeneratorData)dataComponent).getSiteData();
        semesterBox.setValue(courseData.getSemester());
        yearBox.setValue(courseData.getYear());
        
        //if(subjectBox.getValue()==null)
        //    numberBox.setValue("");
            
        //else
            try{
        //File fileToBeModified = new File("siteText.txt");
        String oldContent = "";
        BufferedReader reader = new BufferedReader(new FileReader("siteText.txt"));
        String line = reader.readLine();
        while (line != null) 
                    {
                        //System.out.println(line);
                        oldContent = oldContent + line + "";
                        //System.out.println(oldContent);
                        line = reader.readLine();
                    }
        System.out.println(oldContent);
        if(!oldContent.trim().isEmpty()){
            sub=oldContent.split(" ");
            for(int j=0;j<sub.length;j++){
               subL.add(sub[j]);
            }
            subList=getSubjectBox().getItems();
            for(int i=0;i<subL.size();i++){
            if(!subList.contains((subL.get(i)).toString())){
                subList.add((subL.get(i)).toString());
            }
        }//siteData.setSubL(subL);
        }
        }catch(IOException e){
        e.printStackTrace();
        }
            List list=buildCourseList();
        numberBox.getItems().clear();
        numberBox.getItems().addAll(list);
        numberBox.setValue(courseData.getNumber());
        subjectBox.setValue(courseData.getSubject());
        if(!gettypeClick().equals("l")){
            numberBox.setValue("");
            subjectBox.setValue("");
        }
        
        //numberBox.
       // numberBox.getItems().clear();
        //numberBox.getItems().addAll(list);
        styleBox.setValue(courseData.getStyleSheetName());
        insName.setText(courseData.getInsName());
        insEmail.setText(courseData.getInsEmail());
        insRoom.setText(courseData.getInsRoom());
        insHome.setText(courseData.getInsHome());
        
        faviconImage.setImage(new Image(new File(courseData.getFaviconPath()).toURI().toString()));
        navbarImage.setImage(new Image(new File(courseData.getFaviconPath()).toURI().toString()));
        leftFooterImage.setImage(new Image(new File(courseData.getLeftFooterPath()).toURI().toString()));
        rightFooterImage.setImage(new Image(new File(courseData.getRightFooterPath()).toURI().toString()));
        
        if (courseData.getExportDirectory() != null || !courseData.getExportDirectory().isEmpty()) {
            //app.getGUI().updateExportControl(true);
        } 
        //exportLabel.setText(courseData.getExportDirectory());
        //courseData.reloadSitePages();

    }
    public List getMajorCodes(){
        return majorCodes;
    }
    public void setSubChoice(List mc){
        subjectBox.getItems().clear();
        subjectBox.getItems().addAll(mc);
    }
    public String gettypeClick(){
        return type;
    }
    public void settypeClick(String mc){
        type=mc;
        
    }
    public List buildYearList() {
        
        List yearList = new ArrayList(LIST_END_YEAR - LIST_START_YEAR + 1);
        
        for (int i = LIST_START_YEAR; i <= LIST_END_YEAR; i++) {
            yearList.add(""+i);
        }
        
        return yearList;
    }
    
    public List buildCourseList() {
        //maxCourseNumber = 150;
        int minCourseNumber = 101;
        List courseNumberList = new ArrayList(maxCourseNumber - minCourseNumber + 1);

        for (int i = minCourseNumber; i <= maxCourseNumber; i++) {
            courseNumberList.add(""+i);
            //numlist.add(""+i);
        }
        
        return courseNumberList;
           
    }
    public int getmaxCourseNum() { 
        return maxCourseNumber;
    }
    public void setmaxCourseNum(int max) { 
         maxCourseNumber=max;
         //reloadWorkspace();
    }
    public VBox getComponentPane() { 
        return courseBox;
    }
    public ComboBox<String> getSubjectBox() {
        return subjectBox;
    }

    public ComboBox<String> getNumberBox() {
        return numberBox;
    }

    public ComboBox<String> getSemesterBox() {
        return semesterBox;
    }
    
    public ChoiceBox<String> getStyleBox() {
        return styleBox;
    }

    public ComboBox<String> getYearBox() {
        return yearBox;
    }

    public TextField getTitleTextField() {
        return titleTextField;
    }
     public TextField getInsName() {
        return insName;
    }
    /* public ArrayList getNumList() {
        return numlist;
    }
     public ArrayList getSubList() {
        return sublist;
    }*/
      public TextField getInsEmail() {
        return insEmail;
    }
       public TextField getInsHome() {
        return insHome;
    }
        public TextField getInsRoom() {
        return insRoom;
    }
    public TextArea getInsOH() {
        return ohText;
    }
     public String getInsOHText() {
        return ohdata;
    }
      public void setInsOHText(String text) {
         ohdata=text;
         ohText.setText(text);
    }

    /**
     * @return the courseInfoTitleLabel
     */
    public Label getBannerInfoTitleLabel() {
        return bannerInfoTitleLabel;
    }
    public Label getInstructorLabel() {
        return instructor;
    }
    /**
     * @return the templateTitleLabel
     */
    public Label getPageTitleLabel() {
        return pageTitleLabel;
    }

    /**
     * @return the styleTitleLabel
     */
    public Label getStyleTitleLabel() {
        return styleTitleLabel;
    }

    /**
     * @return the infoPane
     */
    public GridPane getBannerPane() {
        return bannerPane;
    }
    public GridPane getInsPane() {
        return insPane;
    }

    /**
     * @return the templateBox
     */
    public GridPane getPageBox() {
        return pageBox;
    }

    /**
     * @return the styleInfoPane
     */
    public GridPane getStyleInfoPane() {
        return styleInfoPane;
    }

    void resetWorkspace() {
        
        subjectBox.setValue(null);
        numberBox.setValue(null);
        semesterBox.setValue(null);
        yearBox.setValue(null);
        styleBox.setValue(null);
        insName.clear();
        insRoom.clear();
        insEmail.clear();
        insHome.clear();
        ohText.clear();
        this.settypeClick("n");
    }

    void handleDeleteKeyEvent() {
    }
}
