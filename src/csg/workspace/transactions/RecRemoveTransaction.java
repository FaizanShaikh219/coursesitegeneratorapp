package csg.workspace.transactions;

import csg.CourseSiteGeneratorApp;
import csg.data.CourseSiteGeneratorData;
import csg.data.MeetingTimesData;
import csg.data.Recitations;
import jtps.jTPS_Transaction;

/**
 *
 * @author USER
 */
public class RecRemoveTransaction implements jTPS_Transaction{
    Recitations rec;
    CourseSiteGeneratorApp app;
    public RecRemoveTransaction(CourseSiteGeneratorApp app, Recitations rec) {
        this.app=app;
        this.rec=rec;
    }

    @Override
    public void doTransaction() {
        MeetingTimesData mtData = ((CourseSiteGeneratorData)app.getDataComponent()).getMeetingTimesData();
            mtData.removeRec(rec); 
    }

    @Override
    public void undoTransaction() {
        MeetingTimesData mtData = ((CourseSiteGeneratorData)app.getDataComponent()).getMeetingTimesData();
            mtData.addRec(rec); 
    }
    
}
