package csg.workspace.transactions;
/**
 *
 * @author Faizan Shaikh
 */
import csg.CourseSiteGeneratorApp;
import csg.data.CourseSiteGeneratorData;
import csg.data.OHTimeSlot;
import csg.data.TAData;
import csg.data.TAData.TimeSlotPosition;
import csg.data.TeachingAssistant;
import csg.workspace.CourseSiteGeneratorWorkspace;
import csg.workspace.OfficeHoursWorkspace;
import djf.ui.AppGUI;
import java.util.List;
import javafx.application.Platform;
import jtps.jTPS_Transaction;


public class TATransactions {
 
    
    public static class AddTATransaction implements jTPS_Transaction {

        TeachingAssistant ta;
        CourseSiteGeneratorApp app;
    
        public AddTATransaction(CourseSiteGeneratorApp initApp, TeachingAssistant ta) {
            this.app = initApp;
            this.ta=ta;
        }
        @Override
        public void doTransaction() {
            AppGUI gui = app.getGUI();
            
            gui.getFileController().markAsEdited(gui);
            TAData taData = ((CourseSiteGeneratorData)app.getDataComponent()).getTAData();
            taData.addTA(ta);
            
        }

        @Override
        public void undoTransaction() {
            AppGUI gui = app.getGUI();
            gui.getFileController().markAsEdited(gui);
            TAData taData = ((CourseSiteGeneratorData)app.getDataComponent()).getTAData();
            taData.removeTA(ta);            
        }    
    }
    /**
     * Transaction for removing TA.
     */
    public static class RemoveTATransaction implements jTPS_Transaction {
        
            TeachingAssistant ta;
            CourseSiteGeneratorApp app;
            List<TimeSlotPosition> positions;
            List<String> mainTARecitationSections;
            List<String> assistantTARecitationSections;
    
            public RemoveTATransaction(CourseSiteGeneratorApp initApp, TeachingAssistant ta) {
                app = initApp;
                this.ta=ta;
                TAData taData = ((CourseSiteGeneratorData)app.getDataComponent()).getTAData();
                this.positions = taData.getTAOfficeHours(ta.getName());
     
            }

        @Override
        public void doTransaction() {

            AppGUI gui = app.getGUI();        
            gui.getFileController().markAsEdited(gui);
            TAData taData = ((CourseSiteGeneratorData)app.getDataComponent()).getTAData();
            
            for (TimeSlotPosition position : positions) {
                taData.toggleTA(position, ta,ta.getName());
            }
            
            taData.removeTA(ta);
            
            //remove TA from recitations
            //RecitationData recData = ((CourseSiteGeneratorData)app.getDataComponent()).getRecitatationData();        
            //List[] list = recData.removeTAFromRecitations(new String(name));
            //mainTARecitationSections = list[0];
            //assistantTARecitationSections = list[1];
            
            //((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getRecitationWorkspace().getRecitationTable().refresh();

        }

        @Override
        public void undoTransaction() {
            /*AppGUI gui = app.getGUI();  
            gui.getFileController().markAsEdited(gui);
 
            TAData taData = ((CourseSiteGeneratorData)app.getDataComponent()).getTAData();
            taData.addTA(ta);
            
            for (TimeSlotPosition position : positions) {
                taData.toggleTA(position, ta);
            }*/
            //RecitationData recData = ((CourseSiteGeneratorData)app.getDataComponent()).getRecitatationData();
            //recData.updateRecitationTAs(new String(name), mainTARecitationSections, assistantTARecitationSections);
            //((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getRecitationWorkspace().getRecitationTable().refresh();

        }
    }
    
    /**
     * Transaction for editing an existing TA.
     */
    public static class EditTATransaction implements jTPS_Transaction {

        String name;
        String email;
        TeachingAssistant ta;
        String type;
        String oldname;
        String oldemail;
        
        String oldtype;
        CourseSiteGeneratorApp app;
    
        public EditTATransaction(CourseSiteGeneratorApp initApp, String name, String email, TeachingAssistant ta,String Type,String oldname, String oldemail, String oldType) {
            app = initApp;
            this.name = name;
            this.email = email;
            this.ta=ta;
            this.type=Type;
            this.oldemail=oldemail;
            this.oldname=oldname;
            this.oldtype=oldType;
        }
     
        @Override
        public void doTransaction() {
            AppGUI gui = app.getGUI();
            gui.getFileController().markAsEdited(gui);
                  
            TAData taData = ((CourseSiteGeneratorData)app.getDataComponent()).getTAData();
            taData.editTA((name), (email), ta,type);

            //RecitationData recData = ((CourseSiteGeneratorData)app.getDataComponent()).getRecitatationData();
            //recData.updateRecitationTAName(new String(name), new String(oldName));
            
            //refresh the relevant tables
            CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
            //workspace.getRecitationWorkspace().getRecitationTable().refresh();
            workspace.getTAWorkspace().getTaTable().refresh();
        
        }

        @Override
        public void undoTransaction() {
            AppGUI gui = app.getGUI();
            app.getGUI().getFileController().markAsEdited(gui);
        
            TAData taData = ((CourseSiteGeneratorData)app.getDataComponent()).getTAData();
            taData.editTA(oldname,oldemail,ta,oldtype);
     
            //RecitationData recData = ((CourseSiteGeneratorData)app.getDataComponent()).getRecitatationData();
            //recData.updateRecitationTAName(new String(oldName), new String(name));

            //refresh the relevant tables
            CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
            //workspace.getRecitationWorkspace().getRecitationTable().refresh();
            workspace.getTAWorkspace().getTaTable().refresh();            
        }
    }
    
    /**
     * Transaction for modifying office hour boundaries.
     */
    public static class ChangeOpenOfficeHourTransaction implements jTPS_Transaction {
       
        CourseSiteGeneratorApp app;

        int newEnd;
        int newStart;
        int oldStart;
        int oldEnd;
        List slotList;
        String type;
        public ChangeOpenOfficeHourTransaction(CourseSiteGeneratorApp initApp, List state, int newStart, int newEnd, int oldStart, int oldEnd ,String type) {
    
            app = initApp;
            this.newEnd = newEnd;
            this.newStart = newStart;
            this.oldStart = oldStart;
            this.oldEnd = oldEnd;
            this.slotList = state;
            this.type=type;
        }

        @Override
        public void doTransaction() {
            AppGUI gui = app.getGUI();
            gui.getFileController().markAsEdited(gui);

            TAData taData = ((CourseSiteGeneratorData)app.getDataComponent()).getTAData();
            OfficeHoursWorkspace taWorkspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getTAWorkspace();   

            taWorkspace.setTableLock(true);
            taData.changeTimeBounds(newStart, newEnd,type);
            
            taWorkspace.getStartTime().setValue(OHTimeSlot.formatTime(newStart, 00));
            taWorkspace.getEndTime().setValue(OHTimeSlot.formatTime(newEnd, 00));            
            //This should fire after item deletion.
            Platform.runLater(() ->taWorkspace.setTableLock(false));

        }

        @Override
        public void undoTransaction() {

            AppGUI gui = app.getGUI();
            gui.getFileController().markAsEdited(gui);
            
            TAData taData = ((CourseSiteGeneratorData)app.getDataComponent()).getTAData();             
            OfficeHoursWorkspace taWorkspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getTAWorkspace();  
            
            taWorkspace.setTableLock(true);

            taData.setOfficeHourState(slotList, oldStart, oldEnd);
            taWorkspace.getStartTime().setValue(OHTimeSlot.formatTime(oldStart, 00));
            taWorkspace.getEndTime().setValue(OHTimeSlot.formatTime(oldEnd, 00));
            //This should fire after item deletion.

            Platform.runLater(() ->taWorkspace.setTableLock(false));

        }
        
    }
    
    /**
     * Transaction for adding/removing TA Office hours.
     */
    public static class ToggleOfficeHoursTransaction implements jTPS_Transaction {
        
        CourseSiteGeneratorApp app;

        int day;
        int hour;
        boolean onHour;
        
        
        TeachingAssistant t;
        
        public ToggleOfficeHoursTransaction(CourseSiteGeneratorApp initApp, TimeSlotPosition position, TeachingAssistant ta) {
            this.app = initApp;
            this.hour = position.getHour();
            this.day = position.getDayIndex();
            this.onHour = position.getOnHour();
            t=ta;
        }

        @Override
        public void doTransaction() {

            AppGUI gui = app.getGUI();
            gui.getFileController().markAsEdited(gui);
            TAData taData = ((CourseSiteGeneratorData)app.getDataComponent()).getTAData();
  
            TimeSlotPosition position = taData.new TimeSlotPosition(day, hour, onHour);
            taData.toggleTA(position, t,t.getName());
            
        }

        @Override
        public void undoTransaction() {
            AppGUI gui = app.getGUI();
            gui.getFileController().markAsEdited(gui);
            TAData taData = ((CourseSiteGeneratorData)app.getDataComponent()).getTAData();
            
            TimeSlotPosition position = taData.new TimeSlotPosition(day, hour, onHour);
            taData.toggleTA(position,t, t.getName());
               
        }        
    }    
    
}
