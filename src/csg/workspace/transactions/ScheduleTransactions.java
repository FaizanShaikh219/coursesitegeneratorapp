package csg.workspace.transactions;

import csg.CourseSiteGeneratorApp;
import csg.data.CourseSiteGeneratorData;
import csg.data.ScheduleData;
import csg.data.ScheduleItem;
import csg.workspace.CourseSiteGeneratorWorkspace;
import csg.workspace.ScheduleWorkspace;
import djf.ui.AppGUI;
import java.time.LocalDate;
import jtps.jTPS_Transaction;

/**
 *
 * @author USER
 */
public class ScheduleTransactions {
    public static class AddItemTransaction implements jTPS_Transaction {
        CourseSiteGeneratorApp app;
        
        String type;
        String title;
        String date;
        //char[] time;
        String topic;
        String link;
        //char[] criteria;
        
        
        public AddItemTransaction(CourseSiteGeneratorApp initApp, String type, String title, LocalDate date,  String topic, String link) {
            this.app = initApp;
            this.type = type;
            this.title = title;
            this.date = date.toString();
            //this.time = time.toCharArray();
            this.topic = topic;
            this.link = link;
            //this.criteria = criteria.toCharArray();
        }
        
        @Override
        public void doTransaction() {
            AppGUI gui = app.getGUI();
            gui.getFileController().markAsEdited(gui);
            ScheduleData scheduleData = ((CourseSiteGeneratorData)app.getDataComponent()).getScheduleData();
            
            scheduleData.addScheduleItem((type), LocalDate.parse((date)), (title), (topic),  (link));

        }

        @Override
        public void undoTransaction() {
            AppGUI gui = app.getGUI();
            gui.getFileController().markAsEdited(gui);
            ScheduleData scheduleData = ((CourseSiteGeneratorData)app.getDataComponent()).getScheduleData();
                     
            scheduleData.removeScheduleItem((type), LocalDate.parse((date)), (title), (topic), (link));
    

        }  
    }
    public static class EditItemTransaction implements jTPS_Transaction {
        CourseSiteGeneratorApp app;
        
        String type;
        String title;
        String date;
        //char[] time;
        String topic;
        String link;
        //char[] criteria;
  
        String oldType;
        String oldTitle;
        String oldDate;
        //char[] oldTime;
        String oldTopic;
        String oldLink;
        //char[] oldCriteria;
        
        
        public EditItemTransaction(CourseSiteGeneratorApp initApp, ScheduleItem newItem, ScheduleItem oldItem) {
             this.app = initApp;
           
            type = newItem.getType();
            title = newItem.getTitle();
            date = newItem.getDate().toString();
            //time = newItem.getTime().toCharArray();
            
            topic = newItem.getTopic();
            link = newItem.getLink();
            //criteria = newItem.getCriteria().toCharArray();

            oldType = oldItem.getType();
            oldTitle = oldItem.getTitle();
            oldDate = oldItem.getDate().toString();
            //oldTime = oldItem.getTime().toCharArray();
            
            oldTopic = oldItem.getTopic();
            oldLink = oldItem.getLink();
            //oldCriteria = oldItem.getCriteria().toCharArray();            
                   
            
        }
        
        @Override
        public void doTransaction() {

            AppGUI gui = app.getGUI();
            gui.getFileController().markAsEdited(gui);
            ScheduleData scheduleData = ((CourseSiteGeneratorData)app.getDataComponent()).getScheduleData();
                     
            ScheduleItem item = new ScheduleItem((type), LocalDate.parse((date)), (title), (topic), (link));
            ScheduleItem oldItem = new ScheduleItem((oldType), LocalDate.parse((oldDate)), (oldTitle), (oldTopic), (oldLink));
            scheduleData.replaceItem(item, oldItem);
            ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getScheduleWorkspace().getScheduleTable().refresh();
                
        }

        @Override
        public void undoTransaction() {
            AppGUI gui = app.getGUI();
            gui.getFileController().markAsEdited(gui);
            ScheduleData scheduleData = ((CourseSiteGeneratorData)app.getDataComponent()).getScheduleData();
                     
            ScheduleItem item = new ScheduleItem((type), LocalDate.parse((date)), (title),(topic),(link));
            ScheduleItem oldItem = new ScheduleItem((oldType), LocalDate.parse((oldDate)),(oldTitle),(oldTopic),(oldLink));
            scheduleData.replaceItem(oldItem, item);            
        }
 
    }
    public static class RemoveItemTransaction implements jTPS_Transaction {
        CourseSiteGeneratorApp app;
        ScheduleItem toberemove;
        public RemoveItemTransaction(CourseSiteGeneratorApp app,ScheduleItem toberemove){
            this.app=app;
            this.toberemove=toberemove;
        }
        @Override
        public void doTransaction() {
            ScheduleData scheduleData = ((CourseSiteGeneratorData)app.getDataComponent()).getScheduleData();
            scheduleData.removeScheduleItem(toberemove.getType(), toberemove.getDate(), toberemove.getTitle(), toberemove.getTopic(), toberemove.getLink());
        }

        @Override
        public void undoTransaction() {
            ScheduleData scheduleData = ((CourseSiteGeneratorData)app.getDataComponent()).getScheduleData();
            
                scheduleData.addScheduleItem(toberemove.getType(), toberemove.getDate(), toberemove.getTitle(), toberemove.getTopic(), toberemove.getLink());
        
        }
    }
    public static class DateChangeTransaction implements jTPS_Transaction {
        CourseSiteGeneratorApp app;
        LocalDate oldsld;
        LocalDate oldeld;
        LocalDate sld;
        LocalDate eld;
        //ScheduleItem toberemove;
        public DateChangeTransaction(CourseSiteGeneratorApp app,LocalDate sld,LocalDate eld){
            ScheduleData scheduleData = ((CourseSiteGeneratorData)app.getDataComponent()).getScheduleData();
            oldsld=scheduleData.getStartingDate();
            oldeld=scheduleData.getEndingDate();
            this.app=app;
            //this.toberemove=toberemove;
            this.sld=sld;
            this.eld=eld;
        }
        @Override
        public void doTransaction() {
            CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        ScheduleWorkspace sworkspace = workspace.getScheduleWorkspace();
            ScheduleData scheduleData = ((CourseSiteGeneratorData)app.getDataComponent()).getScheduleData();
            scheduleData.dateChange(sld,eld);
            sworkspace.getStartDatePicker().setValue(sld);
            sworkspace.getEndDatePicker().setValue(eld);
            System.out.println(sld.toString());
            System.out.println(eld.toString());
            sworkspace.getStartDatePicker().setValue(sld);
            sworkspace.getEndDatePicker().setValue(eld);
        }

        @Override
        public void undoTransaction() {
            CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        ScheduleWorkspace sworkspace = workspace.getScheduleWorkspace();
            ScheduleData scheduleData = ((CourseSiteGeneratorData)app.getDataComponent()).getScheduleData();
            scheduleData.dateChange(oldsld,oldeld);
            System.out.println(oldsld.toString());
            System.out.println(oldeld.toString());
            sworkspace.getStartDatePicker().setValue(oldsld);
            sworkspace.getEndDatePicker().setValue(oldeld);
        }
        
    }
}
