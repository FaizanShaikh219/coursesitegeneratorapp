package csg.workspace.transactions;

import csg.CourseSiteGeneratorApp;
import csg.data.CourseSiteGeneratorData;
import csg.data.Labs;
import csg.data.Lectures;
import csg.data.MeetingTimesData;
import csg.data.Recitations;
import javafx.scene.control.TableColumn;
import jtps.jTPS_Transaction;

/**
 *
 * @author USER
 */
public class MTEditTransactions {
    
    Recitations rec;
    Labs lab;
    

    public static class EditLecTransaction implements jTPS_Transaction{

       CourseSiteGeneratorApp app;
        TableColumn.CellEditEvent<Lectures, String> t;
        String newValue;
        String oldValue;
        String type;
        public EditLecTransaction(CourseSiteGeneratorApp app,TableColumn.CellEditEvent<Lectures, String> t,String type) {
            this.app=app;
            this.t=t;
            newValue=t.getNewValue();
            oldValue=t.getOldValue();
            this.type=type;
            //System.out.print(newValue+" "+oldValue);
        }

        @Override
        public void doTransaction() {
            MeetingTimesData mtData = ((CourseSiteGeneratorData)app.getDataComponent()).getMeetingTimesData();
            mtData.editLec(t,newValue,type);
        }

        @Override
        public void undoTransaction() {
            MeetingTimesData mtData = ((CourseSiteGeneratorData)app.getDataComponent()).getMeetingTimesData();
            mtData.undoeditLec(t,oldValue,type);
        }
    }
    public static class EditRecTransaction implements jTPS_Transaction{

       CourseSiteGeneratorApp app;
        TableColumn.CellEditEvent<Recitations, String> t;
        String newValue;
        String oldValue;
        String type;
        public EditRecTransaction(CourseSiteGeneratorApp app,TableColumn.CellEditEvent<Recitations, String> t,String type) {
            this.app=app;
            this.t=t;
            newValue=t.getNewValue();
            oldValue=t.getOldValue();
            this.type=type;
            //System.out.print(newValue+" "+oldValue);
        }

        @Override
        public void doTransaction() {
            MeetingTimesData mtData = ((CourseSiteGeneratorData)app.getDataComponent()).getMeetingTimesData();
            mtData.editRec(t,newValue,type);
        }

        @Override
        public void undoTransaction() {
            MeetingTimesData mtData = ((CourseSiteGeneratorData)app.getDataComponent()).getMeetingTimesData();
            mtData.undoeditRec(t,oldValue,type);
        }
    }

    public static class EditLabTransaction implements jTPS_Transaction{

        
        CourseSiteGeneratorApp app;
        TableColumn.CellEditEvent<Labs, String> t;
        String newValue;
        String oldValue;
        String type;
        public EditLabTransaction(CourseSiteGeneratorApp app, TableColumn.CellEditEvent<Labs, String> t,String type) {
            this.app=app;
            this.t=t;
            newValue=t.getNewValue();
            oldValue=t.getOldValue();
            this.type=type;
            //System.out.print(newValue+" "+oldValue);
        }

        @Override
        public void doTransaction() {
            MeetingTimesData mtData = ((CourseSiteGeneratorData)app.getDataComponent()).getMeetingTimesData();
            mtData.editLab(t,newValue,type);
        }

        @Override
        public void undoTransaction() {
            MeetingTimesData mtData = ((CourseSiteGeneratorData)app.getDataComponent()).getMeetingTimesData();
            mtData.undoeditLab(t,oldValue,type);
        }
    
    
    }
    
}