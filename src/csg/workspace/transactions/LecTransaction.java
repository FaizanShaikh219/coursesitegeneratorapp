package csg.workspace.transactions;

import csg.CourseSiteGeneratorApp;
import csg.data.CourseSiteGeneratorData;
import csg.data.Labs;
import csg.data.Lectures;
import csg.data.MeetingTimesData;
import jtps.jTPS_Transaction;

/**
 *
 * @author USER
 */
public class LecTransaction implements jTPS_Transaction{
    
    Lectures lec;
    CourseSiteGeneratorApp app;
    public LecTransaction(CourseSiteGeneratorApp app, Lectures lec) {
        this.app=app;
        this.lec=lec;
    }

    

    @Override
    public void doTransaction() {
        MeetingTimesData mtData = ((CourseSiteGeneratorData)app.getDataComponent()).getMeetingTimesData();
            mtData.addLec(lec);
    }

    @Override
    public void undoTransaction() {
        MeetingTimesData mtData = ((CourseSiteGeneratorData)app.getDataComponent()).getMeetingTimesData();
            mtData.removeLec(lec);
    }
    
}
