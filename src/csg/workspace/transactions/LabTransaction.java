package csg.workspace.transactions;

import csg.CourseSiteGeneratorApp;
import csg.data.CourseSiteGeneratorData;
import csg.data.Labs;
import csg.data.MeetingTimesData;
import jtps.jTPS_Transaction;

/**
 *
 * @author USER
 */
public class LabTransaction implements jTPS_Transaction{
    Labs lab;
    CourseSiteGeneratorApp app;
    public LabTransaction(CourseSiteGeneratorApp app, Labs lab) {
        this.app=app;
        this.lab=lab;
    }

    @Override
    public void doTransaction() {
        MeetingTimesData mtData = ((CourseSiteGeneratorData)app.getDataComponent()).getMeetingTimesData();
            mtData.addLab(lab);
    }

    @Override
    public void undoTransaction() {
        MeetingTimesData mtData = ((CourseSiteGeneratorData)app.getDataComponent()).getMeetingTimesData();
            mtData.removeLab(lab);
    }
    
}
