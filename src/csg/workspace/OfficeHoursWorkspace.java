package csg.workspace;
/**
 *
 * @author Faizan Shaikh
 */
import csg.workspace.controller.TAController;
import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import csg.data.CourseSiteGeneratorData;
import csg.data.OHTimeSlot;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import djf.components.AppDataComponent;
import static djf.ui.AppGUI.ENABLED;
import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;


public class OfficeHoursWorkspace implements Tab {

    
        private CourseSiteGeneratorApp app;
        private SplitPane taPane;
        private TableView taTable;
        private TableView officeHoursTable;
        private GridPane taTablePane;
        private VBox officeHoursPane;
        
        private Label taTitleLabel;
        private Label officeHoursTitleLabel;
        private TextField addTATextField;
        private TextField addEmailTextField;
        private Button addTAButton;
        private Button clearTAButton;
        private Button removeTAButton;
        
        private ChoiceBox startTime;
        private ChoiceBox endTime;
        private TAController taController;
        
        
        boolean tableLock = false;
        
        public OfficeHoursWorkspace(CourseSiteGeneratorApp app) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();  

            this.app = app;
            
            taController = new TAController(app);
            
            initTATablePane();
            initOfficeHoursPane();
            initControls();
            
           taPane = new SplitPane(taTablePane, officeHoursPane);
           taPane.setDividerPositions(.40);
           taPane.setPadding(new Insets(10, 10, 10, 10));
        }
        
        @Override
    public SplitPane getComponentPane() { 
        return taPane;
    }

    private void initOfficeHoursPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager(); 
        officeHoursPane = new VBox(10);
        VBox.setVgrow(officeHoursPane, Priority.ALWAYS);
        HBox topBox = new HBox(10);
        topBox.setAlignment(Pos.CENTER_LEFT);

        officeHoursTitleLabel = new Label(props.getProperty(CourseSiteGeneratorProp.TA_OFFICE_HOURS_TEXT));
        topBox.getChildren().add(officeHoursTitleLabel);
        
        List timesList = buildTimesList();

        startTime = new ChoiceBox();
        endTime = new ChoiceBox();
        
        startTime.getItems().addAll(timesList);
        endTime.getItems().addAll(timesList);
        
        HBox spacer = new HBox();
        HBox.setHgrow(spacer, Priority.ALWAYS);

        topBox.getChildren().add(spacer);
        topBox.getChildren().add(new Label(props.getProperty(CourseSiteGeneratorProp.TA_START_TIME_TEXT)));
        topBox.getChildren().add(startTime);
        topBox.getChildren().add(new Label(props.getProperty(CourseSiteGeneratorProp.TA_END_TIME_TEXT)));
        topBox.getChildren().add(endTime);
        
        officeHoursPane.getChildren().add(topBox);
        
        TAData taData = ((CourseSiteGeneratorData)app.getDataComponent()).getTAData();
        officeHoursTable = new TableView();
        officeHoursTable.prefHeightProperty().bind(officeHoursPane.heightProperty().multiply(.9));
        
        officeHoursTable.setPlaceholder(new Label(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_EMPTY_TABLE_TEXT)));
        officeHoursTable.setEditable(true);
        officeHoursTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        officeHoursTable.getSelectionModel().setCellSelectionEnabled(true);

        //officeHoursTable.setItems(taData.getOfficeHours());
        officeHoursPane.getChildren().add(officeHoursTable);
        
        buildOfficeHoursTable();
    }
    private static ToggleGroup radiogroup;
    private void initTATablePane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();  
        TAData taData = ((CourseSiteGeneratorData)app.getDataComponent()).getTAData();

        taTablePane = new GridPane();
        taTablePane.setVgap(10);
        taTablePane.setHgap(10);
        
        //Setup Pane top title
        HBox taTitleBox = new HBox(10);
        taTitleBox.setAlignment(Pos.CENTER_LEFT);
        taTitleLabel = new Label(props.getProperty(CourseSiteGeneratorProp.TA_LIST_TITLE));        
        removeTAButton = new Button(props.getProperty(CourseSiteGeneratorProp.REMOVE_BUTTON_TEXT));
        removeTAButton.setDisable(true);
        taTitleBox.getChildren().addAll(taTitleLabel, removeTAButton); 
        taTablePane.add(taTitleBox, 0, 0, 3, 1);
 
        //Configure Table
        taTable = new TableView();
        
        taTable.setPlaceholder(new Label(props.getProperty(CourseSiteGeneratorProp.TA_EMPTY_TABLE_TEXT)));
        taTable.setEditable(true);
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        taTable.setItems(taData.getTeachingAssistants());
        radiogroup = new ToggleGroup();
        RadioButton rb1=new RadioButton();
        rb1.setText("All");
        rb1.setToggleGroup(radiogroup);
        rb1.setSelected(ENABLED);
        RadioButton rb2=new RadioButton();
        rb2.setText("Undergraduate");
        rb2.setToggleGroup(radiogroup);
        RadioButton rb3=new RadioButton();
        rb3.setText("Graduate");
        rb3.setToggleGroup(radiogroup);
        taTablePane.add(rb2, 1, 0);
        taTablePane.add(rb3, 2, 0);
        taTablePane.add(rb1, 3, 0);
        
        
        TableColumn<TeachingAssistant, String> nameColumn = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.TA_NAME_COLUMN_TEXT));
        TableColumn<TeachingAssistant, String> emailColumn = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.TA_EMAIL_COLUMN_TEXT));
        TableColumn<TeachingAssistant, String> typeColumn = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.TA_TYPE_COLUMN_TEXT));
        TableColumn<TeachingAssistant, String> slotColumn = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.TA_SLOT_COLUMN_TEXT));
        nameColumn.setCellValueFactory(new PropertyValueFactory("name"));
        emailColumn.setCellValueFactory(new PropertyValueFactory("email"));
        typeColumn.setCellValueFactory(new PropertyValueFactory("type"));
        slotColumn.setCellValueFactory(new PropertyValueFactory("slots"));
        nameColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/4.0));
        emailColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/3.0));
        typeColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(7.0/24.0));
        slotColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/8.0));

        

        
        
        
        taTable.getColumns().addAll( nameColumn, emailColumn, typeColumn, slotColumn);//,underGradColumn);
        
        taTablePane.add(taTable, 0, 1, 4, 1);
        //app.getGUI().addGUINode(OH_TAS_TABLE_VIEW, taTable);
        addTATextField = new TextField(); 
        addEmailTextField = new TextField();
        
        addTATextField.setPromptText(props.getProperty(CourseSiteGeneratorProp.TA_NAME_TEXT));
        addEmailTextField.setPromptText(props.getProperty(CourseSiteGeneratorProp.TA_EMAIL_TEXT));
        addEmailTextField.setDisable(ENABLED);
        addTATextField.setDisable(ENABLED);
        taTablePane.add(addTATextField, 0, 2);
        taTablePane.add(addEmailTextField, 1, 2);
        addTAButton = new Button(props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON_TEXT));
        addTAButton.setDisable(ENABLED);
        //clearTAButton = new Button(props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON_TEXT));
        taTablePane.add(addTAButton, 3,  2);
        //taTablePane.add(clearTAButton, 3, 2);
    }
    //initialize the functionality of all the controls
    private void initControls() {
        addTATextField.textProperty().addListener(e->{
            taController.processTypeTA("n",addTATextField.getText());
        });
        addEmailTextField.textProperty().addListener(e->{
            taController.processTypeTA("e",addEmailTextField.getText());
        });
        radiogroup.selectedToggleProperty().addListener(e->{
            if(radiogroup.getSelectedToggle()!=null){
                RadioButton rg=(RadioButton)radiogroup.getSelectedToggle();
                if(rg.getText().equals("All")){
                    addTATextField.setDisable(ENABLED);
                    addEmailTextField.setDisable(ENABLED);
                    addTAButton.setDisable(ENABLED);
                    taController.processSelectAllTAs();
                }
                else if(rg.getText().equals("Undergraduate")){
                    addTATextField.setDisable(false);
                    addEmailTextField.setDisable(false);
                    addTAButton.setDisable(false);
                    taController.processSelectUndergradTAs();
                }
                else if(rg.getText().equals("Graduate")){
                    addTATextField.setDisable(false);
                    addEmailTextField.setDisable(false);
                    addTAButton.setDisable(false);
                    taController.processSelectGradTAs();
                }
                //controller.typeChange(rg);
            }
        });
        addTAButton.setOnAction(e -> {
            
            //if (taTable.getSelectionModel().getSelectedItem() == null) {
                taController.handleAddTA();
            //}
            //else {
            //    taController.handleEditTA();
            //}
        });
        //clearTAButton.setOnAction(e -> taController.handleClearTA());
        removeTAButton.setOnAction(e -> taController.handleRemoveTA());
        taTable.setOnMouseClicked(ev->{
                if(ev.getClickCount()==1){
                taController.highlight();
                removeTAButton.setDisable(false);
                //taController.setCutTrue();
                
            }
                else if(ev.getClickCount()==2){
                RadioButton r = (RadioButton)radiogroup.getSelectedToggle();
                taController.openDialogBox(r.getText());
            }
            
            });
        //taTable.getSelectionModel().selectedItemProperty().addListener(e ->{ 
          //      taController.handleSelectTA();
        //});
        
        officeHoursTable.getFocusModel().focusedCellProperty().addListener(e -> {taController.handleOfficeHoursToggle();});
        startTime.setOnAction(e -> taController.handleOfficeHourStartTimeChange());
        endTime.setOnAction(e -> taController.handleOfficeHourEndTimeChange());
     
    }
    
    
    void buildOfficeHoursTable() {
   
        //initialize variables
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ArrayList<String> timeHeaders = props.getPropertyOptionsList(CourseSiteGeneratorProp.OFFICE_HOURS_TABLE_HEADERS);
        ArrayList<String> dowHeaders = props.getPropertyOptionsList(CourseSiteGeneratorProp.DAYS_OF_WEEK);
        TAData taData = ((CourseSiteGeneratorData)app.getDataComponent()).getTAData();

 
        officeHoursTable.setItems(taData.getOfficeHours());
        TableColumn<OHTimeSlot, String> startTimeColumn = new TableColumn<>(timeHeaders.get(0));
        startTimeColumn.setSortable(false);
 
        startTimeColumn.setCellValueFactory(new PropertyValueFactory("tableStartTime"));
        startTimeColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(.15));
        
        TableColumn<OHTimeSlot, String> endTimeColumn = new TableColumn<>(timeHeaders.get(1));
        endTimeColumn.setCellValueFactory(new PropertyValueFactory("tableEndTime"));
        endTimeColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(.15));
        endTimeColumn.setSortable(false);
        
        officeHoursTable.getColumns().addAll(startTimeColumn, endTimeColumn);          

        for (String header : dowHeaders) {
            TableColumn<OHTimeSlot, String> column = new TableColumn<>(header);
            column.setCellValueFactory(param -> param.getValue().getDayTimeSlot(header));
            column.prefWidthProperty().bind(taTable.widthProperty().multiply(.25));
            column.setSortable(false);
            officeHoursTable.getColumns().add(column);
            
        }
        taData.buildOfficeHourTimeSlots();
    
    }
    
    private List buildTimesList() {
        
        ArrayList list = new ArrayList();

        for (int i = 0; i < 24; i++) {   
            list.add(OHTimeSlot.formatTime(i, 00));
        }
        return list;   
    }
    
    public void setTableLock(boolean value) {
        tableLock = value;
    }
    public boolean isTableLocked() {
        return tableLock;
    }

   
    public TableView getTaTable() {
        return taTable;
    }
    public ToggleGroup getToggleGroup() {
        return radiogroup;
    }

    /**
     * @return the officeHoursTable
     */
    public TableView getOfficeHoursTable() {
        return officeHoursTable;
    }

    /**
     * @return the taTablePane
     */
    public GridPane getTaTablePane() {
        return taTablePane;
    }

    /**
     * @return the officeHoursPane
     */
    public VBox getOfficeHoursPane() {
        return officeHoursPane;
    }

    /**
     * @return the taTitleLabel
     */
    public Label getTaTitleLabel() {
        return taTitleLabel;
    }

    /**
     * @return the officeHoursTitleLabel
     */
    public Label getOfficeHoursTitleLabel() {
        return officeHoursTitleLabel;
    }

    /**
     * @return the addTATextField
     */
    public TextField getAddTATextField() {
        return addTATextField;
    }

    /**
     * @return the addEmailTextField
     */
    public TextField getAddEmailTextField() {
        return addEmailTextField;
    }

    /**
     * @return the addTAButton
     */
    public Button getAddTAButton() {
        return addTAButton;
    }

    /**
     * @return the clearTAButton
     */
    public Button getClearTAButton() {
        return clearTAButton;
    }

    /**
     * @return the removeTAButton
     */
    public Button getRemoveTAButton() {
        return removeTAButton;
    }

    /**
     * @return the startTime
     */
    public ChoiceBox getStartTime() {
        return startTime;
    }

    /**
     * @return the endTime
     */
    public ChoiceBox getEndTime() {
        return endTime;
    }

    void reloadWorkspace(AppDataComponent dataComponent) {
        TAData taData = ((CourseSiteGeneratorData)dataComponent).getTAData();

        startTime.setValue(OHTimeSlot.formatTime(taData.getStartHour(), 0));
        endTime.setValue(OHTimeSlot.formatTime(taData.getEndHour(), 0));
        addTATextField.clear();
        addEmailTextField.clear();
        
        
    }
    public void resetWorkspace() {
        this.getTaTable().getItems().clear();
        this.getOfficeHoursTable().getItems().clear();
        this.getAddEmailTextField().clear();
        this.getAddTATextField().clear();
    }

    void handleDeleteKeyEvent() {
        
        taController.handleRemoveTA();

    }
   /* private CourseSiteGeneratorApp app;
    public TAWorkspace(CourseSiteGeneratorApp app) {
        this.app=app;

        // LAYOUT THE APP
        initLayout();

        // INIT THE EVENT HANDLERS
        initControllers();

        // 
        initFoolproofDesign();

        // INIT DIALOGS
        initDialogs();
    }

    private void initDialogs() {
        TADialog taDialog = new TADialog((CourseSiteGeneratorApp) app);
        app.getGUI().addDialog(OH_TA_EDIT_DIALOG, taDialog);
    }

    // THIS HELPER METHOD INITIALIZES ALL THE CONTROLS IN THE WORKSPACE
    private void initLayout() {
        // FIRST LOAD THE FONT FAMILIES FOR THE COMBO BOX
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // THIS WILL BUILD ALL OF OUR JavaFX COMPONENTS FOR US
        AppNodesBuilder ohBuilder = app.getGUI().getNodesBuilder();

        // INIT THE HEADER ON THE LEFT
        VBox leftPane = ohBuilder.buildVBox(OH_LEFT_PANE, null, CLASS_OH_PANE, ENABLED);
        HBox tasHeaderBox = ohBuilder.buildHBox(OH_TAS_HEADER_PANE, leftPane, CLASS_OH_BOX, ENABLED);
        ohBuilder.buildLabel(CourseSiteGeneratorProp.OH_TAS_HEADER_LABEL, tasHeaderBox, CLASS_OH_HEADER_LABEL, ENABLED);
        HBox typeHeaderBox = ohBuilder.buildHBox(OH_GRAD_UNDERGRAD_TAS_PANE, tasHeaderBox, CLASS_OH_RADIO_BOX, ENABLED);
        ToggleGroup tg = new ToggleGroup();
        ohBuilder.buildRadioButton(OH_ALL_RADIO_BUTTON, typeHeaderBox, CLASS_OH_RADIO_BUTTON, ENABLED, tg, true);
        ohBuilder.buildRadioButton(OH_GRAD_RADIO_BUTTON, typeHeaderBox, CLASS_OH_RADIO_BUTTON, ENABLED, tg, false);
        ohBuilder.buildRadioButton(OH_UNDERGRAD_RADIO_BUTTON, typeHeaderBox, CLASS_OH_RADIO_BUTTON, ENABLED, tg, false);

        // MAKE THE TABLE AND SETUP THE DATA MODEL
        TableView<TeachingAssistant> taTable = ohBuilder.buildTableView(OH_TAS_TABLE_VIEW, leftPane, CLASS_OH_TABLE_VIEW, ENABLED);
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        TableColumn nameColumn = ohBuilder.buildTableColumn(OH_NAME_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        TableColumn emailColumn = ohBuilder.buildTableColumn(OH_EMAIL_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        TableColumn slotsColumn = ohBuilder.buildTableColumn(OH_SLOTS_TABLE_COLUMN, taTable, CLASS_OH_CENTERED_COLUMN);
        TableColumn typeColumn = ohBuilder.buildTableColumn(OH_TYPE_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        nameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<String, String>("email"));
        slotsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("slots"));
        typeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("type"));
        nameColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0 / 5.0));
        emailColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(2.0 / 5.0));
        slotsColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0 / 5.0));
        typeColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0 / 5.0));

        // ADD BOX FOR ADDING A TA
        HBox taBox = ohBuilder.buildHBox(OH_ADD_TA_PANE, leftPane, CLASS_OH_PANE, ENABLED);
        ohBuilder.buildTextField(OH_NAME_TEXT_FIELD, taBox, CLASS_OH_TEXT_FIELD, ENABLED);
        ohBuilder.buildTextField(OH_EMAIL_TEXT_FIELD, taBox, CLASS_OH_TEXT_FIELD, ENABLED);
        ohBuilder.buildTextButton(OH_ADD_TA_BUTTON, taBox, CLASS_OH_BUTTON, !ENABLED);

        // MAKE SURE IT'S THE TABLE THAT ALWAYS GROWS IN THE LEFT PANE
        VBox.setVgrow(taTable, Priority.ALWAYS);

        // INIT THE HEADER ON THE RIGHT
        VBox rightPane = ohBuilder.buildVBox(OH_RIGHT_PANE, null, CLASS_OH_PANE, ENABLED);
        HBox officeHoursHeaderBox = ohBuilder.buildHBox(OH_OFFICE_HOURS_HEADER_PANE, rightPane, CLASS_OH_PANE, ENABLED);
        ohBuilder.buildLabel(OH_OFFICE_HOURS_HEADER_LABEL, officeHoursHeaderBox, CLASS_OH_HEADER_LABEL, ENABLED);

        // SETUP THE OFFICE HOURS TABLE
        TableView<TimeSlot> officeHoursTable = ohBuilder.buildTableView(OH_OFFICE_HOURS_TABLE_VIEW, rightPane, CLASS_OH_OFFICE_HOURS_TABLE_VIEW, ENABLED);
        setupOfficeHoursColumn(OH_START_TIME_TABLE_COLUMN, officeHoursTable, CLASS_OH_TIME_COLUMN, "startTime");
        setupOfficeHoursColumn(OH_END_TIME_TABLE_COLUMN, officeHoursTable, CLASS_OH_TIME_COLUMN, "endTime");
        setupOfficeHoursColumn(OH_MONDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN, "monday");
        setupOfficeHoursColumn(OH_TUESDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN, "tuesday");
        setupOfficeHoursColumn(OH_WEDNESDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN, "wednesday");
        setupOfficeHoursColumn(OH_THURSDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN, "thursday");
        setupOfficeHoursColumn(OH_FRIDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN, "friday");

        // MAKE SURE IT'S THE TABLE THAT ALWAYS GROWS IN THE LEFT PANE
        VBox.setVgrow(officeHoursTable, Priority.ALWAYS);

        // BOTH PANES WILL NOW GO IN A SPLIT PANE
        SplitPane sPane = new SplitPane(leftPane, rightPane);
        sPane.setDividerPositions(.4);
        workspace = new BorderPane();

        // AND PUT EVERYTHING IN THE WORKSPACE
        ((BorderPane) workspace).setCenter(sPane);
    }

    private void setupOfficeHoursColumn(Object columnId, TableView tableView, String styleClass, String columnDataProperty) {
        AppNodesBuilder builder = app.getGUIModule().getNodesBuilder();
        TableColumn<TeachingAssistantPrototype, String> column = builder.buildTableColumn(columnId, tableView, styleClass);
        column.setCellValueFactory(new PropertyValueFactory<TeachingAssistantPrototype, String>(columnDataProperty));
        column.prefWidthProperty().bind(tableView.widthProperty().multiply(1.0 / 7.0));
        column.setCellFactory(col -> {
            return new TableCell<TeachingAssistantPrototype, String>() {
                @Override
                protected void updateItem(String text, boolean empty) {
                    super.updateItem(text, empty);
                    if (text == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        // CHECK TO SEE IF text CONTAINS THE NAME OF
                        // THE CURRENTLY SELECTED TA
                        setText(text);
                        TableView<TeachingAssistantPrototype> tasTableView = (TableView) app.getGUIModule().getGUINode(OH_TAS_TABLE_VIEW);
                        TeachingAssistantPrototype selectedTA = tasTableView.getSelectionModel().getSelectedItem();
                        if (selectedTA == null) {
                            setStyle("");
                        } else if (text.contains(selectedTA.getName())) {
                            setStyle("-fx-background-color: yellow");
                        } else {
                            setStyle("");
                        }
                    }
                }
            };
        });
    }

    public void initControllers() {
        OfficeHoursController controller = new OfficeHoursController((OfficeHoursApp) app);
        AppGUIModule gui = app.getGUIModule();

        // FOOLPROOF DESIGN STUFF
        TextField nameTextField = ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD));
        TextField emailTextField = ((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD));

        nameTextField.textProperty().addListener(e -> {
            controller.processTypeTA();
        });
        emailTextField.textProperty().addListener(e -> {
            controller.processTypeTA();
        });

        // FIRE THE ADD EVENT ACTION
        nameTextField.setOnAction(e -> {
            controller.processAddTA();
        });
        emailTextField.setOnAction(e -> {
            controller.processAddTA();
        });
        ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setOnAction(e -> {
            controller.processAddTA();
        });

        TableView officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        officeHoursTableView.getSelectionModel().setCellSelectionEnabled(true);
        officeHoursTableView.setOnMouseClicked(e -> {
            controller.processToggleOfficeHours();
        });

        // DON'T LET ANYONE SORT THE TABLES
        TableView tasTableView = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
        for (int i = 0; i < officeHoursTableView.getColumns().size(); i++) {
            ((TableColumn) officeHoursTableView.getColumns().get(i)).setSortable(false);
        }
        for (int i = 0; i < tasTableView.getColumns().size(); i++) {
            ((TableColumn) tasTableView.getColumns().get(i)).setSortable(false);
        }

        tasTableView.setOnMouseClicked(e -> {
            app.getFoolproofModule().updateAll();
            if (e.getClickCount() == 2) {
                controller.processEditTA();
            }
            controller.processSelectTA();
        });

        RadioButton allRadio = (RadioButton) gui.getGUINode(OH_ALL_RADIO_BUTTON);
        allRadio.setOnAction(e -> {
            controller.processSelectAllTAs();
        });
        RadioButton gradRadio = (RadioButton) gui.getGUINode(OH_GRAD_RADIO_BUTTON);
        gradRadio.setOnAction(e -> {
            controller.processSelectGradTAs();
        });
        RadioButton undergradRadio = (RadioButton) gui.getGUINode(OH_UNDERGRAD_RADIO_BUTTON);
        undergradRadio.setOnAction(e -> {
            controller.processSelectUndergradTAs();
        });
    }

    public void initFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        AppFoolproofModule foolproofSettings = app.getFoolproofModule();
        foolproofSettings.registerModeSettings(OH_FOOLPROOF_SETTINGS,
                new OfficeHoursFoolproofDesign((OfficeHoursApp) app));
    }
*/
    
}
