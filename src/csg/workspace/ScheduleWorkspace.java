package csg.workspace;
/**
 *
 * @author Faizan Shaikh
 */
import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import csg.data.CourseSiteGeneratorData;
//import csg.data.Recitation;
import csg.data.ScheduleData;
import csg.data.ScheduleItem;
//import csg.data.SitePage;
import csg.workspace.controller.ScheduleController;
import djf.components.AppDataComponent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;


public class ScheduleWorkspace implements Tab {
    private CourseSiteGeneratorApp app;
    
    private VBox scheduleBox;
    private ScheduleController scheduleController;
    private DatePicker startDatePicker;
    private DatePicker endDatePicker;
    private Button removeButton;
    private TableView<ScheduleItem> scheduleTable;
    private ChoiceBox<String> typeBox;
    private DatePicker itemDatePicker;
    private TextField timeTextField;
    private TextField titleTextField;
    private TextField topicTextField;
    private TextField linkTextField;
    private TextField criteriaTextField;
    private Button addButton;
    private Button clearButton;
    
    private Label boundariesLabel;
    private Label scheduleItemsLabel;
    private Label addItemLabel;
    
    
    private GridPane boundariesPane;
    private GridPane scheduleItemPane;
    private GridPane schedulePane;
    private VBox schVBox;
    
    /**
     * 
     * @param app The Application
     */
    public ScheduleWorkspace(CourseSiteGeneratorApp app) {
        this.app = app; 
        scheduleBox = new VBox(10);
        scheduleBox.setPadding(new Insets(10, 50, 10, 50));
        scheduleController = new ScheduleController(app);
        
        initCalanderBoundariesPane();
        initScheduleTablePane();
        initAddScheduleItemPane();
        initControls();
        
    }
    
    /**
     * Build pane for starting Monday, ending Friday choice boxes.
     */
    private void initCalanderBoundariesPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        boundariesPane = new GridPane();
        boundariesPane.setHgap(10);
        boundariesPane.setVgap(10);

        boundariesLabel = new Label(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_BOUNDARIES_TEXT));
        startDatePicker = new DatePicker();
        endDatePicker = new DatePicker();
        
        boundariesPane.add(boundariesLabel, 0, 0, 2, 1);
        boundariesPane.add(new Label(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_STARTING_MONDAY_TEXT)), 0, 1);
        boundariesPane.add(getStartDatePicker(), 1, 1);
        boundariesPane.add(new Label(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_ENDING_FRIDAY_TEXT)), 2, 1);
        boundariesPane.add(getEndDatePicker(), 3, 1);
        
        scheduleBox.getChildren().add(boundariesPane);
    }
    
    /**
     * Build pane for table of schedule items.
     */
    private void initScheduleTablePane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ScheduleData scheduleData = ((CourseSiteGeneratorData)app.getDataComponent()).getScheduleData();

        schedulePane = new GridPane();
        schedulePane.setHgap(10);
        schedulePane.setVgap(10);
        schVBox = new VBox();
        scheduleItemsLabel = new Label(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_ITEMS_TITLE));
        
        //setup table
        scheduleTable = new TableView<>();
        scheduleTable.prefWidthProperty().bind(schedulePane.widthProperty().multiply(.4));
        scheduleTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        scheduleTable.setPlaceholder(new Label(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_EMPTY_TABLE_TEXT)));
        scheduleTable.setEditable(true);
        scheduleTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        scheduleTable.setItems(scheduleData.getScheduleItems());
        
        //Add columns
        TableColumn<ScheduleItem, String> typeColumn = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_TYPE_COLUMN_TEXT));
        
        //Translate Schedule Item Type
        typeColumn.setCellValueFactory(e -> {
            String propertyName = e.getValue().getType();
            return new ReadOnlyObjectWrapper(props.getProperty(propertyName));
        });
        TableColumn<ScheduleItem, String> dateColumn = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_DATE_COLUMN_TEXT));
        
        dateColumn.setCellValueFactory(e -> new ReadOnlyObjectWrapper(e.getValue().getDate().format(DateTimeFormatter.ofPattern("MMMM d u"))));
        TableColumn<ScheduleItem, String> titleColumn = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_TITLE_COLUMN_TEXT));
        titleColumn.setCellValueFactory(new PropertyValueFactory("title"));
        TableColumn<ScheduleItem, String> topicColumn = new TableColumn<>(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_TOPIC_COLUMN_TEXT));
        topicColumn.setCellValueFactory(new PropertyValueFactory("topic"));       
        scheduleTable.getColumns().addAll(typeColumn, dateColumn, titleColumn, topicColumn);
        
        removeButton = new Button(props.getProperty(CourseSiteGeneratorProp.REMOVE_BUTTON_TEXT));
        removeButton.setDisable(true);
        
        schedulePane.add(scheduleItemsLabel, 0, 0);
        schedulePane.add(removeButton, 1, 0);
        schVBox.getChildren().add(schedulePane);
        schVBox.getChildren().add(scheduleTable);
        scheduleBox.getChildren().add(schVBox);
        
    }
    
    /**
     * Build pane for Schedule Item Table controls
     */
    private void initAddScheduleItemPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        scheduleItemPane = new GridPane();
        scheduleItemPane.setVgap(10);
        scheduleItemPane.setHgap(10);
        addItemLabel = new Label(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_ADD_TITLE));
        
        typeBox = new ChoiceBox<>();
        List<String> typeNames = props.getPropertyOptionsList(CourseSiteGeneratorProp.SCHEDULE_ITEM_PROPERTY_NAMES);
        for (String typeName : typeNames) {
            typeBox.getItems().add(props.getProperty(typeName));
            
        }
      
        itemDatePicker = new DatePicker();
        
        //timeTextField = new TextField();
        titleTextField = new TextField();
        topicTextField = new TextField();
        linkTextField = new TextField();
        //criteriaTextField = new TextField();
        addButton = new Button(props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON_TEXT));
        clearButton = new Button(props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON_TEXT));
        
        scheduleItemPane.add(addItemLabel, 0, 0);
        
        scheduleItemPane.add(new Label(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_TYPE_TEXT)), 0, 1);
        scheduleItemPane.add(typeBox, 1, 1);
        
        scheduleItemPane.add(new Label(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_DATE_TEXT)), 0, 2);        

        scheduleItemPane.add(itemDatePicker, 1, 2);

        //scheduleItemPane.add(new Label(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_TIME_TEXT)), 0, 3);        
        //scheduleItemPane.add(timeTextField, 1, 3);
        
        scheduleItemPane.add(new Label(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_ITEM_TITLE_TEXT)), 0, 3);        
        scheduleItemPane.add(titleTextField, 1, 3);
        
        scheduleItemPane.add(new Label(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_TOPIC_TEXT)), 0, 4);        
        scheduleItemPane.add(topicTextField, 1, 4);
        
        scheduleItemPane.add(new Label(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_LINK_TEXT)), 0, 5);
        scheduleItemPane.add(linkTextField, 1, 5);
        
        //scheduleItemPane.add(new Label(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_CRITERIA_TEXT)), 0, 7);        
        //scheduleItemPane.add(criteriaTextField, 1, 7);
        
        scheduleItemPane.add(addButton, 0, 6);
        scheduleItemPane.add(clearButton, 1, 6);

        
        scheduleBox.getChildren().add(getScheduleItemPane());
        
    }

    public void initControls() {
        removeButton.setOnMouseClicked(e->{
            scheduleController.removeItem();
            //scheduleTable.getSelectionModel().getSelectedItem();
        });
        scheduleTable.setOnMouseClicked(e->{
            scheduleController.tendToEdit(scheduleTable.getSelectionModel().getSelectedItem());
        });
        addButton.setOnAction(e -> {
            
            if (scheduleTable.getSelectionModel().getSelectedItem() == null) {
                scheduleController.handleAddItem();
            }
            else {
                scheduleController.handleEditItem();
            }
        });
        
        clearButton.setOnAction(e ->{ 
                scheduleController.handleClearItem();
        });
        //scheduleTable.getSelectionModel().selectedItemProperty().addListener(e -> {});//scheduleController.handleSelectItem());
        
        removeButton.setOnAction(e ->{});// scheduleController.handleRemoveItem());
        
        typeBox.setOnAction(e -> {});//scheduleController.handleTypeChange());
        startDatePicker.setOnAction(e ->{
            
            scheduleController.handleDateChange();
        });
        endDatePicker.setOnAction(e ->{
            scheduleController.handleDateChange();
        });
        

    }

    
    @Override
    public Region getComponentPane() {
        return scheduleBox;
    }

    /**
     * @return the startDatePicker
     */
    public DatePicker getStartDatePicker() {
        return startDatePicker;
    }

    /**
     * @return the endDatePicker
     */
    public DatePicker getEndDatePicker() {
        return endDatePicker;
    }

    /**
     * @return the removeButton
     */
    public Button getRemoveButton() {
        return removeButton;
    }

    /**
     * @return the scheduleTable
     */
    public TableView<ScheduleItem> getScheduleTable() {
        return scheduleTable;
    }

    /**
     * @return the typeBox
     */
    public ChoiceBox<String> getTypeBox() {
        return typeBox;
    }

    /**
     * @return the itemDatePicker
     */
    public DatePicker getItemDatePicker() {
        return itemDatePicker;
    }

    /**
     * @return the timeTextField
     */
    public TextField getTimeTextField() {
        return timeTextField;
    }

    /**
     * @return the titleTextField
     */
    public TextField getTitleTextField() {
        return titleTextField;
    }

    /**
     * @return the topicTextField
     */
    public TextField getTopicTextField() {
        return topicTextField;
    }

    /**
     * @return the linkTextField
     */
    public TextField getLinkTextField() {
        return linkTextField;
    }

    /**
     * @return the criteriaTextField
     */
    public TextField getCriteriaTextField() {
        return criteriaTextField;
    }

    /**
     * @return the addButton
     */
    public Button getAddButton() {
        return addButton;
    }

    /**
     * @return the clearButton
     */
    public Button getClearButton() {
        return clearButton;
    }

    /**
     * @return the boundariesLabel
     */
    public Label getBoundariesLabel() {
        return boundariesLabel;
    }

    /**
     * @return the scheduleItemsLabel
     */
    public Label getScheduleItemsLabel() {
        return scheduleItemsLabel;
    }

    /**
     * @return the addItemLabel
     */
    public Label getAddItemLabel() {
        return addItemLabel;
    }

    /**
     * @return the boundariesPane
     */
    public GridPane getBoundariesPane() {
        return boundariesPane;
    }

    /**
     * @return the scheduleItemPane
     */
    public GridPane getScheduleItemPane() {
        return scheduleItemPane;
    }
    public VBox getScheduleBox() {
        return schVBox;
    }

    /**
     * @return the schedulePane
     */
    public GridPane getSchedulePane() {
        return schedulePane;
    }

    public void reloadWorkspace(AppDataComponent dataComponent) {
        //scheduleController.setWorkspaceControlLock(true);

        ScheduleData scheduleData = ((CourseSiteGeneratorData)dataComponent).getScheduleData();
        startDatePicker.setValue(scheduleData.getStartingDate());
        endDatePicker.setValue(scheduleData.getEndingDate());
        setDateBoundaries(scheduleData.getStartingDate(), scheduleData.getEndingDate());
        //scheduleController.setWorkspaceControlLock(false);
    
    
    }
    

    void resetWorkspace() {
        //scheduleController.setWorkspaceControlLock(true);
        
        itemDatePicker.setValue(null);
        //timeTextField.clear();
        titleTextField.clear();
        topicTextField.clear();
        linkTextField.clear();
        //criteriaTextField.clear();
        
        typeBox.setValue(null);
        topicTextField.setDisable(false);
        linkTextField.setDisable(false);
        //criteriaTextField.setDisable(false);
        startDatePicker.setValue(LocalDate.now());
        endDatePicker.setValue(LocalDate.now());
        setMondayFridayLimits();

        //scheduleController.setWorkspaceControlLock(false);
       
        
    }



    public void setDateBoundaries(LocalDate start, LocalDate end) {               
        startDatePicker.setValue(start);
        endDatePicker.setValue(end);
    }
    
    public void setMondayFridayLimits() {
       
    }

    void handleDeleteKeyEvent() {
        
    }

}