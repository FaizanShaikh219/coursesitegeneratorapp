package csg.workspace;
/**
 *
 * @author Faizan Shaikh
 */
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;


public interface Tab {
    public Region getComponentPane();

}
