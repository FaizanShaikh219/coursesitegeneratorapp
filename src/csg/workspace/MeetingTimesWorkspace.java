package csg.workspace;

import csg.data.Recitations;
import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import csg.data.Labs;
import csg.data.Lectures;
import csg.data.TeachingAssistant;
import csg.workspace.controller.MeetingTimesController;
import djf.components.AppDataComponent;
import static djf.settings.AppPropertyType.ADD_ITEM;
import static djf.settings.AppPropertyType.REMOVE_ITEM;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
/**
 *
 * @author Faizan Shaikh
 */

public class MeetingTimesWorkspace implements Tab {
    
    private CourseSiteGeneratorApp app;
    private MeetingTimesController mtController;
    
    private VBox recitationBox;
    private Label recitationsLabel;
    private Label lecturesLabel;
    private Label labsLabel;
    private TableView<Recitations> recitationTable;
    private TableView<Lectures> lectureTable;
    private TableView<Labs> labTable;
    private Label addRecitationLabel;
    private TextField sectionTextField;
    private TextField instructorTextField;
    private TextField recitationDateTextField;
    private TextField locationTextField;
    private ComboBox<TeachingAssistant> mainTABox;
    private ComboBox<TeachingAssistant> assistantTABox;
    private Button lecRemoveButton;
    private Button recRemoveButton;
    private Button labRemoveButton;
    private Button lecAddButton;
    private Button recAddButton;
    private Button labAddButton;
    private Button clearButton;
    private GridPane recitationsPane;
    private GridPane lecturesPane;
    private GridPane labsPane;
    private VBox recVBox;
    private VBox lecVBox;
    private VBox labVBox;
    
    public MeetingTimesWorkspace(CourseSiteGeneratorApp app) {
        this.app = app; 
        //recitationController = new MeetingTimesController(app);
        mtController = new MeetingTimesController(app);
        recitationBox = new VBox(10);
        recitationBox.setPadding(new Insets(10, 50, 10, 50));
        initLecturesPane();
        initRecitationsPane();
        initLabPane();
        
        initControls();
    }
    
    /**
     * Build pane for recitations Table
     */
    
    
    /**
     * Build pane for recitation table controls.
     */
    /*private void initAddRecitationsPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();  
        addRecitationPane = new GridPane();       
        addRecitationPane.setVgap(10);
        addRecitationPane.setHgap(10);
        addRecitationLabel = new Label(props.getProperty(CourseSiteGeneratorProp.RECITATION_ADD_TITLE));
        recAddButton = new Button(props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON_TEXT));
        clearButton = new Button(props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON_TEXT));
        sectionTextField = new TextField();
        instructorTextField = new TextField();
        locationTextField = new TextField();
        recitationDateTextField = new TextField();
        mainTABox = new ComboBox();
        assistantTABox = new ComboBox();
        CourseSiteGeneratorData data = (CourseSiteGeneratorData)app.getDataComponent();
        
        
        mainTABox.setItems(data.getTAData().getTeachingAssistants());
        assistantTABox.setItems(data.getTAData().getTeachingAssistants());

        
        addRecitationPane.add(addRecitationLabel, 0, 0);
        
        addRecitationPane.add(new Label(props.getProperty(CourseSiteGeneratorProp.RECITATION_SECTION_TEXT)), 0, 1);
        addRecitationPane.add(new Label(props.getProperty(CourseSiteGeneratorProp.RECITATION_INSTRUCTOR_TEXT)), 0, 2);
        addRecitationPane.add(new Label(props.getProperty(CourseSiteGeneratorProp.RECITATION_DATE_TEXT)), 0, 3);
        addRecitationPane.add(new Label(props.getProperty(CourseSiteGeneratorProp.RECITATION_LOCATION_TEXT)), 0, 4);
        addRecitationPane.add(new Label(props.getProperty(CourseSiteGeneratorProp.RECITATION_SUPERVISING_TA_TEXT)), 0, 5);
        addRecitationPane.add(new Label(props.getProperty(CourseSiteGeneratorProp.RECITATION_SUPERVISING_TA_TEXT)), 0, 6);
        
        addRecitationPane.add(sectionTextField, 1, 1);
        addRecitationPane.add(instructorTextField, 1, 2);
        addRecitationPane.add(recitationDateTextField, 1, 3);
        addRecitationPane.add(locationTextField, 1, 4);
        addRecitationPane.add(mainTABox, 1, 5);
        addRecitationPane.add(assistantTABox, 1, 6);
        addRecitationPane.add(recAddButton, 0, 7);
        addRecitationPane.add(clearButton, 1, 7);

        recitationBox.getChildren().add(addRecitationPane);
    }*/
    
       
    public void initControls() {
        lecAddButton.setOnAction(e->{
            mtController.handleAddLec();
        });
        labAddButton.setOnAction(e->{
            mtController.handleAddLab();
        });
        recAddButton.setOnAction(e->{
            mtController.handleAddRec();
        });
        lecRemoveButton.setOnAction(e->{
            
            mtController.handleRemoveLec();
        });
        recRemoveButton.setOnAction(e->{
            mtController.handleRemoveRec();
        });
        labRemoveButton.setOnAction(e->{
            mtController.handleRemoveLab();
        });
        
        
    }
    @Override
    public Region getComponentPane() {
        return recitationBox;
    }

    /**
     * @return the recitationBox
     */
    public VBox getRecitationBox() {
        return recitationBox;
    }
    public VBox getRecBox() {
        return recVBox;
    }
    public VBox getLecBox() {
        return lecVBox;
    }
    public VBox getLabBox() {
        return labVBox;
    }
    

    /**
     * @return the recitationsLabel
     */
    public Label getRecitationsLabel() {
        return recitationsLabel;
    }
    public Label getLecturesLabel() {
        return lecturesLabel;
    }
    public Label getLabsLabel() {
        return labsLabel;
    }

    /**
     * @return the recitationTable
     */
    public TableView<Recitations> getRecitationTable() {
        return recitationTable;
    }
    public TableView<Labs> getLabTable() {
        return labTable;
    }
    public TableView getLectureTable() {
        return lectureTable;
    }

    /**
     * @return the addRecitationLabel
     */
    public Label getAddRecitationLabel() {
        return addRecitationLabel;
    }

    /**
     * @return the sectionTextField
     */
    public TextField getSectionTextField() {
        return sectionTextField;
    }

    /**
     * @return the instructorTextField
     */
    public TextField getInstructorTextField() {
        return instructorTextField;
    }

    /**
     * @return the recitationDate
     */
    public TextField getRecitationDateTextField() {
        return recitationDateTextField;
    }

    /**
     * @return the locationTextField
     */
    public TextField getLocationTextField() {
        return locationTextField;
    }

    /**
     * @return the mainTABox
     */
    public ComboBox<TeachingAssistant> getMainTABox() {
        return mainTABox;
    }

    /**
     * @return the assistantTABox
     */
    public ComboBox<TeachingAssistant> getAssistantTABox() {
        return assistantTABox;
    }

    /**
     * @return the removeButton
     */
    public Button getRecRemoveButton() {
        return recRemoveButton;
    }
    public Button getLecRemoveButton() {
        return lecRemoveButton;
    }
    public Button getLabRemoveButton() {
        return labRemoveButton;
    }

    /**
     * @return the addButton
     */
    public Button getRecAddButton() {
        return recAddButton;
    }
    public Button getLecAddButton() {
        return lecAddButton;
    }
    public Button getLabAddButton() {
        return labAddButton;
    }

    /**
     * @return the clearButton
     */
    public Button getClearButton() {
        return clearButton;
    }

    /**
     * @return the recitationsPane
     */
    public GridPane getRecitationsPane() {
        return recitationsPane;
    }
    public GridPane getLecturesPane() {
        return lecturesPane;
    }
    public GridPane getLabsPane() {
        return labsPane;
    }

    /**
     * @return the addRecitationPane
     */
    
    
    public void reloadWorkspace(AppDataComponent dataComponent) {


    }
    
    public void resetWorkspace() {
        //sectionTextField.clear();
        //instructorTextField.clear();
        //recitationDateTextField.clear();
        //locationTextField.clear();
        //mainTABox.setValue(null);
        //assistantTABox.setValue(null);
    }

    void handleDeleteKeyEvent() {
        
        //recitationController.handleRemoveItem();

    }

    private void initLecturesPane() {
PropertiesManager props = PropertiesManager.getPropertiesManager();  
        lecturesPane = new GridPane();
        lecturesPane.setVgap(10);
        lecturesPane.setHgap(10);
        lecVBox= new VBox();
        String imagePathplus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(ADD_ITEM);
        String imagePathminus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(REMOVE_ITEM);
        Image buttonImage1 = new Image(imagePathplus);
        Image buttonImage2 = new Image(imagePathminus);
	
	// NOW MAKE THE BUTTON
        //Button button = new Button();
        //button.setDisable(disabled);
        ImageView buttonView1 = new ImageView(buttonImage1);
        ImageView buttonView2 = new ImageView(buttonImage2);
        buttonView1.setFitHeight(20);
        buttonView1.setFitWidth(20);
        buttonView2.setFitHeight(20);
        buttonView2.setFitWidth(20);
        lecAddButton = new Button();
        lecAddButton.setGraphic(buttonView1);
        lecturesPane.add(lecAddButton, 0, 0);
        
        lecRemoveButton = new Button();
        lecRemoveButton.setGraphic(buttonView2);
        lecturesPane.add(lecRemoveButton, 1, 0);
        
        lecturesLabel = new Label(props.getProperty(CourseSiteGeneratorProp.LECTURE_TITLE));
        lecturesPane.add(lecturesLabel, 2, 0);
        
        

       // MeetingTimesData recitationData = ((CourseSiteGeneratorData)app.getDataComponent()).getMeetingTimesData();
        lectureTable = new TableView<>();
        lectureTable.prefWidthProperty().bind(lecturesPane.widthProperty().multiply(.5));
        lectureTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        //recitationTable.setPlaceholder(new Label(props.getProperty(CourseSiteGeneratorProp.RECITATION_EMPTY_TABLE_TEXT)));
        lectureTable.setEditable(true);
        lectureTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        
        //recitationTable.setItems(recitationData.getMeetingTimes());
        
        TableColumn<Lectures, String> sectionColumn = new TableColumn<>();
        sectionColumn.setText(props.getProperty(CourseSiteGeneratorProp.RECITATION_SECTION_COLUMN_TEXT));
        sectionColumn.setCellValueFactory(new PropertyValueFactory("section"));
        sectionColumn.setCellFactory(TextFieldTableCell.<Lectures>forTableColumn());
        sectionColumn.setOnEditCommit(
            (CellEditEvent<Lectures, String> t) -> {
                mtController.editLec(t,"section");
                
        });
        
        TableColumn<Lectures, String> dayColumn = new TableColumn<>();
        dayColumn.setText(props.getProperty(CourseSiteGeneratorProp.RECITATION_DAY_COLUMN_TEXT));
        dayColumn.setCellValueFactory(new PropertyValueFactory("days"));
        dayColumn.setCellFactory(TextFieldTableCell.<Lectures>forTableColumn());
        dayColumn.setOnEditCommit(
            (CellEditEvent<Lectures, String> t) -> {
                mtController.editLec(t,"days");
                
        });
        
        TableColumn<Lectures, String> timeColumn = new TableColumn<>();
        timeColumn.setText(props.getProperty(CourseSiteGeneratorProp.RECITATION_TIME_COLUMN_TEXT));
        timeColumn.setCellValueFactory(new PropertyValueFactory("time"));       
        timeColumn.setCellFactory(TextFieldTableCell.<Lectures>forTableColumn());
        timeColumn.setOnEditCommit(
            (CellEditEvent<Lectures, String> t) -> {
                mtController.editLec(t,"time");
                
        });
                
        TableColumn<Lectures, String> roomColumn = new TableColumn<>();
        roomColumn.setText(props.getProperty(CourseSiteGeneratorProp.RECITATION_LOCATION_COLUMN_TEXT));
        roomColumn.setCellValueFactory(new PropertyValueFactory("room"));                     
        roomColumn.setCellFactory(TextFieldTableCell.<Lectures>forTableColumn());
        roomColumn.setOnEditCommit(
            (CellEditEvent<Lectures, String> t) -> {
                mtController.editLec(t,"room");
               
        });
              
                                    
        lectureTable.getColumns().addAll(sectionColumn, dayColumn, roomColumn, timeColumn);
        lecVBox.getChildren().add(lecturesPane);
        lecVBox.getChildren().add(lectureTable);       
        recitationBox.getChildren().add(lecVBox);    }

    private void initLabPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();  
        labsPane = new GridPane();
        labsPane.setVgap(10);
        labsPane.setHgap(10);
        labVBox=new VBox();
        String imagePathplus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(ADD_ITEM);
        String imagePathminus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(REMOVE_ITEM);
        Image buttonImage1 = new Image(imagePathplus);
        Image buttonImage2 = new Image(imagePathminus);
	
	// NOW MAKE THE BUTTON
        //Button button = new Button();
        //button.setDisable(disabled);
        ImageView buttonView1 = new ImageView(buttonImage1);
        ImageView buttonView2 = new ImageView(buttonImage2);
        buttonView1.setFitHeight(20);
        buttonView1.setFitWidth(20);
        buttonView2.setFitHeight(20);
        buttonView2.setFitWidth(20);
        labAddButton = new Button();
        labAddButton.setGraphic(buttonView1);
        labsPane.add(labAddButton, 0, 0);
        
        labRemoveButton = new Button();
        labRemoveButton.setGraphic(buttonView2);
        labsPane.add(labRemoveButton, 1, 0);
        
        labsLabel = new Label(props.getProperty(CourseSiteGeneratorProp.LAB_TITLE));
        labsPane.add(labsLabel, 2, 0);
        
        

       // MeetingTimesData recitationData = ((CourseSiteGeneratorData)app.getDataComponent()).getMeetingTimesData();
        labTable = new TableView<>();
        labTable.prefWidthProperty().bind(labsPane.widthProperty().multiply(.5));
        labTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        //recitationTable.setPlaceholder(new Label(props.getProperty(CourseSiteGeneratorProp.RECITATION_EMPTY_TABLE_TEXT)));
        labTable.setEditable(true);
        labTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        //recitationTable.setItems(recitationData.getMeetingTimes());
        
        TableColumn<Labs, String> sectionColumn = new TableColumn<>();
        sectionColumn.setText(props.getProperty(CourseSiteGeneratorProp.RECITATION_SECTION_COLUMN_TEXT));
        sectionColumn.setCellValueFactory(new PropertyValueFactory("section"));
        sectionColumn.setCellFactory(TextFieldTableCell.<Labs>forTableColumn());
        sectionColumn.setOnEditCommit(
            (CellEditEvent<Labs, String> t) -> {
                mtController.editLab(t,"section");
                
        });
        
        TableColumn<Labs, String> dateColumn = new TableColumn<>();
        dateColumn.setText(props.getProperty(CourseSiteGeneratorProp.RECITATION_DATE_COLUMN_TEXT));
        dateColumn.setCellValueFactory(new PropertyValueFactory("daysandtime"));
        dateColumn.setCellFactory(TextFieldTableCell.<Labs>forTableColumn());
        dateColumn.setOnEditCommit(
            (CellEditEvent<Labs, String> t) -> {
                mtController.editLab(t,"days");
                
        });
        
        TableColumn<Labs, String> locationColumn = new TableColumn<>();
        locationColumn.setText(props.getProperty(CourseSiteGeneratorProp.RECITATION_LOCATION_COLUMN_TEXT));
        locationColumn.setCellValueFactory(new PropertyValueFactory("room"));       
        locationColumn.setCellFactory(TextFieldTableCell.<Labs>forTableColumn());
        locationColumn.setOnEditCommit(
            (CellEditEvent<Labs, String> t) -> {
                mtController.editLab(t,"room");
                
        });
        
        TableColumn<Labs, String> mainTAColumn = new TableColumn<>();
        mainTAColumn.setText(props.getProperty(CourseSiteGeneratorProp.RECITATION_TA1_COLUMN_TEXT));
        mainTAColumn.setCellValueFactory(new PropertyValueFactory("ta1"));                     
        mainTAColumn.setCellFactory(TextFieldTableCell.<Labs>forTableColumn());
        mainTAColumn.setOnEditCommit(
            (CellEditEvent<Labs, String> t) -> {
                mtController.editLab(t,"ta1");
                
        });
        
        TableColumn<Labs, String> assistantTAColumn = new TableColumn<>();
        assistantTAColumn.setText(props.getProperty(CourseSiteGeneratorProp.RECITATION_TA2_COLUMN_TEXT));
        assistantTAColumn.setCellValueFactory(new PropertyValueFactory("ta2"));    
        assistantTAColumn.setCellFactory(TextFieldTableCell.<Labs>forTableColumn());
        assistantTAColumn.setOnEditCommit(
            (CellEditEvent<Labs, String> t) -> {
                mtController.editLab(t,"ta2");
                
        });
        
        labTable.getColumns().addAll(sectionColumn, dateColumn, locationColumn, mainTAColumn, assistantTAColumn);
        labVBox.getChildren().add(labsPane);
        labVBox.getChildren().add(labTable);       
        recitationBox.getChildren().add(labVBox);    
    }
    
    
    private void initRecitationsPane() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();  
        recitationsPane = new GridPane();
        recVBox = new VBox();
        recitationsPane.setVgap(10);
        recitationsPane.setHgap(10);
        String imagePathplus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(ADD_ITEM);
        String imagePathminus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(REMOVE_ITEM);
        Image buttonImage1 = new Image(imagePathplus);
        Image buttonImage2 = new Image(imagePathminus);
	
	// NOW MAKE THE BUTTON
        //Button button = new Button();
        //button.setDisable(disabled);
        ImageView buttonView1 = new ImageView(buttonImage1);
        ImageView buttonView2 = new ImageView(buttonImage2);
        buttonView1.setFitHeight(20);
        buttonView1.setFitWidth(20);
        buttonView2.setFitHeight(20);
        buttonView2.setFitWidth(20);
        recAddButton = new Button();
        recAddButton.setGraphic(buttonView1);
        recitationsPane.add(recAddButton, 0, 0);
        
        recRemoveButton = new Button();
        recRemoveButton.setGraphic(buttonView2);
        recitationsPane.add(recRemoveButton, 1, 0);
        
        recitationsLabel = new Label(props.getProperty(CourseSiteGeneratorProp.RECITATION_TITLE));
        recitationsPane.add(recitationsLabel, 2, 0);
        
        

       // MeetingTimesData recitationData = ((CourseSiteGeneratorData)app.getDataComponent()).getMeetingTimesData();
        recitationTable = new TableView<>();
        recitationTable.prefWidthProperty().bind(recVBox.widthProperty().multiply(.5));
        recitationTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        //recitationTable.setPlaceholder(new Label(props.getProperty(CourseSiteGeneratorProp.RECITATION_EMPTY_TABLE_TEXT)));
        recitationTable.setEditable(true);
        recitationTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        //recitationTable.setItems(recitationData.getMeetingTimes());
        
        TableColumn<Recitations, String> sectionColumn = new TableColumn<>();
        sectionColumn.setText(props.getProperty(CourseSiteGeneratorProp.RECITATION_SECTION_COLUMN_TEXT));
        sectionColumn.setCellValueFactory(new PropertyValueFactory("section"));
        sectionColumn.setCellFactory(TextFieldTableCell.<Recitations>forTableColumn());
        sectionColumn.setOnEditCommit(
            (CellEditEvent<Recitations, String> t) -> {
                mtController.editRec(t,"section");
                
        });
        
        TableColumn<Recitations, String> dateColumn = new TableColumn<>();
        dateColumn.setText(props.getProperty(CourseSiteGeneratorProp.RECITATION_DATE_COLUMN_TEXT));
        dateColumn.setCellValueFactory(new PropertyValueFactory("daysandtime"));
        dateColumn.setCellFactory(TextFieldTableCell.<Recitations>forTableColumn());
        dateColumn.setOnEditCommit(
            (CellEditEvent<Recitations, String> t) -> {
                mtController.editRec(t,"days");
                
        });
        
        TableColumn<Recitations, String> locationColumn = new TableColumn<>();
        locationColumn.setText(props.getProperty(CourseSiteGeneratorProp.RECITATION_LOCATION_COLUMN_TEXT));
        locationColumn.setCellValueFactory(new PropertyValueFactory("room"));       
                locationColumn.setCellFactory(TextFieldTableCell.<Recitations>forTableColumn());
        locationColumn.setOnEditCommit(
            (CellEditEvent<Recitations, String> t) -> {
                mtController.editRec(t,"room");
                
        });
        
        TableColumn<Recitations, String> mainTAColumn = new TableColumn<>();
        mainTAColumn.setText(props.getProperty(CourseSiteGeneratorProp.RECITATION_TA1_COLUMN_TEXT));
        mainTAColumn.setCellValueFactory(new PropertyValueFactory("ta1"));                     
        mainTAColumn.setCellFactory(TextFieldTableCell.<Recitations>forTableColumn());
        mainTAColumn.setOnEditCommit(
            (CellEditEvent<Recitations, String> t) -> {
                mtController.editRec(t,"ta1");
                
        });
        
        TableColumn<Recitations, String> assistantTAColumn = new TableColumn<>();
        assistantTAColumn.setText(props.getProperty(CourseSiteGeneratorProp.RECITATION_TA2_COLUMN_TEXT));
        assistantTAColumn.setCellValueFactory(new PropertyValueFactory("ta2"));  
        assistantTAColumn.setCellFactory(TextFieldTableCell.<Recitations>forTableColumn());
        assistantTAColumn.setOnEditCommit(
            (CellEditEvent<Recitations, String> t) -> {
                mtController.editRec(t,"ta2");
                
        });
        
        recitationTable.getColumns().addAll(sectionColumn, dateColumn, locationColumn, mainTAColumn, assistantTAColumn);
        //recVBox.setPadding(new Insets(10,10,10,10));
        //recitationTable.setPadding(new Insets(10,10,10,10));
        recVBox.getChildren().add(recitationsPane);
        recVBox.getChildren().add(recitationTable);       
        recitationBox.getChildren().add(recVBox);
    }
}
        
