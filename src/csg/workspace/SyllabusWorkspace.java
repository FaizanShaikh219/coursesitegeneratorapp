package csg.workspace;
/**
 *
 * @author Faizan Shaikh
 */
import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import csg.data.CourseSiteGeneratorData;
import csg.data.SyllabusData;
import csg.workspace.controller.SyllabusController;
import djf.components.AppDataComponent;
import static djf.settings.AppPropertyType.ADD_ITEM;
import static djf.settings.AppPropertyType.REMOVE_ITEM;
import static djf.settings.AppStartupConstants.FILE_PROTOCOL;
import static djf.settings.AppStartupConstants.PATH_IMAGES;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 *
 * @author USER
 */
public class SyllabusWorkspace implements Tab {
    private VBox syllabusBox;
    private CourseSiteGeneratorApp app;
    private SyllabusController sController;
    private HBox desBox;
    private String desText;
    private Label desLabel;
    private TextArea desArea;
    private VBox desPane;
    private Button desButt;
    private HBox topBox;
    private String topText;
    private Label topLabel;
    private TextArea topArea;
    private Button topButt;
    private VBox topPane;
    private HBox preBox;
    private String preText;
    private Label preLabel;
    private TextArea preArea;
    private Button preButt;
    private VBox prePane;
    private HBox outBox;
    private String outText;
    private Label outLabel;
    private TextArea outArea;
    private Button outButt;
    private VBox outPane;
    private HBox bookBox;
    private String bookText;
    private Label bookLabel;
    private TextArea textArea;
    private Button bookButt;
    private VBox bookPane;
    private HBox gcBox;
    private String gcText;
    private Label gcLabel;
    private TextArea gcArea;
    private Button gcButt;
    private VBox gcPane;
    private HBox gnBox;
    private String gnText;
    private Label gnLabel;
    private TextArea gnArea;
    private Button gnButt;
    private VBox gnPane;
    private HBox adBox;
    private String adText;
    private Label adLabel;
    private TextArea adArea;
    private Button adButt;
    private VBox adPane;
    private HBox saBox;
    private String saText;
    private Label saLabel;
    private TextArea saArea;
    private Button saButt;
    private VBox saPane;
    public SyllabusWorkspace(CourseSiteGeneratorApp app) {
       
        this.app = app;   
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        sController = new SyllabusController(app);
        syllabusBox = new VBox(10);
        syllabusBox.setPadding(new Insets(20, 50, 20, 50));
        //SyllabusData data=
        String desPathplus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(ADD_ITEM);
        String desPathminus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(REMOVE_ITEM);
        Image desImage1 = new Image(desPathplus);
        Image desImage2 = new Image(desPathminus);
	desBox=new HBox();
        ImageView desView1 = new ImageView(desImage1);
        ImageView desView2 = new ImageView(desImage2);
        desView1.setFitHeight(20);
        desView1.setFitWidth(20);
        desView2.setFitHeight(20);
        desView2.setFitWidth(20);
        
        desPane=new VBox();
        desButt=new Button();
        desText="";
        desLabel=new Label(props.getProperty(CourseSiteGeneratorProp.DES_LABEL).toString());
        desArea= new TextArea();
        //desArea.setPrefColumnCount(8);
        desArea.setPrefRowCount(8);
        desArea.setPadding(new Insets(5,5,5,5));
        desButt.setGraphic(desView1);
        desBox.getChildren().add(desButt);
        //getDesPane().getChildren().add(desButt);
        desArea.setVisible(false);
        desBox.getChildren().add(desLabel);
        getDesPane().getChildren().add(desBox);
        //getDesPane().add(desArea, 0, 1, 2, 1);
        
        syllabusBox.getChildren().add(desPane);
        desButt.setOnMouseClicked(e->{
            if(desArea.isVisible()){
                desButt.setGraphic(desView1);
                desText=desArea.getText();
                desArea.setVisible(false);
                getDesPane().getChildren().remove(desArea);
            }else{
                //desArea.setText();
                desButt.setGraphic(desView2);
                desArea.setText(desText);
                getDesPane().getChildren().add(desArea);
                desArea.setVisible(true);
            }
        });
        topBox =new HBox();
        String tPathplus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(ADD_ITEM);
        String tPathminus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(REMOVE_ITEM);
        Image tImage1 = new Image(tPathplus);
        Image tImage2 = new Image(tPathminus);
	
        ImageView tView1 = new ImageView(tImage1);
        ImageView tView2 = new ImageView(tImage2);
        tView1.setFitHeight(20);
        tView1.setFitWidth(20);
        tView2.setFitHeight(20);
        tView2.setFitWidth(20);
        topPane=new VBox();
        topButt=new Button();
        topText="";
        topLabel=new Label(props.getProperty(CourseSiteGeneratorProp.TOP_LABEL).toString());
        topArea= new TextArea();
        //desArea.setPrefColumnCount(8);
        topArea.setPrefRowCount(8);
        topArea.setPadding(new Insets(5,5,5,5));
        topButt.setGraphic(tView1);
        topBox.getChildren().addAll(topButt,topLabel);//.add(topButt)
        //getTopPane().add(topBox, 0, 0);
        topArea.setVisible(false);
        getTopPane().getChildren().add(topBox);
        //getDesPane().add(desArea, 0, 1, 2, 1);
        
        syllabusBox.getChildren().add(topPane);
        topButt.setOnMouseClicked(e->{
            if(topArea.isVisible()){
                topButt.setGraphic(tView1);
                topText=topArea.getText();
                topArea.setVisible(false);
                
                getTopPane().getChildren().remove(topArea);
            }else{
                topButt.setGraphic(tView2);
                topArea.setText(topText);
                //topBox.getChildren().add(topArea);
                getTopPane().getChildren().add(topArea);
                topArea.setVisible(true);
            }
        });
        String pPathplus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(ADD_ITEM);
        String pPathminus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(REMOVE_ITEM);
        Image pImage1 = new Image(pPathplus);
        Image pImage2 = new Image(pPathminus);
	
        ImageView pView1 = new ImageView(pImage1);
        ImageView pView2 = new ImageView(pImage2);
        pView1.setFitHeight(20);
        pView1.setFitWidth(20);
        pView2.setFitHeight(20);
        pView2.setFitWidth(20);
        prePane=new VBox();
        preButt=new Button();
        preText="";
        preLabel=new Label(props.getProperty(CourseSiteGeneratorProp.PRE_LABEL).toString());
        preArea= new TextArea();
        //desArea.setPrefColumnCount(8);
        preArea.setPrefRowCount(8);
        preArea.setPadding(new Insets(5,5,5,5));
        preButt.setGraphic(pView1);
        //getPrePane().add(preButt, 0, 0);
        preBox=new HBox();
        preBox.getChildren().addAll(preButt,preLabel);
        preArea.setVisible(false);
        getPrePane().getChildren().add(preBox);
        //getDesPane().add(desArea, 0, 1, 2, 1);
        
        syllabusBox.getChildren().add(prePane);
        preButt.setOnMouseClicked(e->{
            if(preArea.isVisible()){
                preButt.setGraphic(pView1);
                preText=preArea.getText();
                preArea.setVisible(false);
                getPrePane().getChildren().remove(preArea);
            }else{
                preButt.setGraphic(pView2);
                preArea.setText(preText);
                getPrePane().getChildren().add(preArea);
                preArea.setVisible(true);
            }
        });
        String oPathplus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(ADD_ITEM);
        String oPathminus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(REMOVE_ITEM);
        Image oImage1 = new Image(oPathplus);
        Image oImage2 = new Image(oPathminus);
	
        ImageView oView1 = new ImageView(oImage1);
        ImageView oView2 = new ImageView(oImage2);
        oView1.setFitHeight(20);
        oView1.setFitWidth(20);
        oView2.setFitHeight(20);
        oView2.setFitWidth(20);
        outPane=new VBox();
        outButt=new Button();
        outText="";
        outLabel=new Label(props.getProperty(CourseSiteGeneratorProp.OUT_LABEL).toString());
        outArea= new TextArea();
        //desArea.setPrefColumnCount(8);
        outArea.setPrefRowCount(8);
        outArea.setPadding(new Insets(5,5,5,5));
        outButt.setGraphic(oView1);
        //getOutPane().add(outButt, 0, 0);
        outBox=new HBox();
        outBox.getChildren().addAll(outButt,outLabel);
        outArea.setVisible(false);
        getOutPane().getChildren().add(outBox);
        //getDesPane().add(desArea, 0, 1, 2, 1);
        
        syllabusBox.getChildren().add(outPane);
        outButt.setOnMouseClicked(e->{
            if(outArea.isVisible()){
                outButt.setGraphic(oView1);
                outText=outArea.getText();
                outArea.setVisible(false);
                getOutPane().getChildren().remove(outArea);
            }else{
                outButt.setGraphic(oView2);
                outArea.setText(outText);
                getOutPane().getChildren().add(outArea);
                outArea.setVisible(true);
            }
        });
        String bPathplus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(ADD_ITEM);
        String bPathminus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(REMOVE_ITEM);
        Image bImage1 = new Image(bPathplus);
        Image bImage2 = new Image(bPathminus);
	
        ImageView bView1 = new ImageView(bImage1);
        ImageView bView2 = new ImageView(bImage2);
        bView1.setFitHeight(20);
        bView1.setFitWidth(20);
        bView2.setFitHeight(20);
        bView2.setFitWidth(20);
        bookPane=new VBox();
        bookButt=new Button();
        bookText="";
        bookLabel=new Label(props.getProperty(CourseSiteGeneratorProp.BOOK_LABEL).toString());
        textArea= new TextArea();
        //desArea.setPrefColumnCount(8);
        textArea.setPrefRowCount(8);
        textArea.setPadding(new Insets(5,5,5,5));
         bookButt.setGraphic(bView1);
        //getBookPane().add(bookButt, 0, 0);
        bookBox=new HBox();
        bookBox.getChildren().addAll(bookButt,bookLabel);
        textArea.setVisible(false);
        getBookPane().getChildren().add(bookBox);
        //getDesPane().add(desArea, 0, 1, 2, 1);
       
        syllabusBox.getChildren().add(bookPane);
        bookButt.setOnMouseClicked(e->{
            if(textArea.isVisible()){
                bookButt.setGraphic(bView1);
                bookText=textArea.getText();
                textArea.setVisible(false);
                getBookPane().getChildren().remove(textArea);
            }else{
                bookButt.setGraphic(bView2);
                textArea.setText(bookText);
                getBookPane().getChildren().add(textArea);
                textArea.setVisible(true);
            }
        });
        String gcPathplus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(ADD_ITEM);
        String gcPathminus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(REMOVE_ITEM);
        Image gcImage1 = new Image(gcPathplus);
        Image gcImage2 = new Image(gcPathminus);
	
        ImageView gcView1 = new ImageView(gcImage1);
        ImageView gcView2 = new ImageView(gcImage2);
        gcView1.setFitHeight(20);
        gcView1.setFitWidth(20);
        gcView2.setFitHeight(20);
        gcView2.setFitWidth(20);
        gcPane=new VBox();
        gcButt=new Button();
        gcText="";
        gcLabel=new Label(props.getProperty(CourseSiteGeneratorProp.GC_LABEL).toString());
        gcArea= new TextArea();
        //desArea.setPrefColumnCount(8);
        gcArea.setPrefRowCount(8);
        gcArea.setPadding(new Insets(5,5,5,5));
        gcButt.setGraphic(gcView1);
        //getGCPane().add(gcButt, 0, 0);
        gcBox=new HBox();
        gcBox.getChildren().addAll(gcButt,gcLabel);
        gcArea.setVisible(false);
        getGCPane().getChildren().add(gcBox);
        //getDesPane().add(desArea, 0, 1, 2, 1);
        
        syllabusBox.getChildren().add(gcPane);
        gcButt.setOnMouseClicked(e->{
            if(gcArea.isVisible()){
                gcButt.setGraphic(gcView1);
                gcText=gcArea.getText();
                gcArea.setVisible(false);
                getGCPane().getChildren().remove(gcArea);
            }else{
                gcButt.setGraphic(gcView2);
                gcArea.setText(gcText);
                getGCPane().getChildren().add(gcArea);
                gcArea.setVisible(true);
            }
        });
        String gnPathplus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(ADD_ITEM);
        String gnPathminus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(REMOVE_ITEM);
        Image gnImage1 = new Image(gnPathplus);
        Image gnImage2 = new Image(gnPathminus);
	
        ImageView gnView1 = new ImageView(gnImage1);
        ImageView gnView2 = new ImageView(gnImage2);
        gnView1.setFitHeight(20);
        gnView1.setFitWidth(20);
        gnView2.setFitHeight(20);
        gnView2.setFitWidth(20);
        gnPane=new VBox();
        gnButt=new Button();
        gnText="";
        gnLabel=new Label(props.getProperty(CourseSiteGeneratorProp.GN_LABEL).toString());
        gnArea= new TextArea();
        //desArea.setPrefColumnCount(8);
        gnArea.setPrefRowCount(8);
        gnArea.setPadding(new Insets(5,5,5,5));
        gnButt.setGraphic(gnView1);
        //getGNPane().add(gnButt, 0, 0);
        gnBox=new HBox();
        gnBox.getChildren().addAll(gnButt,gnLabel);
        gnArea.setVisible(false);
        getGNPane().getChildren().add(gnBox);
        //getDesPane().add(desArea, 0, 1, 2, 1);
        
        syllabusBox.getChildren().add(gnPane);
        gnButt.setOnMouseClicked(e->{
            if(gnArea.isVisible()){
                gnButt.setGraphic(gnView1);
                gnText=gnArea.getText();
                gnArea.setVisible(false);
                getGNPane().getChildren().remove(gnArea);
            }else{
                gnButt.setGraphic(gnView2);
                gnArea.setText(gnText);
                getGNPane().getChildren().add(gnArea);
                gnArea.setVisible(true);
            }
        });
        String adPathplus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(ADD_ITEM);
        String adPathminus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(REMOVE_ITEM);
        Image adImage1 = new Image(adPathplus);
        Image adImage2 = new Image(adPathminus);
	
        ImageView adView1 = new ImageView(adImage1);
        ImageView adView2 = new ImageView(adImage2);
        adView1.setFitHeight(20);
       adView1.setFitWidth(20);
        adView2.setFitHeight(20);
        adView2.setFitWidth(20);
        adPane=new VBox();
        adButt=new Button();
        adText="";
        adLabel=new Label(props.getProperty(CourseSiteGeneratorProp.AD_LABEL).toString());
        adArea= new TextArea();
        //desArea.setPrefColumnCount(8);
        adArea.setPrefRowCount(8);
        adArea.setPadding(new Insets(5,5,5,5));
         adButt.setGraphic(adView1);
        //getADPane().add(adButt, 0, 0);
        adBox=new HBox();
        adBox.getChildren().addAll(adButt,adLabel);
        adArea.setVisible(false);
        getADPane().getChildren().add(adBox);
        //getDesPane().add(desArea, 0, 1, 2, 1);
       
        syllabusBox.getChildren().add(adPane);
        adButt.setOnMouseClicked(e->{
            if(adArea.isVisible()){
                adButt.setGraphic(adView1);
                adText=adArea.getText();
                adArea.setVisible(false);
                getADPane().getChildren().remove(adArea);
            }else{
                adButt.setGraphic(adView2);
                adArea.setText(adText);
                getADPane().getChildren().add(adArea);
                adArea.setVisible(true);
            }
        });
        String saPathplus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(ADD_ITEM);
        String saPathminus = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(REMOVE_ITEM);
        Image saImage1 = new Image(saPathplus);
        Image saImage2 = new Image(saPathminus);
	
        ImageView saView1 = new ImageView(saImage1);
        ImageView saView2 = new ImageView(saImage2);
        saView1.setFitHeight(20);
        saView1.setFitWidth(20);
        saView2.setFitHeight(20);
        saView2.setFitWidth(20);
        saPane=new VBox();
        saButt=new Button();
        saText="";
        saLabel=new Label(props.getProperty(CourseSiteGeneratorProp.SA_LABEL).toString());
        saArea= new TextArea();
        //desArea.setPrefColumnCount(8);
        saArea.setPrefRowCount(8);
        saArea.setPadding(new Insets(5,5,5,5));
        saButt.setGraphic(saView1);
        //getSAPane().add(saButt, 0, 0);
        saBox=new HBox();
        saBox.getChildren().addAll(saButt,saLabel);
        saArea.setVisible(false);
        getSAPane().getChildren().add(saBox);
        //getDesPane().add(desArea, 0, 1, 2, 1);
        
        syllabusBox.getChildren().add(saPane);
        saButt.setOnMouseClicked(e->{
            if(saArea.isVisible()){
                saButt.setGraphic(saView1);
                saText=saArea.getText();
                saArea.setVisible(false);
                getSAPane().getChildren().remove(saArea);
            }else{
                saButt.setGraphic(saView2);
                saArea.setText(saText);
                getSAPane().getChildren().add(saArea);
                saArea.setVisible(true);
            }
        });
        
        initControllers();
        
    }
    //@Override
    public VBox getComponentPane() {
        return syllabusBox;//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public VBox getDesPane() {
        return desPane;    
    }
    public Label getDesLabel() {
        return desLabel;    
    }

    void reloadWorkspace(AppDataComponent dataComponent) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    void handleDeleteKeyEvent() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public VBox getTopPane() {
return topPane;    }

    public VBox getPrePane() {
return prePane;    }

    public VBox getOutPane() {
return outPane;    }

    public VBox getBookPane() {
return bookPane;    }

    public VBox getGCPane() {
return gcPane;    }

    public VBox getGNPane() {
return gnPane;    }

    public VBox getADPane() {
return adPane;    }

    public VBox getSAPane() {
return saPane;    }
    public Label getPreLabel() {
        return preLabel;    
    }
    public Label getOutLabel() {
        return outLabel;    
    }
    public Label getBookLabel() {
        return bookLabel;    
    }
    public Label getGCLabel() {
        return gcLabel;    
    }
    public Label getGNLabel() {
        return gnLabel;    
    }
    public Label getADLabel() {
        return adLabel;    
    }
    public Label getSALabel() {
        return saLabel;    
    }
    public Label getTopLabel() {
        return topLabel;    
    }
    public String getDesText() {
        return desArea.getText();    
    }
    public String getPreText() {
        return preArea.getText();    
    }
    public String getOutText() {
        return outArea.getText();    
    }
    public String getTopText() {
        return topArea.getText();    
    }
    public String getTextText() {
        return textArea.getText();    
    }
    public String getGCText() {
        return gcArea.getText();    
    }
    public String getGNText() {
        return gnArea.getText();    
    }
    public String getADText() {
        return adArea.getText();    
    }
    public String getSAText() {
        return saArea.getText();    
    }
    public void setDesText(String t) {
         desText=t;  
         desArea.setText(t);
    }
    public void setPreText(String t) {
         preText=t;    
         preArea.setText(t);
    }
    public void setOutText(String t) {
         outText=t;   
         outArea.setText(t);
    }
    public void setTopText(String t) {
         topText=t; 
         topArea.setText(t);
    }
    public void setTextText(String t) {
         bookText=t;   
         textArea.setText(t);
    }
    public void setGCText(String t) {
         gcText=t;   
         gcArea.setText(t);
    }
    public void setGNText(String t) {
         gnText=t; 
         gnArea.setText(t);
    }
    public void setADText(String t) {
         adText=t; 
         adArea.setText(t);
    }
    public void setSAText(String t) {
         saText=t;  
         saArea.setText(t);
    }

    private void initControllers() {
        //SyllabusData sData = ((CourseSiteGeneratorData)app.getDataComponent()).getSyllabusData();
        desArea.textProperty().addListener(e->{
            sController.handleDes();
        });
        topArea.textProperty().addListener(e->{
            sController.handleTop();
        });
        preArea.textProperty().addListener(e->{
            sController.handlePre();
        });
        outArea.textProperty().addListener(e->{
            sController.handleOut();
        });
        textArea.textProperty().addListener(e->{
            sController.handleText();
        });
        gcArea.textProperty().addListener(e->{
            sController.handleGC();
        });
        gnArea.textProperty().addListener(e->{
            sController.handleGN();
        });
        adArea.textProperty().addListener(e->{
            sController.handleAD();
        });
        saArea.textProperty().addListener(e->{
            sController.handleSA();
        });
    }
}
