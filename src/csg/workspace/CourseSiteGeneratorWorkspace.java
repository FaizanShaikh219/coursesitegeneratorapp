package csg.workspace;
/**
 *
 * @author Faizan Shaikh
 */
import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import djf.modules.AppLanguageModule;
import djf.ui.MsgDialog;
import djf.ui.dialogs.AppDialogsFacade;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import jtps.jTPS;
import properties_manager.PropertiesManager;


public class CourseSiteGeneratorWorkspace extends AppWorkspaceComponent {
    
    CourseSiteGeneratorApp app;
    SiteWorkspace courseWorkspace;
    OfficeHoursWorkspace taWorkspace;
    ScheduleWorkspace scheduleWorkspace;
    MeetingTimesWorkspace meetingWorkspace;
    SyllabusWorkspace syllabusWorkspace;
    
    private jTPS transactionProcessor;
    
    private Tab courseTab;
    private Tab taTab;
    private Tab scheduleTab;
    private Tab meetingTab;
    private TabPane tabPane;
    private Tab syllabusTab;
        
    public CourseSiteGeneratorWorkspace(CourseSiteGeneratorApp app) {
        
        transactionProcessor = new jTPS();
        
        
        this.app = app;
        workspace = new BorderPane();
        tabPane = new TabPane();
        tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
        
        //load the tab text properties
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        //initialize all the subcomponents
        courseWorkspace = new SiteWorkspace(app);
        taWorkspace = new OfficeHoursWorkspace(app);
        scheduleWorkspace = new ScheduleWorkspace(app);
        meetingWorkspace=new MeetingTimesWorkspace(app);
        syllabusWorkspace=new SyllabusWorkspace(app);
        String courseTabText = props.getProperty(CourseSiteGeneratorProp.COURSE_TAB_TEXT.toString());
        String taTabText = props.getProperty(CourseSiteGeneratorProp.TA_TAB_TEXT.toString());
        String recitationTabText = props.getProperty(CourseSiteGeneratorProp.RECITATION_TAB_TEXT.toString());
        String scheduleTabText = props.getProperty(CourseSiteGeneratorProp.SCHEDULE_TAB_TEXT.toString());
        String syllabusTabText = "Syllabus";//props.getProperty(CourseSiteGeneratorProp.SYLLABUS_TAB_TEXT.toString());
    
        courseTab = new Tab(courseTabText);
        
        syllabusTab=new Tab(syllabusTabText);
        taTab = new Tab(taTabText);
        scheduleTab = new Tab(scheduleTabText);
        meetingTab = new Tab(recitationTabText);
        tabPane.getTabs().add(courseTab);
        tabPane.getTabs().add(taTab);
        tabPane.getTabs().add(scheduleTab);
        tabPane.getTabs().add(meetingTab);
        tabPane.getTabs().add(syllabusTab);
        //tabPane.tabMinWidthProperty().bind(root.widthProperty().divide(tabPane.getTabs().size()).subtract(20));
        //tabPane.setTabMinWidth(tabPane.getWidth() / 5);
        //tabPane.setTabMaxWidth(tabPane.getWidth() / 5); 
       tabPane.widthProperty().addListener((observable, oldValue, newValue) ->
    {
        tabPane.setTabMinWidth(tabPane.getWidth() / 5);
        tabPane.setTabMaxWidth(tabPane.getWidth() / 5);      
    });
        ScrollPane courseScrollPane = new ScrollPane(courseWorkspace.getComponentPane());
        courseScrollPane.setFitToWidth(true);
        ScrollPane syllabusScrollPane = new ScrollPane(syllabusWorkspace.getComponentPane());
        syllabusScrollPane.setFitToWidth(true);
        
        courseTab.setContent(courseScrollPane);
        syllabusTab.setContent(syllabusScrollPane);
        taTab.setContent(taWorkspace.getComponentPane());
        scheduleTab.setContent(scheduleWorkspace.getComponentPane());
        meetingTab.setContent(meetingWorkspace.getComponentPane());
        ((BorderPane) workspace).setCenter(tabPane);
        
        
        initKeyEvents();
    }

    
    //Clear workspace before loading new file
    @Override
    public void resetWorkspace() {
        
        transactionProcessor = new jTPS();
        taWorkspace.resetWorkspace();
        courseWorkspace.resetWorkspace(); 
        scheduleWorkspace.resetWorkspace();
        meetingWorkspace.resetWorkspace();
       
    }

    /**
     * create hooks for key press events.
     */
    public void initKeyEvents() {
      
        workspace.setOnKeyPressed(e -> {
            
            KeyCode code = e.getCode();
            if (code == KeyCode.DELETE || code == KeyCode.MINUS) {
                
                Tab selectedTab = tabPane.getSelectionModel().getSelectedItem();

                if (selectedTab == taTab) {
                    taWorkspace.handleDeleteKeyEvent();
                }
                else if (selectedTab == courseTab) {
                    courseWorkspace.handleDeleteKeyEvent();
                }
                 else if (selectedTab == scheduleTab) {
                    scheduleWorkspace.handleDeleteKeyEvent();
                }
                else if (selectedTab == meetingTab) {
                    meetingWorkspace.handleDeleteKeyEvent();
                }
                else if (selectedTab == syllabusTab) {
                    syllabusWorkspace.handleDeleteKeyEvent();
                }
                
                
            }
            else if (code == KeyCode.Z && e.isControlDown()) {
                undoAction();
            }
            else if (code == KeyCode.Y && e.isControlDown()) {
                redoAction();
            }        
        }); 
    }
    

    //Configure workspace to work with new file
    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        
        taWorkspace.reloadWorkspace(dataComponent);
        courseWorkspace.reloadWorkspace(dataComponent);
        scheduleWorkspace.reloadWorkspace(dataComponent);
        meetingWorkspace.reloadWorkspace(dataComponent);
        syllabusWorkspace.reloadWorkspace(dataComponent);
        
    }

    public SiteWorkspace getCourseWorkspace() {
        return courseWorkspace;
    }
    public SyllabusWorkspace getSyllabusWorkspace() {
        return syllabusWorkspace;
    }
    
    public OfficeHoursWorkspace getTAWorkspace() {
        return taWorkspace;
    }
    
    public ScheduleWorkspace getScheduleWorkspace() {
        return scheduleWorkspace;
    }
    public MeetingTimesWorkspace getMeetingTimesWorkspace() {
        return meetingWorkspace;
    }
    public jTPS getTransactionProcessor() {
        return transactionProcessor;
        
    }

    @Override
    public void undoAction() {
        transactionProcessor.undoTransaction();
    }

    @Override
    public void redoAction() {
        transactionProcessor.doTransaction();
    }

    public void showAppInfo() {
        MsgDialog dialog = MsgDialog.getSingleton();
        PropertiesManager prop = PropertiesManager.getPropertiesManager();
        dialog.show(prop.getProperty(CourseSiteGeneratorProp.HELP_INFORMATION_TITLE), 
                prop.getProperty(CourseSiteGeneratorProp.HELP_INFORMATION_MESSAGE) );
    }

    @Override
    public void changeAction() {
        try {
            AppLanguageModule languageSettings = app.getLanguageModule();
            AppDialogsFacade.showLanguageDialog(languageSettings,app);
        }
        catch(AppLanguageModule.LanguageException le) {
            System.out.println("Error Loading Language into UI");
        }
    }

    @Override
    public void helpAction() {
        PropertiesManager prop = PropertiesManager.getPropertiesManager();
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle((prop.getProperty(CourseSiteGeneratorProp.HELP_INFORMATION_TITLE)).toString());
        alert.setHeaderText("About the App");
        alert.setContentText((prop.getProperty(CourseSiteGeneratorProp.HELP_INFORMATION_MESSAGE)).toString());

        alert.showAndWait();
    }

    @Override
    public void aboutAction() {
        //AppDialogsFacade.showAboutDialog(app);
        PropertiesManager prop = PropertiesManager.getPropertiesManager();
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle((prop.getProperty(CourseSiteGeneratorProp.ABOUT_INFORMATION_TITLE)).toString());
        alert.setHeaderText("About the App");
        alert.setContentText((prop.getProperty(CourseSiteGeneratorProp.ABOUT_INFORMATION_MESSAGE)).toString());

        alert.showAndWait();
        //AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        
        //dialog.setTitle();
        //dialog.show();
        //dialog.show((prop.getProperty(CourseSiteGeneratorProp.HELP_INFORMATION_TITLE)).toString(), 
        //        (prop.getProperty(CourseSiteGeneratorProp.HELP_INFORMATION_MESSAGE)).toString() );*/
    }
}
