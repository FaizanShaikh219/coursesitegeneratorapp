package csg.file;

import csg.CourseSiteGeneratorApp;
import csg.data.CourseSiteGeneratorData;
import csg.data.ScheduleData;
import csg.data.ScheduleItem;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;

/**
 *
 * @author USER
 */
class ScheduleFiles implements FileSectionManager {

    private CourseSiteGeneratorApp app;
    private static final String JSON_SCHEDULE_STARTMONTH = "startingMondayMonth";
    private static final String JSON_SCHEDULE_ENDMONTH = "endingFridayMonth";
    private static final String JSON_SCHEDULE_STARTDAY = "startingMondayDay";
    private static final String JSON_SCHEDULE_ENDDAY = "endingFridayDay";
    private static final String JSON_SCHEDULE_START = "startingMonday";
    private static final String JSON_SCHEDULE_END = "endingFriday";
    private static final String JSON_SCHEDULE_ITEM = "scheduleItems";
    private static final String JSON_SCHEDULE_ITEM_TYPE = "type";
    private static final String JSON_SCHEDULE_ITEM_DATE = "date";
    private static final String JSON_SCHEDULE_ITEM_TITLE = "title";
    private static final String JSON_SCHEDULE_ITEM_TOPIC = "topic";
    private static final String JSON_SCHEDULE_ITEM_LINK = "link";
    private static final String JSON_SCHEDULE_ITEM_TIME = "time";
    private static final String JSON_SCHEDULE_ITEM_CRITERIA = "criteria";
    private static final String MONTH = "month";
    private static final String DAY = "day";
    private String HOLIDAY="holidays";
    private String LECTURE="lectures";
    private String HW="hws";
    private String REFERENCE="references";
    private String RECITATION="recitations";
    public ScheduleFiles(CourseSiteGeneratorApp app) {
        this.app = app;
    }
    @Override
    public JsonObject generateDataJSON(CourseSiteGeneratorData data) {
        ScheduleData scheduleData = data.getScheduleData();

        JsonArrayBuilder scheduleArrayBuilder = Json.createArrayBuilder();

        for (ScheduleItem item : scheduleData.getBackScheduleItems()) {
            JsonObject scheduleObject = Json.createObjectBuilder()
                    .add(JSON_SCHEDULE_ITEM_TYPE, item.getType())
                    .add(JSON_SCHEDULE_ITEM_DATE, item.getDate().toString())
                    .add(JSON_SCHEDULE_ITEM_TITLE, item.getTitle())
                    .add(JSON_SCHEDULE_ITEM_TOPIC, item.getTopic())
                    //.add(JSON_SCHEDULE_ITEM_TIME, item.getTime())
                    .add(JSON_SCHEDULE_ITEM_LINK, item.getLink())
                    //.add(JSON_SCHEDULE_ITEM_CRITERIA, item.getCriteria())
                    .build();
            
                scheduleArrayBuilder.add(scheduleObject);            
        }
        
        JsonObject object = Json.createObjectBuilder()
                .add(JSON_SCHEDULE_START, scheduleData.getStartingDate().toString())
                .add(JSON_SCHEDULE_END, scheduleData.getEndingDate().toString())
                .add(JSON_SCHEDULE_ITEM, scheduleArrayBuilder.build())
                .build();
        return object;
    }

    @Override
    public void loadData(JsonObject section, CourseSiteGeneratorData data) {
        ScheduleData scheduleData = data.getScheduleData();
        String start = section.getString(JSON_SCHEDULE_START);
        String end = section.getString(JSON_SCHEDULE_END);
        
        DateTimeFormatter dateFormat = DateTimeFormatter.ISO_DATE;
        scheduleData.setStartingDate(LocalDate.parse(start, dateFormat));
        scheduleData.setEndingDate(LocalDate.parse(end, dateFormat));
    
            LocalDate sDate=LocalDate.parse(start, dateFormat);
            LocalDate eDate=LocalDate.parse(start, dateFormat);
            
        
        JsonArray scheduleArray = section.getJsonArray(JSON_SCHEDULE_ITEM);

        for (Object scheduleObject : scheduleArray.toArray()) {
            JsonObject scheduleJsonObject = (JsonObject)scheduleObject;
            String type = scheduleJsonObject.getString(JSON_SCHEDULE_ITEM_TYPE);
            String date = scheduleJsonObject.getString(JSON_SCHEDULE_ITEM_DATE);
            String topic = scheduleJsonObject.getString(JSON_SCHEDULE_ITEM_TOPIC);
            String title = scheduleJsonObject.getString(JSON_SCHEDULE_ITEM_TITLE);
            //String time = scheduleJsonObject.getString(JSON_SCHEDULE_ITEM_TIME);
            String link = scheduleJsonObject.getString(JSON_SCHEDULE_ITEM_LINK); 
            //String criteria = scheduleJsonObject.getString(JSON_SCHEDULE_ITEM_CRITERIA);
            LocalDate lDate=LocalDate.parse(start, dateFormat);
            
            
            if(lDate.isAfter(sDate)&&lDate.isBefore(eDate)){
                scheduleData.addScheduleItem(type, LocalDate.parse(date, dateFormat), title, topic,  link);
            }
            else if(lDate.isEqual(sDate)||lDate.isEqual(eDate)){
                scheduleData.addScheduleItem(type, LocalDate.parse(date, dateFormat), title, topic,  link);
            }
            else{
                scheduleData.addBackItem(type, LocalDate.parse(date, dateFormat), title, topic,  link);
            }
        }
    }

    JsonObject generateSchDataJSON(CourseSiteGeneratorData data) {
        ScheduleData scheduleData = data.getScheduleData();
        JsonArrayBuilder hwArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder holArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder recArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder lecArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder refArrayBuilder = Json.createArrayBuilder();
        
        for (ScheduleItem item : scheduleData.getBackScheduleItems()) {
            if(item.getType().equals("SCHEDULE_ITEM_HOMEWORK")){
            JsonObject hwObject = Json.createObjectBuilder()
                    .add(MONTH, item.getDate().getMonthValue()+"")
                    .add(DAY, item.getDate().getDayOfMonth()+"")
                    .add(JSON_SCHEDULE_ITEM_TITLE, item.getTitle())
                    .add(JSON_SCHEDULE_ITEM_TOPIC, item.getTopic())
                    .add(JSON_SCHEDULE_ITEM_TIME,"" )
                    .add(JSON_SCHEDULE_ITEM_LINK, item.getLink())
                    .add(JSON_SCHEDULE_ITEM_CRITERIA,"none" )
                    .build();
            hwArrayBuilder.add(hwObject);
                
            }
        }
        for (ScheduleItem item : scheduleData.getBackScheduleItems()) {
            if(item.getType().equals("SCHEDULE_ITEM_HOLIDAY")){
            JsonObject holObject = Json.createObjectBuilder()
                    .add(MONTH, item.getDate().getMonthValue()+"")
                    .add(DAY, item.getDate().getDayOfMonth()+"")
                    .add(JSON_SCHEDULE_ITEM_TITLE, item.getTitle())
                    
                    .add(JSON_SCHEDULE_ITEM_LINK, item.getLink())
                    
                    .build();
            holArrayBuilder.add(holObject);
                
            }
        }
        for (ScheduleItem item : scheduleData.getBackScheduleItems()) {
            if(item.getType().equals("SCHEDULE_ITEM_LECTURE")){
            JsonObject lecObject = Json.createObjectBuilder()
                    .add(MONTH, item.getDate().getMonthValue()+"")
                    .add(DAY, item.getDate().getDayOfMonth()+"")
                    .add(JSON_SCHEDULE_ITEM_TITLE, item.getTitle())
                    .add(JSON_SCHEDULE_ITEM_TOPIC, item.getTopic())
                    //.add(JSON_SCHEDULE_ITEM_TIME,"" )
                    .add(JSON_SCHEDULE_ITEM_LINK, item.getLink())
                    //.add(JSON_SCHEDULE_ITEM_CRITERIA,"none" )
                    .build();
            lecArrayBuilder.add(lecObject);
                
            }
        }
        for (ScheduleItem item : scheduleData.getBackScheduleItems()) {
            if(item.getType().equals("SCHEDULE_ITEM_REFERENCE")){
            JsonObject refObject = Json.createObjectBuilder()
                    .add(MONTH, item.getDate().getMonthValue()+"")
                    .add(DAY, item.getDate().getDayOfMonth()+"")
                    .add(JSON_SCHEDULE_ITEM_TITLE, item.getTitle())
                    .add(JSON_SCHEDULE_ITEM_TOPIC, item.getTopic())
                    //.add(JSON_SCHEDULE_ITEM_TIME,"" )
                    .add(JSON_SCHEDULE_ITEM_LINK, item.getLink())
                    //.add(JSON_SCHEDULE_ITEM_CRITERIA,"none" )
                    .build();
            refArrayBuilder.add(refObject);
                
            }
        }
        for (ScheduleItem item : scheduleData.getBackScheduleItems()) {
            if(item.getType().equals("SCHEDULE_ITEM_RECITATION")){
            JsonObject recObject = Json.createObjectBuilder()
                    .add(MONTH, item.getDate().getMonthValue()+"")
                    .add(DAY, item.getDate().getDayOfMonth()+"")
                    .add(JSON_SCHEDULE_ITEM_TITLE, item.getTitle())
                    .add(JSON_SCHEDULE_ITEM_TOPIC, item.getTopic())
                    
                    .build();
            recArrayBuilder.add(recObject);
                
            }
        }
        
        
        JsonObject object = Json.createObjectBuilder()
                .add(JSON_SCHEDULE_STARTMONTH, scheduleData.getStartingDate().getMonthValue())
                .add(JSON_SCHEDULE_STARTDAY, scheduleData.getStartingDate().getDayOfMonth())
                .add(JSON_SCHEDULE_ENDMONTH, scheduleData.getEndingDate().getMonthValue())
                .add(JSON_SCHEDULE_ENDDAY, scheduleData.getEndingDate().getDayOfMonth())
                .add(HOLIDAY, holArrayBuilder)
                .add(LECTURE, lecArrayBuilder)
                .add(REFERENCE, refArrayBuilder)
                .add(RECITATION, recArrayBuilder)
                .add(HW, hwArrayBuilder)
                
                .build();
        return object;
    }
    
}
