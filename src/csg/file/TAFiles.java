package csg.file;
/**
 *
 * @author Faizan Shaikh
 */
import csg.CourseSiteGeneratorApp;
import csg.data.CourseSiteGeneratorData;
import csg.data.OHTimeSlot;
import csg.data.SiteData;
import csg.data.TAData;
import csg.data.TAData.TimeSlotPosition;
import csg.data.TeachingAssistant;
import java.io.StringReader;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;


public class TAFiles implements FileSectionManager {
    
    private CourseSiteGeneratorApp app;
    private static final String JSON_NAME = "name";
    private static final String JSON_LINK = "link";
    private static final String JSON_OFFICE_START_HOUR = "startHour";
    private static final String JSON_OFFICE_END_HOUR = "endHour";
    private static final String JSON_OFFICE_HOURS = "officeHours";
    private static final String JSON_OFFICE_DAY = "day";
    private static final String JSON_OFFICE_TIME = "time";
    private static final String JSON_TA_NAME = "name";
    private static final String JSON_TA = "teachingAssistants";
    private static final String JSON_UGTA = "undergrad_tas";
    private static final String JSON_GTA = "grad_tas";
    private static final String JSON_TA_EMAIL = "email";
    private static final String JSON_TA_UNDERGRAD = "Type";
    private static final String JSON_COURSE_INSTRUCTOR = "instructor";
    private static final String JSON_COURSE_INSTRUCTOR_PHOTO = "photo";
    private static final String JSON_COURSE_INSTRUCTOR_EMAIL = "email";
    private static final String JSON_COURSE_INSTRUCTOR_ROOM = "room";
    private static final String JSON_COURSE_INSTRUCTOR_OH = "hours";
    public TAFiles(CourseSiteGeneratorApp app) {
        this.app = app;
    }

    @Override
    public JsonObject generateDataJSON(CourseSiteGeneratorData data) {

        TAData taData = data.getTAData();
        JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder officeHourArrayBuilder = Json.createArrayBuilder();

        //build JSON lists for office hours and teaching assistant tables.
        for (TeachingAssistant ta : taData.getTeachingAssistants()) {
            JsonObject taObject = Json.createObjectBuilder()
                    .add(JSON_TA_NAME, ta.getName())
                    .add(JSON_TA_EMAIL, ta.getEmail())
                    .add(JSON_TA_UNDERGRAD, ta.getType())
                    .build();
            
             
            taArrayBuilder.add(taObject);
                       
            //get list of all persons timeslots.
            List<TimeSlotPosition> timeSlots = taData.getTAOfficeHours(ta.getName());
            for (TimeSlotPosition position : timeSlots) {
                JsonObject officeHourSlotObject = Json.createObjectBuilder()
                        .add(JSON_TA_NAME, ta.getName())
                        .add(JSON_OFFICE_DAY, position.getDay())
                        .add(JSON_OFFICE_TIME, OHTimeSlot.formatTime(position.getHour(), position.getMinute()))
                        .build();
                
                officeHourArrayBuilder.add(officeHourSlotObject); 
            }
        }
        
        //build the JSON object to return.
        JsonObject object = Json.createObjectBuilder()
                .add(JSON_OFFICE_START_HOUR, taData.getStartHour())
                .add(JSON_OFFICE_END_HOUR, taData.getEndHour())
                .add(JSON_OFFICE_HOURS, officeHourArrayBuilder.build())
                .add(JSON_TA, taArrayBuilder.build())
                .build();
                
    
        return object;
     
    }

    @Override
    public void loadData(JsonObject section, CourseSiteGeneratorData data) {
        TAData taData = data.getTAData();
        //OHTimeSlot timeslot = data.
        JsonArray taArray = section.getJsonArray(JSON_TA);
        
        for (Object taObject : taArray.toArray()) {
            JsonObject taJsonObject = (JsonObject)taObject;
            String name = taJsonObject.getString(JSON_TA_NAME);
            String email = taJsonObject.getString(JSON_TA_EMAIL);
            String isUnderGrad = taJsonObject.getString(JSON_TA_UNDERGRAD);
            TeachingAssistant ta=new TeachingAssistant(name,email,isUnderGrad);
            taData.addTA(ta);
        }
        taData.setStartHour(section.getInt(JSON_OFFICE_START_HOUR));
        taData.setEndHour(section.getInt(JSON_OFFICE_END_HOUR));
        taData.initHours(section.getInt(JSON_OFFICE_START_HOUR),section.getInt(JSON_OFFICE_END_HOUR));
        JsonArray officeHoursArray = section.getJsonArray(JSON_OFFICE_HOURS);
 
        for (Object officeHoursObject : officeHoursArray.toArray()) {
            JsonObject officeHoursJsonObject = (JsonObject)officeHoursObject;
            String time = officeHoursJsonObject.getString(JSON_OFFICE_TIME);
            String day = officeHoursJsonObject.getString(JSON_OFFICE_DAY);
            String name = officeHoursJsonObject.getString(JSON_TA_NAME);

            int[] timeArray = OHTimeSlot.getTimeValue(time);
            int dayIndex = taData.getDayIndexFromString(day);
            boolean onHour = (timeArray[1] == 0);
            
            TeachingAssistant ta=taData.getTA(name);
            TimeSlotPosition position = taData.new TimeSlotPosition(dayIndex, timeArray[0], onHour);
            taData.toggleTA(position,ta, name);
        }
    
    }

    JsonObject generateTADataJSON(CourseSiteGeneratorData data) {
        SiteData courseData = data.getSiteData();
        TAData taData = data.getTAData();
        JsonArrayBuilder ugtaArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder gtaArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder officeHourArrayBuilder = Json.createArrayBuilder();
        JsonReader red=Json.createReader(new StringReader(courseData.getInsOH()));
        JsonArray oh=red.readArray();
        //build JSON lists for office hours and teaching assistant tables.
        for (TeachingAssistant ta : taData.getTeachingAssistants()) {
            if(ta.getType().equals("Undergraduate")){
            JsonObject taObject = Json.createObjectBuilder()
                    .add(JSON_TA_NAME, ta.getName())
                    .add(JSON_TA_EMAIL, ta.getEmail())
                    //.add(JSON_TA_UNDERGRAD, ta.getType())
                    .build();
            
             
            ugtaArrayBuilder.add(taObject);
                       
            //get list of all persons timeslots.
            List<TimeSlotPosition> timeSlots = taData.getTAOfficeHours(ta.getName());
            for (TimeSlotPosition position : timeSlots) {
                JsonObject officeHourSlotObject = Json.createObjectBuilder()
                        
                        .add(JSON_OFFICE_DAY, position.getDay().toUpperCase())
                        .add(JSON_OFFICE_TIME, OHTimeSlot.formatTime(position.getHour(), position.getMinute()).trim().replace(":", "_").replace(" ", ""))
                        .add(JSON_TA_NAME, ta.getName())
                        .build();
                
                officeHourArrayBuilder.add(officeHourSlotObject); 
            }
            }
        }
        for (TeachingAssistant ta : taData.getTeachingAssistants()) {
            if(ta.getType().equals("Graduate")){
            JsonObject taObject = Json.createObjectBuilder()
                    .add(JSON_TA_NAME, ta.getName())
                    .add(JSON_TA_EMAIL, ta.getEmail())
                    //.add(JSON_TA_UNDERGRAD, ta.getType())
                    .build();
            
             
            gtaArrayBuilder.add(taObject);
                       
            //get list of all persons timeslots.
            List<TimeSlotPosition> timeSlots = taData.getTAOfficeHours(ta.getName());
            for (TimeSlotPosition position : timeSlots) {
                JsonObject officeHourSlotObject = Json.createObjectBuilder()
                        
                        .add(JSON_OFFICE_DAY, position.getDay().toUpperCase())
                        .add(JSON_OFFICE_TIME, OHTimeSlot.formatTime(position.getHour(), position.getMinute()).trim().replace(":", "_").replace(" ", ""))
                        .add(JSON_TA_NAME, ta.getName())
                        .build();
                
                officeHourArrayBuilder.add(officeHourSlotObject); 
            }
            }
        }
        //SiteData courseData = data.getSiteData();
        
        JsonObject ins = Json.createObjectBuilder()
                .add(JSON_NAME, courseData.getInsName())
                .add(JSON_LINK, courseData.getInsHome())
                .add(JSON_COURSE_INSTRUCTOR_EMAIL, courseData.getInsEmail())
                .add(JSON_COURSE_INSTRUCTOR_ROOM, courseData.getInsRoom())
                .add(JSON_COURSE_INSTRUCTOR_PHOTO, "./images/RichardMcKenna.jpg")
                .add(JSON_COURSE_INSTRUCTOR_OH, oh)
                .build();
        //build the JSON object to return.
        JsonObject object = Json.createObjectBuilder()
                .add(JSON_OFFICE_START_HOUR, taData.getStartHour())
                .add(JSON_OFFICE_END_HOUR, taData.getEndHour())
                .add(JSON_OFFICE_HOURS, officeHourArrayBuilder.build())
                .add(JSON_UGTA, ugtaArrayBuilder.build())
                .add(JSON_GTA, gtaArrayBuilder.build())
                .add(JSON_COURSE_INSTRUCTOR, ins)
                .build();
                
    
        return object;
    }
    
}
