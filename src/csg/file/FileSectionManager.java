    package csg.file;
/**
 *
 * @author Faizan Shaikh
 */
import csg.data.CourseSiteGeneratorData;
import javax.json.JsonObject;


public interface FileSectionManager {
    
    public JsonObject generateDataJSON(CourseSiteGeneratorData data);
    public void loadData(JsonObject section,CourseSiteGeneratorData data);
    
    
}
