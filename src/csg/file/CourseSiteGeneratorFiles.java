package csg.file;
/**
 *
 * @author Faizan Shaikh
 */
import csg.CourseSiteGeneratorApp;
import csg.data.CourseSiteGeneratorData;
import csg.data.SiteData;
import csg.workspace.controller.SiteController;


import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.json.JsonObject;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javax.json.Json;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import org.apache.commons.io.FileUtils;


public class CourseSiteGeneratorFiles implements AppFileComponent {
    
        private CourseSiteGeneratorApp app;
        
       
        private static final String JSON_COURSE_TITLE = "courseData";
        private static final String JSON_TA_TITLE = "taData";
        private static final String JSON_SYLLABUS_TITLE = "syllabus";
        private static final String JSON_SCHEDULE_TITLE = "schedule";
        private static final String JSON_MEETING_TITLE = "meeting times";
        private static final String EXPORT_JSON_FILENAME = "CourseSiteData.json";
        
        public static final String DEFAULT_JAVASCRIPT_DIRECTORY = "/js/";
        public static final String DEFAULT_STYLE_DIRECTORY = "/css/";
        public static final String DEFAULT_IMAGES_DIRECTORY = "/images/";
        public static final String DEFAULT_TEMPLATE_DIRECTORY = "/work/template/";
        public static final String DEFAULT_EXPORT_DIRECTORY = "/work/export/";
        public static final String DEFAULT_FAKEEXPORT_DIRECTORY = "/work/public_html/";
        public static final String DEFAULT_WORK_STYLE_DIRECTORY = "/work/css/";
        public static final String DEFAULT_STYLE_EXTENSION = ".css";
        public static final String OFFICEHOURSDATA = "OfficeHoursData.json";
        public static final String SECTIONSDATA = "SectionsData.json";
        public static final String SYLLABUSDATA = "SyllabusData.json";
        public static final String PAGEDATA = "PageData.json";
        public static final String SCHEDULEDATA = "ScheduleData.json";

        private TAFiles taFiles;
        private MTFiles mtFiles;
        private ScheduleFiles schFiles;
        private SiteFiles siteFiles;
        private SyllabusFiles syllabusFiles;
        
        
    public CourseSiteGeneratorFiles(CourseSiteGeneratorApp app) {
        this.app = app;
        taFiles = new TAFiles(app);
        mtFiles=new MTFiles();
        schFiles = new ScheduleFiles(app);
        siteFiles = new SiteFiles(app);
        syllabusFiles = new SyllabusFiles(app);
        
    }

    public SiteFiles getSiteFiles(){
        return siteFiles;
    }
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
       JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_COURSE_TITLE, siteFiles.generateDataJSON((CourseSiteGeneratorData) data))
                .add(JSON_SCHEDULE_TITLE, schFiles.generateDataJSON((CourseSiteGeneratorData) data))
                .add(JSON_TA_TITLE, taFiles.generateDataJSON((CourseSiteGeneratorData) data))
                .add(JSON_SYLLABUS_TITLE, syllabusFiles.generateDataJSON((CourseSiteGeneratorData) data))
                .add(JSON_MEETING_TITLE, mtFiles.generateDataJSON((CourseSiteGeneratorData) data))

                .build();
        
        writeJSONFile(dataManagerJSO, filePath);
    }

    
    public void writeJSONFile(JsonObject object, String filePath) throws FileNotFoundException {
           Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);

        StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(object);
	jsonWriter.close();
        
  
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(object);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();        
    }
    
    
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        System.out.println(filePath);
       JsonObject json = loadJSONFile(filePath);
        
        
        taFiles.loadData(json.getJsonObject(JSON_TA_TITLE), (CourseSiteGeneratorData) data);
        siteFiles.loadData(json.getJsonObject(JSON_COURSE_TITLE), (CourseSiteGeneratorData) data);
        syllabusFiles.loadData(json.getJsonObject(JSON_SYLLABUS_TITLE), (CourseSiteGeneratorData) data);
        schFiles.loadData(json.getJsonObject(JSON_SCHEDULE_TITLE), (CourseSiteGeneratorData) data);
        mtFiles.loadData(json.getJsonObject(JSON_MEETING_TITLE), (CourseSiteGeneratorData) data);
        
        
        if (app != null) {
            app.getWorkspaceComponent().reloadWorkspace(app.getDataComponent());
        }
    }

  private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
   
    
   

    
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        
    }
    
  
   
   
   
    public static List getFilesInDirectory(File directory) {
        
        ArrayList<String> fileNames = new ArrayList<>();
        for (File file : directory.listFiles()) {
            
           if (file.isFile()) {
                fileNames.add(file.getName());
           }
        }
        return fileNames;
    }

    @Override
    public void exportData(AppDataComponent data) throws IOException {
        SiteData siteData = ((CourseSiteGeneratorData )data).getSiteData();
        if(!siteData.getSubject().isEmpty()&&!siteData.getNumber().isEmpty()&&!siteData.getSemester().isEmpty()&&!siteData.getYear().isEmpty()){
            boolean success = (new File(System.getProperty("user.dir")+DEFAULT_EXPORT_DIRECTORY.toString()+siteData.getSubject()+"_"+siteData.getNumber().toString()
                    +"_"+siteData.getSemester()+"_"+siteData.getYear()+"/public_html"))
                    .mkdirs();
            System.out.println(success);
            if (success) {
                try {
                    System.out.println("Directory created");
                    app.getGUI().updateExportControl(true);
                    //File dest = new File("C:\\Users\\yourProfile\\Desktop"); //any location
                    FileUtils.copyDirectory(new File(System.getProperty("user.dir")+DEFAULT_FAKEEXPORT_DIRECTORY.toString()), (new File(System.getProperty("user.dir")+DEFAULT_EXPORT_DIRECTORY.toString()+siteData.getSubject()+"_"+siteData.getNumber().toString()
                    +"_"+siteData.getSemester()+"_"+siteData.getYear()+"/public_html")));
                    //Files.copy(file.toPath(),data.getExportDir(), StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException ex) {
                    Logger.getLogger(SiteController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }  
        }
        this.writeJSONFile(siteFiles.generateSiteDataJSON((CourseSiteGeneratorData) data), 
                System.getProperty("user.dir")+DEFAULT_EXPORT_DIRECTORY.toString()+siteData.getSubject()+"_"+siteData.getNumber().toString()
                    +"_"+siteData.getSemester()+"_"+siteData.getYear()+"/public_html" +DEFAULT_JAVASCRIPT_DIRECTORY.toString()+PAGEDATA.toString());
        this.writeJSONFile(mtFiles.generateDataJSON((CourseSiteGeneratorData) data), 
                System.getProperty("user.dir")+DEFAULT_EXPORT_DIRECTORY.toString()+siteData.getSubject()+"_"+siteData.getNumber().toString()
                    +"_"+siteData.getSemester()+"_"+siteData.getYear()+"/public_html" +DEFAULT_JAVASCRIPT_DIRECTORY.toString()+SECTIONSDATA.toString());
        this.writeJSONFile(taFiles.generateTADataJSON((CourseSiteGeneratorData) data), 
                System.getProperty("user.dir")+DEFAULT_EXPORT_DIRECTORY.toString()+siteData.getSubject()+"_"+siteData.getNumber().toString()
                    +"_"+siteData.getSemester()+"_"+siteData.getYear()+"/public_html" +DEFAULT_JAVASCRIPT_DIRECTORY.toString()+OFFICEHOURSDATA.toString());
        this.writeJSONFile(syllabusFiles.generateSYDataJSON((CourseSiteGeneratorData) data), 
                System.getProperty("user.dir")+DEFAULT_EXPORT_DIRECTORY.toString()+siteData.getSubject()+"_"+siteData.getNumber().toString()
                    +"_"+siteData.getSemester()+"_"+siteData.getYear()+"/public_html" +DEFAULT_JAVASCRIPT_DIRECTORY.toString()+SYLLABUSDATA.toString());
        this.writeJSONFile(schFiles.generateSchDataJSON((CourseSiteGeneratorData) data), 
                System.getProperty("user.dir")+DEFAULT_EXPORT_DIRECTORY.toString()+siteData.getSubject()+"_"+siteData.getNumber().toString()
                    +"_"+siteData.getSemester()+"_"+siteData.getYear()+"/public_html" +DEFAULT_JAVASCRIPT_DIRECTORY.toString()+SCHEDULEDATA.toString());
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Export");
            alert.setHeaderText("Done!");
            alert.setContentText("Export Successful");

            alert.showAndWait();
            return;
    }
    
    
}
