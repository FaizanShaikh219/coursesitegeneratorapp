package csg.file;
/**
 *
 * @author Faizan Shaikh
 */
import csg.CourseSiteGeneratorApp;
import csg.data.CourseSiteGeneratorData;
import csg.data.SyllabusData;
import csg.workspace.CourseSiteGeneratorWorkspace;
import csg.workspace.SyllabusWorkspace;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 *
 * @author USER
 */
public class SyllabusFiles implements FileSectionManager{
    private CourseSiteGeneratorApp app;
    private static final String JSON_COURSE_DES = "description";
    private static final String JSON_COURSE_TOP = "topics";
    private static final String JSON_COURSE_PRE = "prerequisites";
    private static final String JSON_COURSE_OUT = "outcomes";
    private static final String JSON_COURSE_TEXT = "textbooks";
    private static final String JSON_COURSE_GC = "gradedComponents";
    private static final String JSON_COURSE_GN = "gradingNote";
    private static final String JSON_COURSE_AD = "academicDishonesty";
    private static final String JSON_COURSE_SA = "specialAssistance";
    
    
    public SyllabusFiles(CourseSiteGeneratorApp app) {
        this.app = app;
        
    }
    @Override
    public JsonObject generateDataJSON(CourseSiteGeneratorData data) {
        SyllabusData sData = data.getSyllabusData();
        
	//JsonArrayBuilder pageArrayBuilder = Json.createArrayBuilder();
        
       
                
        JsonObject object = Json.createObjectBuilder()
                .add(JSON_COURSE_DES, sData.getDes())
                .add(JSON_COURSE_TOP, sData.getTop())
                .add(JSON_COURSE_PRE, sData.getPre())
                .add(JSON_COURSE_OUT, sData.getOut())
                .add(JSON_COURSE_TEXT, sData.getText())
                .add(JSON_COURSE_GN, sData.getGN())
                .add(JSON_COURSE_GC, sData.getGC())
                .add(JSON_COURSE_AD, sData.getAD())
                .add(JSON_COURSE_SA, sData.getSA())
                .build();
        //System.out.println(siteData.getInsName());
        return object;
        
    }

    @Override
    public void loadData(JsonObject section, CourseSiteGeneratorData data) {
        SyllabusData sData = data.getSyllabusData();
        SyllabusWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getSyllabusWorkspace(); 
        sData.setDes(section.getString(JSON_COURSE_DES));
        workspace.setDesText(section.getString(JSON_COURSE_DES));
        sData.setPre(section.getString(JSON_COURSE_PRE));
        workspace.setPreText(section.getString(JSON_COURSE_PRE));
        sData.setOut(section.getString(JSON_COURSE_OUT));
        workspace.setOutText(section.getString(JSON_COURSE_OUT));
        sData.setTop(section.getString(JSON_COURSE_TOP));
        workspace.setTopText(section.getString(JSON_COURSE_TOP));
        sData.setText(section.getString(JSON_COURSE_TEXT));
        workspace.setTextText(section.getString(JSON_COURSE_TEXT));
        sData.setGC(section.getString(JSON_COURSE_GC));
        workspace.setGCText(section.getString(JSON_COURSE_GC));
        sData.setGN(section.getString(JSON_COURSE_GN));
        workspace.setGNText(section.getString(JSON_COURSE_GN));
        sData.setAD(section.getString(JSON_COURSE_AD));
        workspace.setADText(section.getString(JSON_COURSE_AD));
        sData.setSA(section.getString(JSON_COURSE_SA));
        workspace.setSAText(section.getString(JSON_COURSE_SA));
    }

    JsonObject generateSYDataJSON(CourseSiteGeneratorData data) {
        SyllabusData sData = data.getSyllabusData();
        JsonReader t=Json.createReader(new StringReader(sData.getTop()));
        JsonArray top=t.readArray();
        JsonReader o=Json.createReader(new StringReader(sData.getOut()));
        JsonArray out=o.readArray();
        JsonReader te=Json.createReader(new StringReader(sData.getText()));
        JsonArray text=te.readArray();
        JsonReader g=Json.createReader(new StringReader(sData.getGC()));
        JsonArray gc=g.readArray();
        JsonObject object = Json.createObjectBuilder()
                .add(JSON_COURSE_DES, sData.getDes())
                .add(JSON_COURSE_TOP, top)
                .add(JSON_COURSE_PRE, sData.getPre())
                .add(JSON_COURSE_OUT, out)
                .add(JSON_COURSE_TEXT, text)
                .add(JSON_COURSE_GN, sData.getGN())
                .add(JSON_COURSE_GC, gc)
                .add(JSON_COURSE_AD, sData.getAD())
                .add(JSON_COURSE_SA, sData.getSA())
                .build();
        //System.out.println(siteData.getInsName());
        return object;
    }
}
