package csg.file;

import csg.CourseSiteGeneratorApp;
import csg.data.CourseSiteGeneratorData;
import csg.data.Labs;
import csg.data.Lectures;
import csg.data.MeetingTimesData;
import csg.data.Recitations;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;

/**
 *
 * @author USER
 */
class MTFiles implements FileSectionManager {

    private CourseSiteGeneratorApp app;
    private static final String JSON_RECITATION = "recitations";
     //   private static final String JSON_MT = "meeting times";

    
    private static final String JSON_RECITATION_SECTION = "section";
    private static final String JSON_RECITATION_ROOM = "location";
    private static final String JSON_RECITATION_DAYSANDTIME = "day_time";
    private static final String JSON_RECITATION_TA1 = "ta_1";
    private static final String JSON_RECITATION_TA2 = "ta_2";
    
    private static final String JSON_LABS = "labs";
    private static final String JSON_LABS_SECTION = "section";
    private static final String JSON_LABS_ROOM = "location";
    private static final String JSON_LABS_DAYSANDTIME = "day_time";
    private static final String JSON_LABS_TA1 = "ta_1";
    private static final String JSON_LABS_TA2 = "ta_2";
    
    private static final String JSON_LECTURES = "lectures";
    private static final String JSON_LECTURES_SECTION = "section";
    private static final String JSON_LECTURES_ROOM = "room";
    private static final String JSON_LECTURES_DAYS = "days";
    private static final String JSON_LECTURES_TIME = "time";
    
    
    
    @Override
    public JsonObject generateDataJSON(CourseSiteGeneratorData data) {
        MeetingTimesData mtData = data.getMeetingTimesData();
        //JsonArrayBuilder mtArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder lecArrayBuilder = Json.createArrayBuilder();
        for (Lectures lec : mtData.getLectures()) {
            
            
            JsonObject lecObject = Json.createObjectBuilder()
                .add(JSON_LECTURES_SECTION, lec.getSection())
                .add(JSON_LECTURES_DAYS, lec.getDays())
                .add(JSON_LECTURES_ROOM, lec.getRoom())
                .add(JSON_LECTURES_TIME, lec.getTime())
                
                .build();
            
            lecArrayBuilder.add(lecObject);
        }
        //mtArrayBuilder.add(lecArrayBuilder);
        JsonArrayBuilder recArrayBuilder = Json.createArrayBuilder();
        for (Recitations recitation : mtData.getRecitations()) {
            
            
            JsonObject recitationObject = Json.createObjectBuilder()
                .add(JSON_RECITATION_SECTION, recitation.getSection())
                .add(JSON_RECITATION_DAYSANDTIME, recitation.getDaysandtime())
                .add(JSON_RECITATION_ROOM, recitation.getRoom())
                .add(JSON_RECITATION_TA1, recitation.getTa1())
                .add(JSON_RECITATION_TA2, recitation.getTa2())
                //.add(JSON_RECITATION_ASSISTANT_TA, assistantTA)
                .build();
            
            recArrayBuilder.add(recitationObject);
        }
        //mtArrayBuilder.add(recArrayBuilder);
        JsonArrayBuilder labArrayBuilder = Json.createArrayBuilder();
        for (Labs lab : mtData.getLabs()) {
            
            
            JsonObject labObject = Json.createObjectBuilder()
                .add(JSON_LABS_SECTION, lab.getSection())
                .add(JSON_LABS_DAYSANDTIME, lab.getDaysandtime())
                .add(JSON_LABS_ROOM, lab.getRoom())
                .add(JSON_LABS_TA1, lab.getTa1())
                .add(JSON_LABS_TA2, lab.getTa2())
                
                .build();
            
            labArrayBuilder.add(labObject);
        }
        JsonObject obj = Json.createObjectBuilder()
                .add(JSON_LECTURES, lecArrayBuilder.build())
                .add(JSON_RECITATION, recArrayBuilder.build())
                .add(JSON_LABS, labArrayBuilder.build()).build();
        
        
        return obj;

    }

    @Override
    public void loadData(JsonObject section, CourseSiteGeneratorData data) {
        JsonArray lecArray = section.getJsonArray(JSON_LECTURES);
        JsonArray recArray = section.getJsonArray(JSON_RECITATION);
        JsonArray labArray = section.getJsonArray(JSON_LABS);
        
        MeetingTimesData mtData = data.getMeetingTimesData();
        
        for(Object obj : lecArray.toArray()){
            JsonObject lecJsonObject = (JsonObject)obj;
            String lecSection = lecJsonObject.getString(JSON_LECTURES_SECTION);
            String lecRoom = lecJsonObject.getString(JSON_LECTURES_ROOM);
            String lecDays = lecJsonObject.getString(JSON_LECTURES_DAYS);
            String lecTime = lecJsonObject.getString(JSON_LECTURES_TIME);
            
            Lectures lec=new Lectures(lecSection,lecDays,lecTime,lecRoom);
            mtData.addLec(lec);
        }
        for(Object obj : recArray.toArray()){
            JsonObject recJsonObject = (JsonObject)obj;
            String recSection = recJsonObject.getString(JSON_RECITATION_SECTION);
            //String instructor = recJsonObject.getString(JSON_RECITATION_INSTRUCTOR);
            String recdaysandtime = recJsonObject.getString(JSON_RECITATION_DAYSANDTIME);
            String recRoom = recJsonObject.getString(JSON_RECITATION_ROOM);
            String recTA1 = recJsonObject.getString(JSON_RECITATION_TA1);
            String recTA2 = recJsonObject.getString(JSON_RECITATION_TA2);
            Recitations rec = new Recitations(recSection,recdaysandtime,recTA1,recTA2,recRoom);
            mtData.addRec(rec);
            
        }
        for(Object obj : labArray.toArray()){
            JsonObject labJsonObject = (JsonObject)obj;
            String labSection = labJsonObject.getString(JSON_LABS_SECTION);
            //String instructor = recJsonObject.getString(JSON_RECITATION_INSTRUCTOR);
            String labdaysandtime = labJsonObject.getString(JSON_LABS_DAYSANDTIME);
            String labRoom = labJsonObject.getString(JSON_LABS_ROOM);
            String labTA1 = labJsonObject.getString(JSON_LABS_TA1);
            String labTA2 = labJsonObject.getString(JSON_LABS_TA2);
            Labs lab = new Labs(labSection,labdaysandtime,labTA1,labTA2,labRoom);
            mtData.addLab(lab);
            
        }
        /*for (Object object : mtArray.toArray()) {
            
            JsonObject mtJsonObject = (JsonObject)object;
            JsonArray lecJsonObject = mtJsonObject.getJsonArray(JSON_LECTURES);
            JsonArray recJsonObject = mtJsonObject.getJsonArray(JSON_RECITATION);
            JsonArray labJsonObject = mtJsonObject.getJsonArray(JSON_LABS);
            for(O)
            String lecSection = lecJsonObject.get.getString(JSON_LECTURES_SECTION);
            String lecRoom = recitationJsonObject.getString(JSON_LECTURES_ROOM);
            String lecDays = recitationJsonObject.getString(JSON_LECTURES_DAYS);
            String lecTime = recitationJsonObject.getString(JSON_LECTURES_TIME);
            Lectures lec=new Lectures(lecSection,lecDays,lecTime,lecRoom);
            mtData.addLec(lec);

            recitationData.addRecitation(recitationSection, instructor, date, location, mainTA, assistantTA);
            String recitationSection = recitationJsonObject.getString(JSON_RECITATION_SECTION);
            String instructor = recitationJsonObject.getString(JSON_RECITATION_INSTRUCTOR);
            String date = recitationJsonObject.getString(JSON_RECITATION_DATE);
            String location = recitationJsonObject.getString(JSON_RECITATION_LOCATION);
            String mainTA = recitationJsonObject.getString(JSON_RECITATION_MAIN_TA);
            String assistantTA = recitationJsonObject.getString(JSON_RECITATION_ASSISTANT_TA);

            recitationData.addRecitation(recitationSection, instructor, date, location, mainTA, assistantTA);
            String recitationSection = recitationJsonObject.getString(JSON_RECITATION_SECTION);
            String instructor = recitationJsonObject.getString(JSON_RECITATION_INSTRUCTOR);
            String date = recitationJsonObject.getString(JSON_RECITATION_DATE);
            String location = recitationJsonObject.getString(JSON_RECITATION_LOCATION);
            String mainTA = recitationJsonObject.getString(JSON_RECITATION_MAIN_TA);
            String assistantTA = recitationJsonObject.getString(JSON_RECITATION_ASSISTANT_TA);

            recitationData.addRecitation(recitationSection, instructor, date, location, mainTA, assistantTA);
        }*/
    }

    
    
}
