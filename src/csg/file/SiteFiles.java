package csg.file;
/**
 *
 * @author Faizan Shaikh
 */
import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import csg.data.SiteData;
import csg.data.CourseSiteGeneratorData;
import csg.workspace.CourseSiteGeneratorWorkspace;
import csg.workspace.SiteWorkspace;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.collections.ObservableList;
//import com.google.gson.Gson;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import properties_manager.PropertiesManager;


public class SiteFiles implements FileSectionManager {

    private CourseSiteGeneratorApp app;
    private List list; 
    
    //constants for course files
    private static final String JSON_NAME = "name";
    private static final String JSON_LINK = "link";
    private static final String JSON_HOME_NAME = "Home";
    private static final String JSON_SYLLABUS_NAME = "Syllabus";
    private static final String JSON_SCHEDULE_NAME = "Schedule";
    private static final String JSON_HW_NAME = "HWs";
    private static final String JSON_HOME_LINK = "index.html";
    private static final String JSON_SYLLABUS_LINK = "syllabus.html";
    private static final String JSON_SCHEDULE_LINK = "schedule.html";
    private static final String JSON_HW_LINK = "hws.html";
    private static final String JSON_COURSE_SUBJECT = "subject";
    private static final String JSON_COURSE_NUMBER = "number";
    private static final String JSON_COURSE_YEAR = "year";
    private static final String JSON_COURSE_TITLE = "title";
    private static final String JSON_COURSE_INSTRUCTOR = "instructor";
    private static final String JSON_COURSE_INSTRUCTOR_SITE = "link";
    private static final String JSON_COURSE_INSTRUCTOR_PHOTO = "photo";
    private static final String JSON_COURSE_INSTRUCTOR_EMAIL = "email";
    private static final String JSON_COURSE_INSTRUCTOR_ROOM = "room";
    private static final String JSON_COURSE_INSTRUCTOR_OH = "hours";
    private static final String JSON_COURSE_EXPORT_DIR = "exportDir";
    private static final String JSON_COURSE_FAV = "favicon";
    private static final String JSON_COURSE_LEFTFOOTER = "bottom_left";
    private static final String JSON_COURSE_RIGHTFOOTER = "bottom_right";
    private static final String JSON_COURSE_STYLESHEET = "stylesheets";
    private static final String JSON_COURSE_NAV = "navbar";
    private static final String JSON_COURSE_SEMESTER = "semester";
    private static final String JSON_COURSE_LOGOS = "logos";
    private static final String JSON_HREF = "href";
    private static final String JSON_SRC = "src";
    private static final String JSON_COURSE_TEMPLATE_DIR = "templateDir";
    private static final String JSON_COURSE_PAGES = "pages";
    /*private static final String JSON_COURSE_PAGES_USE = "use";
    private static final String JSON_COURSE_PAGES_NAVBAR_TITLE = "navbarTitle";
    private static final String JSON_COURSE_PAGES_FILENAME = "fileName";
    private static final String JSON_COURSE_PAGES_SCRIPT = "script";*/
    public String [] sub;
    public ArrayList<String> subL;
    public String [] num;
    public ArrayList<String> numL;
   
    public SiteFiles(CourseSiteGeneratorApp app) {
        this.app = app;
        list=new ArrayList();
        subL=new ArrayList();
        numL=new ArrayList();
        
    }
    
    
  
    public JsonObject generateSiteDataJSON(CourseSiteGeneratorData data) {
        SiteData courseData = data.getSiteData();
        
 	JsonArrayBuilder pageArrayBuilder = Json.createArrayBuilder();
        //JsonArrayBuilder instructorArrayBuilder = Json.createArrayBuilder();
        JsonReader red=Json.createReader(new StringReader(courseData.getInsOH()));
        JsonArray oh=red.readArray();
        JsonObject nav = Json.createObjectBuilder()
                .add(JSON_HREF, "http://www.stonybrook.edu")
                .add(JSON_SRC, "./images/NavBar.png")
                .build();
        JsonObject fav = Json.createObjectBuilder().add(JSON_HREF, "./images/FavIcon.png").build();
        JsonObject left = Json.createObjectBuilder()
                .add(JSON_HREF, "http://www.cs.stonybrook.edu")
                .add(JSON_SRC, "./images/leftfooter.jpg")
                .build();
        JsonObject right = Json.createObjectBuilder()
                .add(JSON_HREF, "http://www.cs.stonybrook.edu")
                .add(JSON_SRC, "./images/rightfooter.png")
                .build();
        //JsonObject navArrayBuilder = Json.createObjectBuilder().add(JSON_COURSE_FAV, nav).build();
        //JsonObject favArrayBuilder = Json.createObjectBuilder().add(JSON_COURSE_NAV, fav).build();
        //JsonObject leftArrayBuilder = Json.createObjectBuilder().add(JSON_COURSE_LEFTFOOTER, left).build();
        //JsonObject rightArrayBuilder = Json.createObjectBuilder().add(JSON_COURSE_RIGHTFOOTER, right).build();
        JsonObject logosArrayBuilder = Json.createObjectBuilder()
                .add(JSON_COURSE_FAV, fav)
                .add(JSON_COURSE_NAV, nav)
                .add(JSON_COURSE_LEFTFOOTER, left)
                .add(JSON_COURSE_RIGHTFOOTER, right)
                .build();
        //logosArrayBuilder.add(navArrayBuilder);
        //logosArrayBuilder.add(favArrayBuilder);
        //logosArrayBuilder.add(leftArrayBuilder);
        //logosArrayBuilder.add(rightArrayBuilder);
        
        
        //for (SitePage page : courseData.getSitePages()) {
            JsonObject home = Json.createObjectBuilder()
                    .add(JSON_NAME,JSON_HOME_NAME )
                    .add(JSON_LINK,JSON_HOME_LINK )
                    //.add(JSON_COURSE_PAGES_FILENAME, page.getFileName())
                    //.add(JSON_COURSE_PAGES_SCRIPT, page.getScript())
                    .build();
            
            pageArrayBuilder.add(home);
            JsonObject syllabus = Json.createObjectBuilder()
                    .add(JSON_NAME, JSON_SYLLABUS_NAME)
                    .add(JSON_LINK, JSON_SYLLABUS_LINK)
                    //.add(JSON_COURSE_PAGES_FILENAME, page.getFileName())
                    //.add(JSON_COURSE_PAGES_SCRIPT, page.getScript())
                    .build();
            
            pageArrayBuilder.add(syllabus);
            JsonObject sch = Json.createObjectBuilder()
                    .add(JSON_NAME, JSON_SCHEDULE_NAME)
                    .add(JSON_LINK, JSON_SCHEDULE_LINK)
                    //.add(JSON_COURSE_PAGES_FILENAME, page.getFileName())
                    //.add(JSON_COURSE_PAGES_SCRIPT, page.getScript())
                    .build();
            
            pageArrayBuilder.add(sch);
            JsonObject hw = Json.createObjectBuilder()
                    .add(JSON_NAME, JSON_HW_NAME)
                    .add(JSON_LINK, JSON_HW_LINK)
                    //.add(JSON_COURSE_PAGES_FILENAME, page.getFileName())
                    //.add(JSON_COURSE_PAGES_SCRIPT, page.getScript())
                    .build();
            
            pageArrayBuilder.add(hw);
        //}
        JsonObject ins = Json.createObjectBuilder()
                .add(JSON_NAME, courseData.getInsName())
                .add(JSON_LINK, courseData.getInsHome())
                .add(JSON_COURSE_INSTRUCTOR_EMAIL, courseData.getInsEmail())
                .add(JSON_COURSE_INSTRUCTOR_ROOM, courseData.getInsRoom())
                .add(JSON_COURSE_INSTRUCTOR_PHOTO, "./images/RichardMcKenna.jpg")
                .add(JSON_COURSE_INSTRUCTOR_OH, oh)
                .build();
                //instructorArrayBuilder.add(ins);
        //JsonObject log = Json.createObjectBuilder()
               
        JsonObject object = Json.createObjectBuilder()
                //.add(JSON_COURSE_NAME, courseData.getTitle())
                .add(JSON_COURSE_SUBJECT, courseData.getSubject())
                .add(JSON_COURSE_NUMBER, courseData.getNumber())//.getCourseNumber())
                .add(JSON_COURSE_SEMESTER, courseData.getSemester())
                .add(JSON_COURSE_YEAR, courseData.getYear())
                .add(JSON_COURSE_TITLE, courseData.getTitle())
                .add(JSON_COURSE_INSTRUCTOR,ins )//.getInstructor())
                //.add(JSON_COURSE_INSTRUCTOR_SITE, courseData.getInsHome())//.getSite())
                .add(JSON_COURSE_LOGOS, logosArrayBuilder)
                
                .add(JSON_COURSE_PAGES, pageArrayBuilder.build())
        
                .build();
        return object;        
          
    }
    
    public JsonObject generateDataJSON(CourseSiteGeneratorData data) {
        
        
       SiteData siteData = data.getSiteData();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	JsonArrayBuilder pageArrayBuilder = Json.createArrayBuilder();
        
       SiteWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
               
        JsonObject object = Json.createObjectBuilder()
                .add(JSON_COURSE_TITLE, siteData.getTitle())
                .add(JSON_COURSE_SUBJECT, siteData.getSubject())
                .add(JSON_COURSE_NUMBER, siteData.getNumber())
                .add(JSON_COURSE_SEMESTER, siteData.getSemester())
                .add(JSON_COURSE_YEAR, siteData.getYear())
                .add(JSON_COURSE_INSTRUCTOR, siteData.getInsName())
                .add(JSON_COURSE_INSTRUCTOR_EMAIL, siteData.getInsEmail())
                .add(JSON_COURSE_INSTRUCTOR_ROOM, siteData.getInsRoom())
                .add(JSON_COURSE_INSTRUCTOR_SITE, siteData.getInsHome())
                .add(JSON_COURSE_INSTRUCTOR_OH, siteData.getInsOH())
                .add(JSON_COURSE_EXPORT_DIR, siteData.getExportDir())
                .add(JSON_COURSE_FAV, siteData.getFaviconPath())
                .add(JSON_COURSE_NAV, siteData.getNavbarPath())
                .add(JSON_COURSE_LEFTFOOTER, siteData.getLeftFooterPath())
                .add(JSON_COURSE_RIGHTFOOTER, siteData.getRightFooterPath())
                .add(JSON_COURSE_STYLESHEET, siteData.getStyleSheetName())
                .add(JSON_COURSE_TEMPLATE_DIR, siteData.getTemplateDirectory())
                .add(JSON_COURSE_PAGES, pageArrayBuilder.build())
        
                .build();
        //if(!workspace.getMajorCodes().contains(siteData.getSubject())){
             try{
                 //File fileToBeModified = new File("siteText.txt");
                 //System.out.println(fileToBeModified.toString());
                 String oldContent = "";
                 BufferedReader reader = new BufferedReader(new FileReader("siteText.txt"));
                 String line;// = reader.readLine();
                 //System.out.println(fileToBeModified.toString());
                if ((line = reader.readLine()) != null) 
                    {
                        
                        oldContent = oldContent + line + "";
                        //+"\t"+oldContent);
                        //line = reader.readLine();
                    }
                if(!oldContent.contains(""+siteData.getSubject())){
                    String newContent = oldContent+""+siteData.getSubject()+" ";
                    System.out.println(newContent);
                    FileWriter writer = new FileWriter("siteText.txt");
                    writer.write(newContent);
                    writer.close();
                }
                
               
               
                reader.close();
                if(!siteData.getNumber().equals("")){
                    if(Integer.parseInt(siteData.getNumber())>workspace.getmaxCourseNum()){
                        workspace.setmaxCourseNum(Integer.parseInt(siteData.getNumber()));
                    }
                    else{
                    }
                }
        }catch(IOException e){
        e.printStackTrace();
        }
        
        return object;
    }

   
    @Override
    public void loadData(JsonObject section, CourseSiteGeneratorData data) throws NullPointerException {
        SiteData siteData = data.getSiteData();
        SiteWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
         workspace.settypeClick("l");
        //load all the basic course information
        siteData.setTitle(section.getString(JSON_COURSE_TITLE));
        siteData.setSubject(section.getString(JSON_COURSE_SUBJECT));
        siteData.setNumber(section.getString(JSON_COURSE_NUMBER));
        //workspace.getNumberBox().setValue(section.getString(JSON_COURSE_NUMBER));
        siteData.setYear(section.getString(JSON_COURSE_YEAR));
        siteData.setSemester(section.getString(JSON_COURSE_SEMESTER));
        siteData.setInsName(section.getString(JSON_COURSE_INSTRUCTOR));
        siteData.setInsEmail(section.getString(JSON_COURSE_INSTRUCTOR_EMAIL));
        siteData.setInsRoom(section.getString(JSON_COURSE_INSTRUCTOR_ROOM));
        siteData.setInsHome(section.getString(JSON_COURSE_INSTRUCTOR_SITE));
        siteData.setInsOH(section.getString(JSON_COURSE_INSTRUCTOR_OH));
        workspace.setInsOHText(section.getString(JSON_COURSE_INSTRUCTOR_OH));
        siteData.setExportDir(section.getString(JSON_COURSE_EXPORT_DIR));
        //workspace.setExportLabel(section.getString(JSON_COURSE_EXPORT_DIR));
        siteData.setFaviconPath(section.getString(JSON_COURSE_FAV));
        siteData.setNavbarPath(section.getString(JSON_COURSE_NAV));
        siteData.setLeftFooterPath(section.getString(JSON_COURSE_LEFTFOOTER));
        siteData.setRightFooterPath(section.getString(JSON_COURSE_RIGHTFOOTER));
        siteData.setStyleSheetName(section.getString(JSON_COURSE_STYLESHEET));
        siteData.setTemplateDir(section.getString(JSON_COURSE_TEMPLATE_DIR));
        try{
        //File fileToBeModified = new File("siteText.txt");
        String oldContent = "";
        BufferedReader reader = new BufferedReader(new FileReader("siteText.txt"));
        String line = reader.readLine();
        while (line != null) 
                    {
                        //System.out.println(line);
                        oldContent = oldContent + line + "";
                        System.out.println(oldContent);
                        line = reader.readLine();
                    }
        if(!oldContent.trim().isEmpty()){
            sub=oldContent.split(" ");
            for(int j=0;j<sub.length;j++){
               subL.add(sub[j]);
            }
            siteData.setSubL(subL);
        }
        }catch(IOException e){
        e.printStackTrace();
        }
        //SiteWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        if(!siteData.getNumber().equals("")){
            if(Integer.parseInt(siteData.getNumber())>workspace.getmaxCourseNum()){
                workspace.setmaxCourseNum(Integer.parseInt(siteData.getNumber()));
            }
            else{
            }
        }
    }
    public ArrayList getSub(){
        return subL;
    }
    public void writeJSONFile(JsonObject object, String filePath) throws FileNotFoundException {
           Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);

        StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(object);
	jsonWriter.close();
        
  
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(object);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();        
    }

 

    
}
