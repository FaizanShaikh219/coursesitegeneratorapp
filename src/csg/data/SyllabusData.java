package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Faizan Shaikh
 */
public class SyllabusData {
    private StringProperty des;
    private StringProperty pre;
    private StringProperty top;
    private StringProperty out;
    private StringProperty text;
    private StringProperty gc;
    private StringProperty gn;
    private StringProperty ad;
    private StringProperty sa;
    
    public SyllabusData() {
        des = new SimpleStringProperty();
        top = new SimpleStringProperty();
        pre = new SimpleStringProperty();
        out = new SimpleStringProperty();
        text = new SimpleStringProperty();
        gc = new SimpleStringProperty();
        gn = new SimpleStringProperty();
        ad = new SimpleStringProperty();
        sa = new SimpleStringProperty();
    }
    public String getDes() {
        return des.getValue();
    }

    
    public StringProperty getDesProperty() {
        return des;
    }

    
    public void setDes(String d) {

        this.des.setValue(d);
    }
    public String getTop() {
        return top.getValue();
    }

    
    public StringProperty getTopProperty() {
        return top;
    }

    
    public void setTop(String t) {

        this.top.setValue(t);
    }
    public String getPre() {
        return pre.getValue();
    }

    
    public StringProperty getPreProperty() {
        return pre;
    }

    
    public void setPre(String p) {

        this.pre.setValue(p);
    }
    public String getOut() {
        return out.getValue();
    }

    
    public StringProperty getOutProperty() {
        return out;
    }

    
    public void setOut(String o) {

        this.out.setValue(o);
    }
    public String getText() {
        return text.getValue();
    }

    
    public StringProperty getTextProperty() {
        return text;
    }

    
    public void setText(String t) {

        this.text.setValue(t);
    }
    public String getGN() {
        return gn.getValue();
    }

    
    public StringProperty getGNProperty() {
        return gn;
    }

    
    public void setGN(String gn) {

        this.gn.setValue(gn);
    }
    public String getGC() {
        return gc.getValue();
    }

    
    public StringProperty getGCProperty() {
        return gc;
    }

    
    public void setGC(String g) {

        this.gc.setValue(g);
    }
    public String getAD() {
        return ad.getValue();
    }

    
    public StringProperty getADProperty() {
        return ad;
    }

    
    public void setAD(String a) {

        this.ad.setValue(a);
    }
    public String getSA() {
        return sa.getValue();
    }

    
    public StringProperty getSAProperty() {
        return sa;
    }

    
    public void setSA(String s) {

        this.sa.setValue(s);
    }
    void resetData(){
        des.setValue("");
        pre.setValue("");
        top.setValue("");
        out.setValue("");
        text.setValue("");
        gc.setValue("");
        gn.setValue("");
        sa.setValue("");
        ad.setValue("");
    }

    
}
