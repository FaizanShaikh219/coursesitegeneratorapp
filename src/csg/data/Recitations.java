package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Faizan Shaikh
 */
public class Recitations <E extends Comparable<E>> implements Comparable<E> {

    private final StringProperty section;
    private final StringProperty daysandtime;
    private final StringProperty ta1;
    private final StringProperty ta2;
    private final StringProperty room;
    public Recitations() {
        section = new SimpleStringProperty("?");
        daysandtime = new SimpleStringProperty("?");
        ta1 = new SimpleStringProperty("?");
        ta2 = new SimpleStringProperty("?");
        room=new SimpleStringProperty("?");
    }
    public Recitations(String sec, String day, String ta1, String ta2, String room) {
        section = new SimpleStringProperty(sec.toString());
        daysandtime = new SimpleStringProperty(day.toString());
        this.ta1 = new SimpleStringProperty(ta1.toString());
        this.ta2 = new SimpleStringProperty(ta2.toString());
        this.room=new SimpleStringProperty(room.toString());
    }
    public String getSection() {
        return section.get();
    }
    
    public void setSection(String sec) {
        section.setValue(sec);
    }
    public String getDaysandtime() {
        return daysandtime.get();
    }
    
    public void setDaysandtime(String da) {
        daysandtime.setValue(da);
    }
    public String getTa1() {
        return ta1.get();
    }
    
    public void setTa1(String ta1) {
        this.ta1.setValue(ta1);
    }
    public String getTa2() {
        return ta2.get();
    }
    
    public void setTa2(String ta2) {
        this.ta2.setValue(ta2);
    }
    public String getRoom() {
        return room.get();
    }
    
    public void setRoom(String roo) {
        room.setValue(roo);
    }
    @Override
    public int compareTo(E o) {
        return ((getSection())).compareTo((((Recitations)o).getSection()));
    }
    
}
