package csg.data;
/**
 *
 * @author Faizan Shaikh
 */
import csg.CourseSiteGeneratorApp;
import csg.CourseSiteGeneratorProp;
import csg.workspace.CourseSiteGeneratorWorkspace;
import csg.workspace.OfficeHoursWorkspace;
import djf.ui.AppGUI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ToggleGroup;
import properties_manager.PropertiesManager;


public class TAData {
    HashMap<String,ArrayList<TimeSlotPosition>> list;
    private ObservableList<TeachingAssistant> teachingAssistants;
    private ArrayList<TeachingAssistant> backTA;
    private ObservableList<OHTimeSlot> officeHours;
    private int startHour = 7;
    private int endHour = 20;
    CourseSiteGeneratorApp app;
    CourseSiteGeneratorWorkspace workspace;
    OfficeHoursWorkspace taWorkspace;
    private List<String> headerList;
         
    public TAData(CourseSiteGeneratorApp initApp) {
        app=initApp;
        AppGUI gui = app.getGUI();
        
        //TableView<TeachingAssistant> taTable = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
       teachingAssistants = FXCollections.observableArrayList();
       backTA=new ArrayList();
       officeHours = FXCollections.observableArrayList();
       list=new HashMap();
       
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        headerList = props.getPropertyOptionsList(CourseSiteGeneratorProp.DAYS_OF_WEEK);

    }
    
   public boolean isLegalNewTA(String name, String email) {
        if (name.contains(" ")&&name.length()-1>name.indexOf(" ")&&name.trim().length() > 0
                && (email.trim().length() > 0)) {
            // MAKE SURE NO TA ALREADY HAS THE SAME NAME
            CourseSiteGeneratorWorkspace workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        OfficeHoursWorkspace taWorkspace = workspace.getTAWorkspace();
        ToggleGroup tg=taWorkspace.getToggleGroup();
            TeachingAssistant testTA = new TeachingAssistant(name, email,tg.getSelectedToggle().toString());
            if (this.backTA.contains(testTA))
                
                return false;
            if (this.isLegalNewEmail(email)) {
                return true;
            }
        }
        return false;
    }
   public boolean isLegalNewName(String name){
        if (name.contains(" ") &&name.length()-1>name.indexOf(" ")&& name.trim().length() > 0) {
            // MAKE SURE NO TA ALREADY HAS THE SAME NAME
            //TeachingAssistantPrototype testTA = new TeachingAssistantPrototype(name, email);
            int i=0;
            while(i<this.backTA.size()){
                if(name.equals(this.backTA.get(i).getName())) return false;
                else {
                    i++;
                }
            }
                
            return true;
           
        }
        return false;
    }
    public boolean isLegalNewEmail(String email) {
        Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(
                "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);
        if (matcher.find()) {
            for (TeachingAssistant ta : this.backTA) {
                if (ta.getEmail().equals(email.trim()))
                    return false;
            }
            return true;
        }
        else return false;
    }
    /**
     * 
     * @return list of teaching assistants
     */
    public ObservableList<TeachingAssistant> getTeachingAssistants() {
        return teachingAssistants;
    }

    /**
     * 
     * @return list of office hour hour slots
     */
    public ObservableList<OHTimeSlot> getOfficeHours() {
        return officeHours;
    }
    
    /**
     * 
     * @return the end hour for office hours
     */
    public int getStartHour() {
        return startHour;
    }
    public void setStartHour(int sh) {
         startHour = sh;
    }
    
    /**
     * 
     * @return the start hour for office hours
     */
    public int getEndHour() {
        return endHour;
    }
    public void setEndHour(int eh) {
         endHour=eh;
    }
    
    /**
     * initialize office hours
     * @param starHour the start hour
     * @param endHour the end hour
     */
    public void initHours(int starHour, int endHour) {
        officeHours.clear();
        this.startHour = startHour;
        this.endHour = endHour;
        buildOfficeHourTimeSlots();
    }
    /**
     * Add a new TA
     * 
     * @param name the TA name
     * @param email the TA email
     * @param isUnderGrad is the TA an undergraduate
     */
    public void addTA(TeachingAssistant ta) {
        workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        taWorkspace = workspace.getTAWorkspace();
        teachingAssistants=taWorkspace.getTaTable().getItems();
        teachingAssistants.add(ta);
        list.put(ta.getName(), new ArrayList<TimeSlotPosition>());
        backTA.add(ta);
        
    }

    /**
     * Remove teaching assistant
     * 
     * @param name the TA name
     * @param email the TA email
     */
    public void removeTA(TeachingAssistant tas) {
        workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        taWorkspace = workspace.getTAWorkspace();
        teachingAssistants=taWorkspace.getTaTable().getItems();
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().equals(tas.getName()) && ta.getEmail().equals(tas.getEmail())) {
                teachingAssistants.remove(ta);
                backTA.remove(ta);
                break;
            }
        }
        list.remove(tas);
    }               

    /**
     * Find a teaching assistant by name
     * @param name the name
     * @return 
     */
    public TeachingAssistant getTA(String name) {
        workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        taWorkspace = workspace.getTAWorkspace();
        teachingAssistants=taWorkspace.getTaTable().getItems();
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().equals(name)) {
                return ta;
            }
        }  
        return null;
    }
    
    public void editTA(String name, String email, TeachingAssistant ta,String type) {
       
        
        //for (TeachingAssistant ta : teachingAssistants) {
            
            //if (ta.getName().equals(name) 
            //        && ((TeachingAssistant)ta).getEmail().equals(email)) {
      
                //if (!name.equals(name)) {
                    for(TeachingAssistant t:backTA){
                        if(t.equals(ta)){
                            t.setEmail(email);
                            t.setName(name);
                            t.setType(type);
                        }
                    }
                    changeOHNames(name, ta);
                    ta.setName(name);             
                //}
            
                //if (!ta.getEmail().equals(email)) {
                    ta.setEmail(email);
                //} 
                
                    
                    
                    ta.setType(type); 
                    
                
            

                //break;
            //}
       // }   
    }
    
   /**
    * modify a name on the office hours table.
    * 
    * @param name the new name to replace the old name
    * @param oldName  the name to replace with the new name
    */
    
    public void changeOHNames(String name, TeachingAssistant ta) {
        
        for (OHTimeSlot slot : officeHours) {
           
           for (int i = 0; i < slot.getRowData().length; i++) {
               String rowText = slot.getRowData()[i].getValueSafe();
               

               if (rowText.contains(ta.getName())) {

                    TimeSlotPosition pos = new TimeSlotPosition(i, slot.getStartHour(), slot.getStartMinute() == 0);
                    toggleTA(pos, ta,ta.getName());
                    toggleTA(pos, ta,name);

               }
           }
       }
    }
    
    /**
     * 
     * @param row the table index of row, every hour has two indexes.
     * @param day header value of day
     * @return 
     */
    
    public TimeSlotPosition getTimeSlotPosition(int row, String day) {
        
        int column = headerList.indexOf(day);
        
        int hour;
        boolean onHour;
        
        if (row % 2 == 1) {
            hour = row - 1;
            onHour = false;
        }
        else {
            hour = row;
            onHour = true;
        }
        
        hour = hour / 2;
        hour = hour + startHour;
        
        
        return new TimeSlotPosition(column, hour, onHour);
       
    }
   
    /**
     * 
     * @return a cloned list of all office hours.
     */
    public List<OHTimeSlot> getOfficeHoursState() {
        
        List state = new ArrayList();
        
        for (OHTimeSlot slot : officeHours) {
            state.add(slot.copy());
        }
        
        return state;
    }
    /**
     * 
     * @param state An array containing an alternative state of the office hours table
     * @param newStart the start hour of this alternative state
     * @param newEnd the end hour of this alternative state
     */
    
    public void setOfficeHourState(List<OHTimeSlot> state, int newStart, int newEnd) {
        
        officeHours.clear();
       
        for (OHTimeSlot slot : state) {
            officeHours.add(slot);
        }
       
        startHour = newStart;
        endHour = newEnd;
        Collections.sort(officeHours); 
    }
    
    /**
     * 
     * @param newStart the new office hour start boundaries
     * @param newEnd the new office hour end boundaries
     * @return 
     */
    public void changeTimeBounds(int newStart, int newEnd,String type) {
        
        //List<OHTimeSlot> delSlots = new ArrayList();
        officeHours.clear();
        this.setEndHour(newEnd);
        this.setStartHour(newStart);
        //this.buildOfficeHourTimeSlots();
        this.selectTAs(type);
        /*if (newStart != startHour) {
        
            if (newStart > startHour) {
                for (OHTimeSlot slot : officeHours) { 
                    
                    
                    if (slot.getStartHour() < newStart) {
                        //System.out.println(slot.getRowData().toString());
                            delSlots.add(slot.copy());
                          Platform.runLater( () -> officeHours.remove(slot));
                    }
                }
            }
            else {
                for (int i = newStart; i < startHour; i++) {
                    OHTimeSlot ts1=new OHTimeSlot(headerList, i, 0, i, 30);
                    OHTimeSlot ts2=new OHTimeSlot(headerList, i, 30, i+1, 0);
                    int c=0;
                    /*for(OfficeHourTimeSlot slot : delSlots){
                        if(slot.getStartHour()==i){
                            officeHours.add(slot);
                            delSlots.remove(slot);
                            //officeHours.add(ts2);
                            System.out.println("yees");
                            c=1;
                        }
                        else
                            c=0;
                        
                    }
                    if(c==0){
                        officeHours.add(ts1);
                        officeHours.add(ts2);
                    //}
                }
            }
        }
        
        else if (newEnd != endHour) {
         
            if (newEnd < endHour) {            
                for (OHTimeSlot slot : officeHours) {      
                    

                    if (slot.getStartHour() >= newEnd) {
                        delSlots.add(slot.copy());
                          Platform.runLater( () -> 
                        officeHours.remove(slot));
                    }               
                }
            }
            else {
             
                for (int i = endHour; i < newEnd; i++) {
                    OHTimeSlot ts1=new OHTimeSlot(headerList, i, 0, i, 30);
                    OHTimeSlot ts2=new OHTimeSlot(headerList, i, 30, i+1, 0);
                    
                    officeHours.add(ts1);
                    officeHours.add(ts2);            
                }
            }    
        }
        
        startHour = newStart;
        endHour = newEnd;
        Collections.sort(officeHours);  
        return delSlots;
        */
    }
    
    /**
     * Get a list of office hour positions for the TA
     * @param name the ta name
     * @return a list of positions containing ta office hours
     */
    public List<TimeSlotPosition> getTAOfficeHours(String name) {
       
       List<TimeSlotPosition> list = new ArrayList<>();
       
       for (OHTimeSlot slot : officeHours) {
           
           for (int i = 0; i < slot.getRowData().length; i++) {
               String rowData = slot.getRowData()[i].getValueSafe();
               
               if (rowData.contains(name)) {
                   
                   list.add(new TimeSlotPosition(i, slot.getStartHour(), slot.getStartMinute() == 0));
               }
           }
       }
       
       return list;
    }
    
    /**
     * Toggle TA for office hour time slot.
     * 
     * @param slot Position of time slot.
     * @param name TA name
     */
    public void toggleTA(TimeSlotPosition slot, TeachingAssistant ta,String name) {
       
        StringProperty cellTextProperty = officeHours.get(slot.getTableRow()).getDayTimeSlot(headerList.get(slot.dayIndex));
        String cellText = cellTextProperty.getValueSafe();
        
        if (cellText.contains(name)) {  
           removeTAFromCell(cellTextProperty, name);
           //OfficeHourTimeSlot oslot;
           list.get(ta.getName()).remove(slot);
           ta.setSlots(""+(Integer.parseInt(ta.getSlots())-1));
        } 
        else if (cellText.length() == 0) {
            cellTextProperty.setValue(name);
            ta.setSlots(""+(Integer.parseInt(ta.getSlots())+1));
            list.get(ta.getName()).add(slot);//.put(ta.getName(), slot);
            //System.out.println(slot.toString());
        } 
        else {
            cellTextProperty.setValue(cellText + "\n" + name);
            ta.setSlots(""+(Integer.parseInt(ta.getSlots())+1));
            list.get(ta.getName()).add(slot);
            //System.out.println(slot.toString());
        }
        
        
    }
    public void togglesTA(TimeSlotPosition slot, TeachingAssistant ta,String name) {
        if(slot.getHour()<this.getStartHour()||slot.getHour()>this.getEndHour()){//officeHours.get(slot.getTableRow())){
           
        }
        else if(slot.getHour()==this.getEndHour()&&slot.getMinute()>0){
            
        }
        else{ 
           StringProperty cellTextProperty = officeHours.get(slot.getTableRow()).getDayTimeSlot(headerList.get(slot.dayIndex));
            String cellText = cellTextProperty.getValueSafe();
        
            if (cellText.contains(name)) {  
                removeTAFromCell(cellTextProperty, name);
           //OfficeHourTimeSlot oslot;
           //list.get(ta.getName()).remove(slot);
           //ta.setSlots(""+(Integer.parseInt(ta.getSlots())-1));
            } 
            else if (cellText.length() == 0) {
                cellTextProperty.setValue(name);
            //ta.setSlots(""+(Integer.parseInt(ta.getSlots())+1));
            //list.get(ta.getName()).add(slot);//.put(ta.getName(), slot);
            } 
            else {
                cellTextProperty.setValue(cellText + "\n" + name);
            //ta.setSlots(""+(Integer.parseInt(ta.getSlots())+1));
            //list.get(ta.getName()).add(slot);
            }
       }
        
    }
        
    /**
     * remove ta from cell string property
     * @param cellProp the cell property containing TA
     * @param name the name to be removed
     */
    private void removeTAFromCell(StringProperty cellProp, String name) {
        // GET THE CELL TEXT
        String cellText = cellProp.getValue();
        // IS IT THE ONLY TA IN THE CELL?
        if (cellText.equals(name)) {
            cellProp.setValue("");
        }
        // IS IT THE FIRST TA IN A CELL WITH MULTIPLE TA'S?
        else if (cellText.indexOf(name) == 0) {
            int startIndex = cellText.indexOf("\n") + 1;
            cellText = cellText.substring(startIndex);
            cellProp.setValue(cellText);
        }
        // IS IT IN THE MIDDLE OF A LIST OF TAs
        else if (cellText.indexOf(name) < cellText.indexOf("\n", cellText.indexOf(name))) {
            int startIndex = cellText.indexOf("\n" + name);
            int endIndex = startIndex + name.length() + 1;
            cellText = cellText.substring(0, startIndex) + cellText.substring(endIndex);
            cellProp.setValue(cellText);
        }
        // IT MUST BE THE LAST TA
        else {
            int startIndex = cellText.indexOf("\n" + name);
            cellText = cellText.substring(0, startIndex);
            cellProp.setValue(cellText);
        }
    }

    /**
     * Build office hour table
     */
    public void buildOfficeHourTimeSlots() {

        for (int i = startHour; i < endHour; i++) {
            officeHours.add(new OHTimeSlot(headerList, i, 0, i, 30));
            officeHours.add(new OHTimeSlot(headerList, i, 30, i+1, 0));
        }
    }
        
    public boolean containsTA(String name) {
        
        for (TeachingAssistant ta : backTA) {
            if (ta.getName().equals(name)) {
                return true;
            }
        }   
        return false;
    }  
    
    /**
     * clear teaching assistant table and office hour grid.
     */
    public void resetData() {
        teachingAssistants.clear();
        officeHours.clear();
        startHour = 7;
        endHour = 20;
        buildOfficeHourTimeSlots();
    }

    public void selectTAs(String type) {
        teachingAssistants.clear();
        officeHours.clear();
        this.buildOfficeHourTimeSlots();
        // = new HashMap<>();
        
        
        if(type.equals("All")){
            for(TeachingAssistant ta:backTA){
                teachingAssistants.add(ta);
                for(TimeSlotPosition tsp:list.get(ta.getName())){
                    this.togglesTA(tsp, ta, ta.getName());
                }
            }
            
        }
        else if(type.equals("Undergraduate")){
            for(TeachingAssistant ta:backTA){
                if(ta.getType().equals("Undergraduate")){
                    teachingAssistants.add(ta);
                    for(TimeSlotPosition tsp:list.get(ta.getName())){
                    this.togglesTA(tsp, ta, ta.getName());
                }
                }
            }
        }
        else if(type.equals("Graduate")){
            for(TeachingAssistant ta:backTA){
                if(ta.getType().equals("Graduate")){
                    teachingAssistants.add(ta);
                    for(TimeSlotPosition tsp:list.get(ta.getName())){
                    this.togglesTA(tsp, ta, ta.getName());
                }
                }
            }
        }
    }
    
    
    /**
     * internal class which handles table position objects for office hours
     */
    public class TimeSlotPosition {
        
        private final int hour;
        private final int dayIndex;
        private final boolean onHour;
        
        public TimeSlotPosition(int dayIndex, int hour, boolean onHour) {
            this.hour = hour;
            this.dayIndex = dayIndex;
            this.onHour = onHour;
        }
        
        public int getHour() { return hour;}
        public int getDayIndex() {
            return dayIndex;
        }
        public String getDay() {
            return TAData.this.getHeaderList().get(dayIndex);
        }
        
        public boolean getOnHour() {
            return onHour;
        }
        
        public int getMinute() {
            return onHour ? 0 : 30;
        }
        public int getTableRow() { 
            
            int row = (hour - startHour) * 2;
            row += onHour ? 0 : 1;
            return row;
        }
       
    }
    
    /**
     * manually set the headers list.
     * @param list the new list of headers.
     */
    public void setHeaderList(List<String> list) {
        headerList = list;
    }
    /**
     * 
     * @return the header list
     */
    public List<String> getHeaderList() {
        return headerList;
    }
    /**
     * 
     * @param day the header day name
     * @return the index of the day
     */
    public int getDayIndexFromString(String day) {
        return headerList.indexOf(day);
    }
}
