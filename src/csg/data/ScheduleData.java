package csg.data;
/**
 *
 * @author Faizan Shaikh
 */
import java.time.LocalDate;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


public class ScheduleData {
        private ObservableList<ScheduleItem> scheduleItems;
        private ArrayList<ScheduleItem> backSch;
        private LocalDate startingDate;
        private LocalDate endingDate;
        
    public ScheduleData() {
       scheduleItems = FXCollections.observableArrayList();
       backSch=new ArrayList();
       startingDate = LocalDate.now();
       endingDate = LocalDate.now();
    }
    
    public ObservableList<ScheduleItem> getScheduleItems() {
        return scheduleItems;
    }
    public ArrayList<ScheduleItem> getBackScheduleItems() {
        return backSch;
    }

    
    public LocalDate getStartingDate() {
        return startingDate;
    }

    
    public LocalDate getEndingDate() {
        return endingDate;
    }

    
    public void setScheduleItems(ObservableList<ScheduleItem> scheduleItems) {
        this.scheduleItems = scheduleItems;
    }

   
    public void setStartingDate(LocalDate startingDate) {
        this.startingDate = startingDate;
    }

   
    public void setEndingDate(LocalDate endingDate) {
        this.endingDate = endingDate;
    }

    public void addBackItem(String type, LocalDate date, String title, String topic,  String link) {
        //scheduleItems.add(new ScheduleItem(type, date, title, topic, link));
        backSch.add(new ScheduleItem(type, date, title, topic, link));
    }
    public void addScheduleItem(String type, LocalDate date, String title, String topic,  String link) {
        scheduleItems.add(new ScheduleItem(type, date, title, topic, link));
        backSch.add(new ScheduleItem(type, date, title, topic, link));
    }
    
    
    public void removeScheduleItem(String type, LocalDate date, String title, String topic, String link) {
        for (ScheduleItem item : scheduleItems) {
            if (item.getType().equals(type) 
                    && item.getDate().equals(date) 
                    && item.getTitle().equals(title) 
                    && item.getTopic().equals(topic)
                    
                    &&item.getLink().equals(link) 
                    ) {
                
                scheduleItems.remove(item);
                backSch.remove(item);
                break;
            }
            
        }
        
    }
    
    
    public boolean containsScheduleItem(String type, LocalDate date, String title, String topic, String link) {
        
           
        for (ScheduleItem item : backSch) {
            if (item.getType().equals(type) 
                    && item.getDate().equals(date) 
                    && item.getTitle().equals(title) 
                    && item.getTopic().equals(topic)
                    &&item.getLink().equals(link) 
                    ) {
                
                return true;
            }
            
        }        
        return false;
        
    }
    
    
    void resetData() {
        scheduleItems.clear();
        backSch.clear();
        startingDate = LocalDate.now();
        endingDate = LocalDate.now();
    }

    
    public void replaceItem(ScheduleItem item, ScheduleItem oldItem) {
        for (ScheduleItem sItem : scheduleItems) {
            
                        if (sItem.getType().equals(oldItem.getType()) 
                    && sItem.getDate().equals(oldItem.getDate()) 
                    && sItem.getTitle().equals(oldItem.getTitle()) 
                    && sItem.getTopic().equals(oldItem.getTopic())
                    
                    &&sItem.getLink().equals(oldItem.getLink()) 
                    ) {
                
                scheduleItems.set(scheduleItems.indexOf(sItem), item);
               // backSch.set(backSch.indexOf(sItem), item);
                break;
            }
        }
        for (ScheduleItem sItem : backSch) {
            
                        if (sItem.getType().equals(oldItem.getType()) 
                    && sItem.getDate().equals(oldItem.getDate()) 
                    && sItem.getTitle().equals(oldItem.getTitle()) 
                    && sItem.getTopic().equals(oldItem.getTopic())
                    
                    &&sItem.getLink().equals(oldItem.getLink()) 
                    ) {
                
                //scheduleItems.set(scheduleItems.indexOf(sItem), item);
                backSch.set(backSch.indexOf(sItem), item);
                break;
            }
        }
        
        
    }

    public void dateChange(LocalDate sld,LocalDate eld) {
        this.setStartingDate(sld);
        this.setEndingDate(eld);
    }

    
}