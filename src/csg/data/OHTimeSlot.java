package csg.data;
/**
 *
 * @author Faizan Shaikh
 */
import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class OHTimeSlot implements Comparable<OHTimeSlot> {

    List<String> dayNames;
    StringProperty[] rowData;
    
   
    StringProperty startTime;
    private int startHour;
    private int startMinute;
    
  
    StringProperty endTime;
    private int endHour;
    private int endMinute;
    
    public OHTimeSlot(List<String> day, int startHour, int startMinute, int endHour, int endMinute) {
        

        rowData = new SimpleStringProperty[day.size()];   
        
        for (int i = 0; i < day.size(); i++) 
            rowData[i] = new SimpleStringProperty(); 
        
        this.startHour = startHour;
        this.startMinute = startMinute;
        this.endHour = endHour;
        this.endMinute = endMinute;
        this.dayNames = day;

        startTime = new SimpleStringProperty(formatTime(startHour, startMinute));
        endTime = new SimpleStringProperty(formatTime(endHour, endMinute));

    }
    
    
    public OHTimeSlot copy() {
       
        return new OHTimeSlot(this);
    }
    

    private OHTimeSlot(OHTimeSlot slot) {
        //constructor is used for duplicating class.
        startHour = slot.startHour;
        startMinute = slot.startMinute;
        endHour = slot.endHour;
        endHour = slot.endMinute;
        startTime = new SimpleStringProperty(slot.startTime.getValue());
        endTime = new SimpleStringProperty(slot.endTime.getValue());
        rowData = new SimpleStringProperty[slot.rowData.length]; 
        
        dayNames = slot.getDayNames();
        
        for (int i = 0; i < slot.rowData.length; i++) {
            rowData[i] = new SimpleStringProperty(slot.rowData[i].getValue());
        }
   
    }
    
    public StringProperty getDayTimeSlot(String day) {

        int index = dayNames.indexOf(day);
        
        if (index < 0 || index >= dayNames.size()) {
            return null;
        }
        
        return rowData[dayNames.indexOf(day)];
    }
    
    
    public StringProperty getDayTimeSlot(int column) {
        return rowData[column];
    }
    
    
    public static String formatTime(int hour, int minute) {
        
        String type;
        int printHour;
        
        if (hour < 12) {
            type = "am";   
            printHour = hour;
        }
        else {
            printHour = hour - 12;
            type = "pm";
        }
        
        if (printHour == 0) {
            printHour = 12;
        }
        
        return printHour + ":" + String.format("%02d", minute) + " " + type;
    }
    
    
    public static int[] getTimeValue(String time) {
        boolean isNoon = time.contains("pm");
        int hour = Integer.parseInt(time.substring(0, time.indexOf(":")));
        int minute = Integer.parseInt(time.substring(time.indexOf(":") + 1, time.indexOf(" ")));

        
        hour = hour % 12;
        
        
        if (isNoon) {
            int[] value = {hour + 12, minute};
            return value;
        }
        
        
        int[] value = {hour, minute};

        return value;
     }
    
    
    public StringProperty[] getRowData() {
        return rowData;
    }
    
    
    public String getTableStartTime() {
        return "\n" + startTime.get() + "\n \n";
    }
    
    
    public String getTableEndTime() {
            return "\n" + endTime.get() + "\n \n";
    }
    
    public String getStartTime() {
        
        return startTime.get();
    }
    
    public StringProperty getStartTimeProperty() {
        return startTime;
    }
    
    
    public String getEndTime() {
        return endTime.get();
    }
    
    public List getDayNames() {
        return dayNames;
    }
    
    
    @Override
    public int compareTo(OHTimeSlot o) {
        if (startHour > o.startHour) 
            return 1;
        
        else if (startHour < o.startHour)
            return -1;
        else if (startMinute > o.startMinute)
            return 1;
        else if (startMinute < o.startMinute)
            return -1;
        
        return 0;          
    }
    
    public int getStartHour() {
        return startHour;
    }
    
    public int getStartMinute() {
        return startMinute;
    }
    
    public int getEndHour() {
        return endHour;
    }
    public void addRowData(String name,int i){
        rowData[i].setValue(rowData[i].get()+"\n"+name);
    }
    public void removeRowData(String name,int i){
        if(rowData[i].get().contains(name)){
            rowData[i].setValue(rowData[i].get().replace(name, ""));
        }
    }
}
