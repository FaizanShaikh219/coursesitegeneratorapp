package csg.data;
/**
 *
 * @author Faizan Shaikh
 */

public enum TAType {
    All,
    Graduate,
    Undergraduate
}