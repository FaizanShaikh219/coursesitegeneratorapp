package csg.data;
/**
 *
 * @author Faizan Shaikh
 */
import csg.CourseSiteGeneratorApp;
import csg.file.CourseSiteGeneratorFiles;
import csg.workspace.CourseSiteGeneratorWorkspace;
import csg.workspace.SiteWorkspace;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;


public class SiteData {
    private ObservableList<String> subList;
    private ObservableList<String> numList;
    private String subject;
    private String semester;
    private String num;
    private String year;
    private List majorCodes;
    private StringProperty title;
    private StringProperty insName;
    private StringProperty insHome;
    private StringProperty insEmail;
    private StringProperty insRoom;
    private StringProperty insOH;
    private ObservableList<String> styleSheets;
    private String faviconPath;
    private String leftFooterPath;
    private String rightFooterPath;
    private String styleSheetName;
    private String exportDir;
    private String dir;
    public ArrayList subL;
    public ArrayList numL;
    public CourseSiteGeneratorApp app;
    private String navbarPath;


    public SiteData(CourseSiteGeneratorApp app) {
        this.app=app;
        styleSheets = FXCollections.observableArrayList();
        title = new SimpleStringProperty();
        insName = new SimpleStringProperty();
        insHome = new SimpleStringProperty();
        insEmail = new SimpleStringProperty();
        insRoom = new SimpleStringProperty();
        insOH= new SimpleStringProperty();
        num="";
        year="";
        subL=new ArrayList();
        numL=new ArrayList();
        subject = new String();
        semester = new String();
        faviconPath = new String();
        navbarPath = new String();
        leftFooterPath = new String();
        rightFooterPath = new String();
        styleSheetName = new String();
        exportDir = new String();
        dir = new String();
        majorCodes=new ArrayList();
        
        //PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        
        
        loadStyleSheets();

    }

    
    public String getSubject() {
        return subject;
    }

    
    public void setSubject(String subject) {
        this.subject = subject;
    }
    public ArrayList getSubL() {
        return subL;
    }
    public ArrayList getNumL() {
        return numL;
    }

    
    public void setSubL(ArrayList subject) {
        SiteWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        subList=workspace.getSubjectBox().getItems();
        this.subL = subject;
        for(int i=0;i<subL.size();i++){
            if(!subList.contains((subL.get(i)).toString())){
                subList.add((subL.get(i)).toString());
            }
        }
    }
    /*public void setNumL(ArrayList number) {
        SiteWorkspace workspace = ((CourseSiteGeneratorWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        numList=workspace.getNumberBox().getItems();
        this.numL = number;
        for(int i=0;i<numL.size();i++){
            if(!numList.contains((numL.get(i)).toString())){
                numList.add((numL.get(i)).toString());
            }
        }
    }*/
    public List getML() {
        return majorCodes;
    }

    
    public void setML(List mc) {
        this.majorCodes = mc;
    }

    
    public String getSemester() {
        return semester;
    }

    
    public void setSemester(String semester) {
        this.semester = semester;
    }

    
    public String getNumber() {
        return num;
    }

    
    public void setNumber(String courseNumber) {
        this.num = courseNumber;
    }

    
    public String getYear() {
        return year;
    }

   
    public void setYear(String year) {
        this.year = year;
    }

    
    public String getTitle() {
        return title.getValueSafe();
    }

    public StringProperty getTitleProperty() {
        return title;
    }

    
    public void setTitle(String title) {
        this.title.setValue(title);
    }

    
    public String getInsName() {
        return insName.getValue();
    }

    
    public StringProperty getInsNameProperty() {
        return insName;
    }

    
    public void setInsName(String instructor) {

        this.insName.setValue(instructor);
    }

    
    public String getInsHome() {
        return insHome.getValue();
    }

    
    public StringProperty getInsHomeProperty() {
        return insHome;
    }

    
    public void setInsHome(String site) {
        this.insHome.setValue(site);
    }
    public String getInsEmail() {
        return insEmail.getValue();
    }

    
    public StringProperty getInsEmailProperty() {
        return insEmail;
    }

    
    public void setInsEmail(String site) {
        this.insEmail.setValue(site);
    }
    public String getInsRoom() {
        return insRoom.getValue();
    }

    
    public StringProperty getInszRoomProperty() {
        return insRoom;
    }

    
    public void setInsRoom(String site) {
        this.insRoom.setValue(site);
    }
    public String getInsOH() {
        return insOH.getValue();
    }

    
    public StringProperty getInsOHProperty() {
        return insOH;
    }

    
    public void setInsOH(String site) {
        this.insOH.setValue(site);
        
    }

    
    public String getFaviconPath() {
        return faviconPath;
    }

    public String getFavicon() {
        return new File(faviconPath).getName();
    }
    
    public void setFaviconPath(String favPath) {
        this.faviconPath = favPath;
    }
public String getNavbarPath() {
        return navbarPath;
    }

    public String getNavbar() {
        return new File(navbarPath).getName();
    }
    
    public void setNavbarPath(String navPath) {
        this.navbarPath = navPath;
    }
    
    public String getLeftFooterPath() {
        return leftFooterPath;
    }
    
    public String getLeftFooter() {
        return new File(leftFooterPath).getName();
    }
    
    public void setLeftFooterPath(String leftFooterPath) {
        this.leftFooterPath = leftFooterPath;
    }

    
    public String getRightFooterPath() {
        return rightFooterPath;
    }

    
    public String getRightFooter() {
        return new File(rightFooterPath).getName();
    }
    
    public void setRightFooterPath(String rightFooterPath) {
        this.rightFooterPath = rightFooterPath;
    }

    
    public String getStyleSheetName() {
        return styleSheetName;
    }

    
    public void setStyleSheetName(String styleSheetPath) {
        this.styleSheetName = styleSheetPath;
    }

    public String getStyleSheetPath() {
        return System.getProperty("user.dir") + CourseSiteGeneratorFiles.DEFAULT_WORK_STYLE_DIRECTORY + this.styleSheetName;
    }
    
    public void setExportDir(String exportDir) {
        this.exportDir = exportDir;
    }
    public String getExportDir() {
        return exportDir;
    }

   
    public void setTemplateDir(String templateDir) {
        this.dir = templateDir;
    }
    
    
    
    
    public void loadStyleSheets() {
        
        File styleSheetDirectory = new File(getStyleSheetPath());
        List<String> files = CourseSiteGeneratorFiles.getFilesInDirectory(styleSheetDirectory);
        
        for (String fileName : files) {
            
            if (fileName.substring(fileName.indexOf('.'))
                    .equals(CourseSiteGeneratorFiles.DEFAULT_STYLE_EXTENSION)) {
                styleSheets.add(fileName);
            }

        }  
    }

    
    public String getTemplateDirectory() {
        if (dir == null || dir.equals("")) {
 
            return System.getProperty("user.dir") + CourseSiteGeneratorFiles.DEFAULT_TEMPLATE_DIRECTORY;
        }
        return dir;

    }

    
    void resetData() {
        //sitePages.clear();
        title.setValue("");
        insName.setValue("");
        insEmail.setValue("");
        insRoom.setValue("");
        insHome.setValue("");
        dir = new String();
        subject = new String();
        num = new String();
        this.setNumber(num);
        semester = new String();
        faviconPath = new String();
        navbarPath= new String();
        leftFooterPath = new String();
        rightFooterPath = new String();
        styleSheetName = new String();
        exportDir = new String();

        //num = 0;
        year = "";
        
        //loadSitePages();
    }

    public ObservableList getStylesheets() {
        return styleSheets;
    }

    public String getExportDirectory() {

        return exportDir;
    }

    public boolean isLegalEmail(String text) {
        Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(
                "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(text);
        if (matcher.find()) {
            
            return true;
        }
        else return false;
    }


}
