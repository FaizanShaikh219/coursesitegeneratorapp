package csg.data;

import csg.CourseSiteGeneratorApp;
import csg.workspace.CourseSiteGeneratorWorkspace;
import csg.workspace.MeetingTimesWorkspace;
import djf.ui.AppGUI;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;

/**
 *
 * @author Faizan Shaikh
 */
public class MeetingTimesData {
    private ObservableList<Lectures> lectures;
    private ObservableList<Recitations> recitations;
    private ObservableList<Labs> labs;
    CourseSiteGeneratorApp app;
    CourseSiteGeneratorWorkspace workspace;
    MeetingTimesWorkspace mtWorkspace;
    MeetingTimesData(CourseSiteGeneratorApp initApp) {
        app=initApp;
        //AppGUI gui = app.getGUI();
        
        //TableView<TeachingAssistant> taTable = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
       lectures = FXCollections.observableArrayList();
       recitations = FXCollections.observableArrayList();
       labs = FXCollections.observableArrayList();
    }
    public ObservableList<Lectures> getLectures() {
        return lectures;
    }
    public ObservableList<Recitations> getRecitations() {
        return recitations;
    }
    public ObservableList<Labs> getLabs() {
        return labs;
    }
    

    void resetData() {
        lectures.clear();
        recitations.clear();
        labs.clear();
    }

    public void addLec(Lectures lec) {
        workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        mtWorkspace = workspace.getMeetingTimesWorkspace();
        lectures=mtWorkspace.getLectureTable().getItems();
        lectures.add(lec);
    }

    public void removeLec(Lectures lec) {
        workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        mtWorkspace = workspace.getMeetingTimesWorkspace();
        lectures=mtWorkspace.getLectureTable().getItems();
        lectures.remove(lec);
    }
    public void addRec(Recitations rec) {
        workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        mtWorkspace = workspace.getMeetingTimesWorkspace();
        recitations=mtWorkspace.getRecitationTable().getItems();
        recitations.add(rec);
    }

    public void removeRec(Recitations rec) {
        workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        mtWorkspace = workspace.getMeetingTimesWorkspace();
        recitations=mtWorkspace.getRecitationTable().getItems();
        recitations.remove(rec);
    }
    public void addLab(Labs lab) {
        workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        mtWorkspace = workspace.getMeetingTimesWorkspace();
        labs=mtWorkspace.getLabTable().getItems();
        labs.add(lab);
    }

    public void removeLab(Labs lab) {
        workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        mtWorkspace = workspace.getMeetingTimesWorkspace();
        labs=mtWorkspace.getLabTable().getItems();
        labs.remove(lab);
    }

    public void editLec(TableColumn.CellEditEvent<Lectures, String> t,String newValue, String type) {
        //workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        //mtWorkspace = workspace.getMeetingTimesWorkspace();
        if(type.equals("section")){
        ((Lectures) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setSection(newValue);
        t.getTableView().refresh();
        }
        else if(type.equals("days")){
            ((Lectures) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setDays(newValue);
        t.getTableView().refresh();
        }
        else if(type.equals("time")){
            ((Lectures) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setTime(newValue);
        t.getTableView().refresh();
        }
        else if(type.equals("room")){
            ((Lectures) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setRoom(newValue);
        t.getTableView().refresh();
        }
        
    }
    public void undoeditLec(TableColumn.CellEditEvent<Lectures, String> t,String oldValue, String type) {
        //workspace = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        //mtWorkspace = workspace.getMeetingTimesWorkspace();
        if(type.equals("section")){
            ((Lectures) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setSection(oldValue);
        }
        else if(type.equals("days")){
            ((Lectures) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setDays(oldValue);
        t.getTableView().refresh();
        }
        else if(type.equals("time")){
            ((Lectures) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setTime(oldValue);
        t.getTableView().refresh();
        }
        else if(type.equals("room")){
            ((Lectures) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setRoom(oldValue);
        t.getTableView().refresh();
        }
        t.getTableView().refresh();
    }

    public void editRec(TableColumn.CellEditEvent<Recitations, String> t, String newValue, String type) {
        if(type.equals("section")){
        ((Recitations) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setSection(newValue);
        
        }
        else if(type.equals("days")){
            ((Recitations) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setDaysandtime(newValue);
        
        }
        else if(type.equals("ta1")){
            ((Recitations) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setTa1(newValue);
        
        }
        else if(type.equals("ta2")){
            ((Recitations) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setTa2(newValue);
        
        }
        else if(type.equals("room")){
            ((Recitations) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setRoom(newValue);
        
        }
        t.getTableView().refresh();
    }

    public void undoeditRec(TableColumn.CellEditEvent<Recitations, String> t, String oldValue, String type) {
        if(type.equals("section")){
        ((Recitations) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setSection(oldValue);
        
        }
        else if(type.equals("days")){
            ((Recitations) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setDaysandtime(oldValue);
        
        }
        else if(type.equals("ta1")){
            ((Recitations) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setTa1(oldValue);
        
        }
        else if(type.equals("ta2")){
            ((Recitations) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setTa2(oldValue);
        
        }
        else if(type.equals("room")){
            ((Recitations) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setRoom(oldValue);
        
        }
        t.getTableView().refresh();
    }

    public void editLab(TableColumn.CellEditEvent<Labs, String> t, String newValue, String type) {
        if(type.equals("section")){
        ((Labs) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setSection(newValue);
        
        }
        else if(type.equals("days")){
            ((Labs) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setDaysandtime(newValue);
        
        }
        else if(type.equals("ta1")){
            ((Labs) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setTa1(newValue);
        
        }
        else if(type.equals("ta2")){
            ((Labs) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setTa2(newValue);
        
        }
        else if(type.equals("room")){
            ((Labs) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setRoom(newValue);
        
        }
        t.getTableView().refresh();
    }

    public void undoeditLab(TableColumn.CellEditEvent<Labs, String> t, String oldValue, String type) {
        if(type.equals("section")){
        ((Labs) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setSection(oldValue);
        
        }
        else if(type.equals("days")){
            ((Labs) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setDaysandtime(oldValue);
        
        }
        else if(type.equals("ta1")){
            ((Labs) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setTa1(oldValue);
        
        }
        else if(type.equals("ta2")){
            ((Labs) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setTa2(oldValue);
        
        }
        else if(type.equals("room")){
            ((Labs) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setRoom(oldValue);
        
        }
        t.getTableView().refresh();
    }

    
    
}
