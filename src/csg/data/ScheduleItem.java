package csg.data;
/**
 *
 * @author Faizan Shaikh
 */
import java.time.LocalDate;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class ScheduleItem {
    
    private StringProperty type;
    private LocalDate date;
    private StringProperty title;
    private StringProperty topic;
    //private StringProperty time;
    private StringProperty link;
    //private StringProperty criteria;
    
    public ScheduleItem(String type, LocalDate date, String title, String topic, String link) {
       this.type = new SimpleStringProperty(type);
       this.date = date;
       this.title = new SimpleStringProperty(title);
       this.topic = new SimpleStringProperty(topic);
       //this.time = new SimpleStringProperty(time);
       this.link = new SimpleStringProperty(link);
       //this.criteria = new SimpleStringProperty(criteria);
    }

    
    public String getType() {
        return type.getValue();
    }

    
    public void setType(StringProperty type) {
        this.type = type;
    }

    
    public LocalDate getDate() {
        return date;
    }

    
    public void setDate(LocalDate date) {
        this.date = date;
    }

    
    public String getTitle() {
        return title.getValueSafe();
    }

    
    public void setTitle(StringProperty title) {
        this.title = title;
    }

    
    public String getTopic() {
        return topic.getValueSafe();
    }

    
    public void setTopic(StringProperty topic) {
        this.topic = topic;
    }

    
    

    
    public String getLink() {
        return link.getValueSafe();
    }

    
    public void setLink(String link) {
        this.link.setValue(link);
    }

    
    
    
    
    
}
