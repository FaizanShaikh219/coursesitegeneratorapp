
package csg.data;
/**
 *
 * @author Faizan Shaikh
 */
import csg.CourseSiteGeneratorApp;
import djf.components.AppDataComponent;



public class CourseSiteGeneratorData implements AppDataComponent {
    
    private CourseSiteGeneratorApp app;
    private SiteData siteData;
    private SyllabusData syllabusData;
    private TAData taData;
    private ScheduleData scheduleData;
    private MeetingTimesData mtData;
    
    
        
    public CourseSiteGeneratorData(CourseSiteGeneratorApp app) {
        this.app = app;
        
        siteData = new SiteData(app);
        syllabusData = new SyllabusData();
        taData = new TAData(app);
        scheduleData = new ScheduleData();
        mtData =new MeetingTimesData(app);
    }

    
    @Override
    public void resetData() {
        taData.resetData();
        syllabusData.resetData();
        siteData.resetData();
        scheduleData.resetData();
        mtData.resetData();
     
    }
    
    public SiteData getSiteData() {
        return siteData;
    }
    public SyllabusData getSyllabusData() {
        return syllabusData;
    }
    
    public TAData getTAData() {
        return taData;
    }
    public ScheduleData getScheduleData() {
        return scheduleData;
    }
    public MeetingTimesData getMeetingTimesData() {
        return mtData;
    }

    
    public void setSiteData(SiteData siteData) {
        this.siteData = siteData;
    }
    public void setSyllabusData(SyllabusData sData) {
        this.syllabusData = sData;
    }

    
    public void setTaData(TAData taData) {
        this.taData = taData;
    }

    public void setScheduleData(ScheduleData scheduleData) {
        this.scheduleData = scheduleData;
    }
    public void setMeetingTimesData(MeetingTimesData meetingData) {
        this.mtData = meetingData;
    }
}
