package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Faizan Shaikh
 */
public class Lectures <E extends Comparable<E>> implements Comparable<E> {
    private final StringProperty section;
    private final StringProperty days;
    private final StringProperty time;
    private final StringProperty room;
    public Lectures() {
        section = new SimpleStringProperty("?");
        days = new SimpleStringProperty("?");
        time = new SimpleStringProperty("?");
        room=new SimpleStringProperty("?");
    }
    public Lectures(String sec,String day,String time,String room) {
        section = new SimpleStringProperty(sec.toString());
        days = new SimpleStringProperty(day.toString());
        this.time = new SimpleStringProperty(time.toString());
        this.room=new SimpleStringProperty(room.toString());
    }
    public String getSection() {
        return section.get();
    }
    
    public void setSection(String sec) {
        section.setValue(sec);
    }
    public String getDays() {
        return days.get();
    }
    
    public void setDays(String da) {
        days.setValue(da);
    }
    public String getTime() {
        return time.get();
    }
    
    public void setTime(String slot) {
        time.setValue(slot);
    }
    public String getRoom() {
        return room.get();
    }
    
    public void setRoom(String roo) {
        room.setValue(roo);
    }
    @Override
    public int compareTo(E o) {
        return ((getSection())).compareTo((((Lectures)o).getSection()));
    }
    
}
