package csg.data;
/**
 *
 * @author Faizan Shaikh
 */
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableBooleanValue;


public class TeachingAssistant<E extends Comparable<E>> implements Comparable<E>  {
    
    private final StringProperty name;
    private final StringProperty email;
    private final StringProperty type;
    private final StringProperty slots;

    
    public TeachingAssistant(String initName, String initEmail, String type) {
        name = new SimpleStringProperty(initName);
        email = new SimpleStringProperty(initEmail);
        this.type = new SimpleStringProperty(type);
        slots=new SimpleStringProperty("0");
    }

    // ACCESSORS AND MUTATORS FOR THE PROPERTIES

    public String getName() {
        return name.get();
    }
    public String getSlots() {
        return slots.get();
    }
    
    public void setSlots(String initSlots) {
        slots.setValue(initSlots);
    }
    
    public StringProperty slotsProperty() {
        return slots;
    }
   
    public void setName(String initName) {
        name.set(initName);
    }
    
   
    public String getEmail() {
        return email.get();
    }
    
    
    public void setEmail(String initEmail) {
        email.set(initEmail);
    }
    
    
    public String getType() {
        return type.get();
    }
   
    public void setType(String isUnderGrad) {
        this.type.set(isUnderGrad);
    }
 


    @Override
    public int compareTo(E otherTA) {
        return getName().compareTo(((TeachingAssistant)otherTA).getName());
        
    }
    
    @Override
    public String toString() {
        return name.getValue() + "<" + email.getValue() + ">";
    }
}