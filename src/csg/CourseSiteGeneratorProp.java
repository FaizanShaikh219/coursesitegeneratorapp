package csg;

/**
 *
 * @author Faizan Shaikh
 */
public enum CourseSiteGeneratorProp {
    COURSE_TAB_TEXT,
    COURSE_INFO_TITLE,
    COURSE_SUBJECT_TEXT,
    COURSE_NUMBER_TEXT,
    COURSE_TITLE_TEXT,
    COURSE_YEAR_TEXT,
    COURSE_SEMESTER_TEXT,
    COURSE_INSTRUCTOR_NAME,
    COURSE_INSTRUCTOR_HOME,
    COURSE_INSTRUCTOR_LABEL,
    COURSE_INSTRUCTOR_ROOM,
    COURSE_INSTRUCTOR_EMAIL,
    COURSE_INSTRUCTOR_OH,
    COURSE_SITE_EXPORT_TEXT,
    COURSE_TEMPLATE_TITLE,
    COURSE_TEMPLATE_MESSAGE,
    COURSE_SELECT_TEMPLATE_BUTTON,
    COURSE_SITE_PAGES_TEXT,
    COURSE_STYLE_TITLE,
    COURSE_FAVICON_TEXT,
    COURSE_NAVBAR_TEXT,
    COURSE_LEFT_FOOTER_TEXT,
    COURSE_RIGHT_FOOTER_TEXT,
    COURSE_STYLESHEET_TEXT,
    COURSE_STYLE_SHEET_INFO_TEXT,
    COURSE_HOME_TEXT,
    COURSE_SYLLABUS_TEXT,
    COURSE_SCHEDULE_TEXT,
    COURSE_HW_TEXT,
    MISSING_TA_NAME_TITLE,
    MISSING_TA_NAME_MESSAGE,
    MISSING_TA_EMAIL_TITLE,
    MISSING_TA_EMAIL_MESSAGE,
    INVALID_TA_EMAIL_TITLE,
    INVALID_TA_EMAIL_MESSAGE,
    TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE,
    TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE,
    TA_TAB_TEXT,
    RECITATION_TAB_TEXT,
    SCHEDULE_TAB_TEXT,
    PROJECT_TAB_TEXT,
    CHANGE_BUTTON,
    TA_LIST_TITLE,
    TA_IS_UNDERGRAD_TEXT,
    TA_NAME_TEXT,
    TA_EMAIL_TEXT,
    TA_ADD_BUTTON_TEXT,
    TA_CLEAR_BUTTON_TEXT,
    TA_OFFICE_HOURS_TEXT,
    TA_START_TIME_TEXT,
    TA_END_TIME_TEXT,
    SCHEDULE_TITLE_TEXT,
    TA_TYPE_COLUMN_TEXT,
    TA_NAME_COLUMN_TEXT,
    TA_EMAIL_COLUMN_TEXT,
    TA_SLOT_COLUMN_TEXT,
    SCHEDULE_BOUNDARIES_TEXT,
    SCHEDULE_STARTING_MONDAY_TEXT,
    SCHEDULE_ENDING_FRIDAY_TEXT,
    SCHEDULE_ITEMS_TITLE,
    SCHEDULE_ADD_TITLE,
    SCHEDULE_TYPE_TEXT,
    SCHEDULE_DATE_TEXT,
    SCHEDULE_TIME_TEXT,
    SCHEDULE_ITEM_TITLE_TEXT,
    SCHEDULE_TOPIC_TEXT,
    SCHEDULE_LINK_TEXT,
    SCHEDULE_CRITERIA_TEXT,
    SCHEDULE_TYPE_COLUMN_TEXT,
    SCHEDULE_DATE_COLUMN_TEXT,
    SCHEDULE_TITLE_COLUMN_TEXT,
    SCHEDULE_TOPIC_COLUMN_TEXT,
    ADD_UPDATE_BUTTON_TEXT,
    CLEAR_BUTTON_TEXT,
    REMOVE_BUTTON_TEXT,
    COURSE_EMPTY_TABLE_TEXT,
    TA_EMPTY_TABLE_TEXT,
    SCHEDULE_EMPTY_TABLE_TEXT,
    RECITATION_EMPTY_TABLE_TEXT,
    RECITATION_TITLE,
    LECTURE_TITLE,
    LAB_TITLE,
    RECITATION_SECTION_COLUMN_TEXT,
    RECITATION_DAY_COLUMN_TEXT,
    RECITATION_TIME_COLUMN_TEXT,
    RECITATION_INSTRUCTOR_COLUMN_TEXT,
    RECITATION_DATE_COLUMN_TEXT,
    RECITATION_LOCATION_COLUMN_TEXT,
    RECITATION_TA1_COLUMN_TEXT,
    RECITATION_TA2_COLUMN_TEXT,
    RECITATION_ADD_TITLE,
    RECITATION_SECTION_TEXT,
    RECITATION_INSTRUCTOR_TEXT,
    RECITATION_DATE_TEXT,
    RECITATION_LOCATION_TEXT,
    RECITATION_SUPERVISING_TA_TEXT,
    PROJECT_TITLE,
    PROJECT_TEAMS_TEXT,
    PROJECT_NAME_COLUMN_TEXT,
    PROJECT_COLOR_COLUMN_TEXT,
    PROJECT_TEXT_COLUMN_TEXT,
    PROJECT_LINK_COLUMN_TEXT,
    PROJECT_ADD_TEAM,
    PROJECT_TEAM_NAME_TEXT,
    PROJECT_TEAM_COLOR_TEXT,
    PROJECT_TEAM_TEXTCOLOR_TEXT,
    PROJECT_TEAM_LINK_TEXT,
    PROJECT_STUDENTS_TITLE,
    PROJECT_STUDENT_FIRSTNAME_COLUMN_TEXT,
    PROJECT_STUDENT_LASTNAME_COLUMN_TEXT,
    PROJECT_STUDENT_TEAM_COLUMN_TEXT,
    PROJECT_STUDENT_ROLE_COLUMN_TEXT,
    PROJECT_STUDENT_FIRSTNAME_TEXT,
    PROJECT_STUDENT_LASTNAME_TEXT,
    PROJECT_STUDENT_TEAM_TEXT,
    PROJECT_STUDENT_ROLE_TEXT,
    OFFICE_HOURS_TABLE_HEADERS,
    DAYS_OF_WEEK,
    INVALID_TIME_TITLE,
    INVALID_TIME_MESSAGE,
    CONFIRM_TIME_CHANGE_TITLE,
    CONFIRM_TIME_CHANGE_MESSAGE,
    COURSE_SEMESTERS,
    COURSE_MAJOR_CODES,
    EXPORT_FILE_TYPES,
    SCHEDULE_ITEM_PROPERTY_NAMES,
    SCHEDULE_ITEM_HOMEWORK,
    SCHEDULE_ITEM_RECITATION,
    SCHEDULE_ITEM_LECTURE,
    SCHEDULE_ITEM_REFERENCE,
    SCHEDULE_ITEM_HOLIDAY,
    
     
    OH_LEFT_PANE,
    OH_TAS_HEADER_PANE,
    OH_TAS_HEADER_LABEL,
    OH_GRAD_UNDERGRAD_TAS_PANE,
    OH_ALL_RADIO_BUTTON,
    OH_GRAD_RADIO_BUTTON,
    OH_UNDERGRAD_RADIO_BUTTON,
    OH_TAS_HEADER_TEXT_FIELD,
    OH_TAS_TABLE_VIEW,
    OH_NAME_TABLE_COLUMN,
    OH_EMAIL_TABLE_COLUMN,
    OH_SLOTS_TABLE_COLUMN,
    OH_TYPE_TABLE_COLUMN,

    OH_ADD_TA_PANE,
    OH_NAME_TEXT_FIELD,
    OH_EMAIL_TEXT_FIELD,
    OH_ADD_TA_BUTTON,

    OH_RIGHT_PANE,
    OH_OFFICE_HOURS_HEADER_PANE,
    OH_OFFICE_HOURS_HEADER_LABEL,
    OH_OFFICE_HOURS_TABLE_VIEW,
    OH_START_TIME_TABLE_COLUMN,
    OH_END_TIME_TABLE_COLUMN,
    OH_MONDAY_TABLE_COLUMN,
    OH_TUESDAY_TABLE_COLUMN,
    OH_WEDNESDAY_TABLE_COLUMN,
    OH_THURSDAY_TABLE_COLUMN,
    OH_FRIDAY_TABLE_COLUMN,
    OH_DAYS_OF_WEEK,
    OH_FOOLPROOF_SETTINGS,
    
    // FOR THE EDIT DIALOG
    OH_TA_EDIT_DIALOG,
    OH_TA_DIALOG_GRID_PANE,
    OH_TA_DIALOG_HEADER_LABEL, 
    OH_TA_DIALOG_NAME_LABEL,
    OH_TA_DIALOG_NAME_TEXT_FIELD,
    OH_TA_DIALOG_EMAIL_LABEL,
    OH_TA_DIALOG_EMAIL_TEXT_FIELD,
    OH_TA_DIALOG_TYPE_LABEL,
    OH_TA_DIALOG_TYPE_BOX,
    OH_TA_DIALOG_GRAD_RADIO_BUTTON,
    OH_TA_DIALOG_UNDERGRAD_RADIO_BUTTON,
    OH_TA_DIALOG_OK_BOX,
    OH_TA_DIALOG_OK_BUTTON, 
    OH_TA_DIALOG_CANCEL_BUTTON, 
    
    MISSING_SCHEDULE_TYPE_TITLE,
    MISSING_SCHEDULE_TYPE_MESSAGE,
    MISSING_SCHEDULE_DATE_TITLE,
    MISSING_SCHEDULE_DATE_MESSAGE,
    MISSING_SCHEDULE_TIME_TITLE,
    MISSING_SCHEDULE_TIME_MESSAGE,
    MISSING_SCHEDULE_TOPIC_TITLE,
    MISSING_SCHEDULE_TOPIC_MESSAGE,
    MISSING_SCHEDULE_CRITERIA_TITLE,
    MISSING_SCHEDULE_CRITERIA_MESSAGE,
    
    MISSING_RECITATION_SECTION_TITLE,
    MISSING_RECITATION_SECTION_MESSAGE,
    MISSING_RECITATION_INSTRUCTOR_TITLE,
    MISSING_RECITATION_INSTRUCTOR_MESSAGE,
    MISSING_RECITATION_DATETIME_TITLE,
    MISSING_RECITATION_DATETIME_MESSAGE,
    MISSING_RECITATION_LOCATION_TITLE,
    MISSING_RECITATION_LOCATION_MESSAGE,
    MISSING_RECITATION_TA_TITLE,
    MISSING_RECITATION_TA_MESSAGE,
    MISSING_STUDENT_FIRSTNAME_TITLE,
    MISSING_STUDENT_FIRSTNAME_MESSAGE,
    MISSING_STUDENT_LASTNAME_TITLE,
    MISSING_STUDENT_LASTNAME_MESSAGE,
    MISSING_STUDENT_ROLE_TITLE,
    MISSING_STUDENT_ROLE_MESSAGE,
    MISSING_STUDENT_TEAM_TITLE,
    MISSING_STUDENT_TEAM_MESSAGE,
    MISSING_TEAM_NAME_TITLE,
    MISSING_TEAM_NAME_MESSAGE,
    MISSING_TEAM_COLOR_TITLE,
    MISSING_TEAM_COLOR_MESSAGE,
    MISSING_TEAM_TEXTCOLOR_TITLE,
    MISSING_TEAM_TEXTCOLOR_MESSAGE,
    MISSING_TEAM_LINK_TITLE,
    MISSING_TEAM_LINK_MESSAGE,
    
    CONFIRM_RECITATION_DELETE_TITLE,
    CONFIRM_RECITATION_DELETE_MESSAGE,
    CONFIRM_SCHEDULE_DELETE_TITLE,
    CONFIRM_SCHEDULE_DELETE_MESSAGE,
    CONFIRM_TEAM_DELETE_TITLE,
    CONFIRM_TEAM_DELETE_MESSAGE,
    CONFIRM_STUDENT_DELETE_TITLE,
    CONFIRM_STUDENT_DELETE_MESSAGE,
    CONFIRM_TA_DELETE_TITLE,
    CONFIRM_TA_DELETE_MESSAGE,
    MISSING_SCHEDULE_ITEMTITLE_TITLE,
    MISSING_SCHEDULE_ITEMTITLE_MESSAGE,
    MISSING_SCHEDULE_LINK_TITLE,
    MISSING_SCHEDULE_LINK_MESSAGE,
    RECITATION_NOT_UNIQUE_TITLE,
    RECITATION_NOT_UNIQUE_MESSAGE,
    STUDENT_INFORMATION_NOT_UNIQUE_TITLE,
    STUDENT_INFORMATION_NOT_UNIQUE_MESSAGE,
    TEAM_NAME_NOT_UNIQUE_TITLE,
    TEAM_NAME_NOT_UNIQUE_MESSAGE,
    SCHEDULE_ITEM_NOT_UNIQUE_TITLE,
    SCHEDULE_ITEM_NOT_UNIQUE_MESSAGE,
    EMPTY_FIELD_TEXT,
    SITE_PAGE_TITLES,
    SITE_PAGE_NAMES,
    SITE_PAGE_SCRIPTS,
    ABOUT_INFORMATION_MESSAGE,
    ABOUT_INFORMATION_TITLE,
    HELP_INFORMATION_MESSAGE,
    HELP_INFORMATION_TITLE,
    PROJECT_TEAMS_EMPTY_TABLE_TEXT,
    PROJECT_STUDENTS_EMPTY_TABLE_TEXT,
    INVALID_SCHEDULE_DATE_TITLE,
    INVALID_SCHEDULE_DATE_MESSAGE,
    SCHEDULE_OUTSIDE_OF_RANGE_TITLE,
SCHEDULE_OUTSIDE_OF_RANGE_MESSAGE, MEETING_TIMES_TITLE, 
DES_LABEL, SYLLABUS_TAB_TEXT, TOP_LABEL, PRE_LABEL, OUT_LABEL, BOOK_LABEL, GC_LABEL, GN_LABEL, AD_LABEL, SA_LABEL
    
    
}