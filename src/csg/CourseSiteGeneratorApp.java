package csg;
/**
 *
 * @author Faizan Shaikh
 */
import java.util.Locale;

import djf.AppTemplate;

import csg.data.CourseSiteGeneratorData;
import csg.file.CourseSiteGeneratorFiles;
import csg.workspace.CourseSiteGeneratorWorkspace;
import csg.style.CourseSiteGeneratorStyle;
import static javafx.application.Application.launch;


public class CourseSiteGeneratorApp extends AppTemplate {

    @Override
    public void buildAllApplicationComponents() {
        fileComponent = new CourseSiteGeneratorFiles(this);
        dataComponent = new CourseSiteGeneratorData(this);
        workspaceComponent = new CourseSiteGeneratorWorkspace(this);
        
        styleComponent = new CourseSiteGeneratorStyle(this);  
    }
    
    public static void main(String[] args) {
	Locale.setDefault(Locale.US);
	launch(args);
    }
    
}
