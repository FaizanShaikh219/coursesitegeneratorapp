package csg.file;
//import csg.data.
import csg.data.CourseSiteGeneratorData;
import csg.data.TAData;
import csg.data.TeachingAssistant;
import csg.test_bed.TestSave;
import java.io.IOException;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Max vonStrandtmann
 */
public class CourseSiteGeneratorFilesTest {
    
    /*static CourseSiteGeneratorFiles files;
    static CourseSiteGeneratorData data;   

    public CourseSiteGeneratorFilesTest() {
        
        
    }
    
    @BeforeClass
    public static void setUpClass() throws IOException {
        files = new CourseSiteGeneratorFiles(null);
        data = new CourseSiteGeneratorData(null); 
        data.getTAData().setHeaderList(TestSave.getHeaders());
        //data.getCourseData().setFileDefaults(TestSave.jsonFileTitles, TestSave.jsonFileNames, TestSave.jsonFileScripts);
        
        
        data.resetData();
        
        data.equals(data);
       
        files.loadData(data, TestSave.OUTPUT_FILE.getPath());
    }
    
    @AfterClass
    public static void tearDownClass() {
        files = null;
        data = null;
    }

    /**
     * Test the data loaded by CourseFiles
     */
    /*@Test
    public void testLoadCourseData() throws Exception {
        System.out.println("load course data");

        //Test that data loaded correctly
        SiteData courseData = data.getCourseData();
        assert(courseData.getCourseNumber() == TestSave.COURSE_NUMBER);
        assert(courseData.getBannerPath().equals(TestSave.COURSE_BANNER));
        assert(courseData.getSubject().equals(TestSave.COURSE_SUBJECT));
        assert(courseData.getTitle().equals(TestSave.COURSE_TITLE));
        assert(courseData.getInstructor().equals(TestSave.COURSE_INSTRUCTOR));
        assert(courseData.getSite().equals(TestSave.COURSE_SITE));
        assert(courseData.getSemester().equals(TestSave.COURSE_SEMESTER));
        assert(courseData.getExportDirectory().equals(TestSave.COURSE_EXPORT));
        assert(courseData.getRightFooterPath().equals(TestSave.COURSE_RIGHTFOOTER));
        assert(courseData.getLeftFooterPath().equals(TestSave.COURSE_LEFTFOOTER));


    }

    /**
     * Test the data loaded by TAFiles
     */
   /* @Test
    public void testLoadTAData() {
        System.out.println("load course data");
         
        TAData taData = data.getTAData();
        
        assert(taData.getStartHour() == TestSave.OFFICE_START);
        assert(taData.getEndHour() == TestSave.OFFICE_END);
        
        //test ta is valid.
        TeachingAssistant ta = taData.getTeachingAssistants().get(0);
        assert(ta.getName().equals(TestSave.TA_NAME));
        assert(ta.getEmail().equals(TestSave.TA_EMAIL));
        //assert(ta.getType() == TestSave.TA_ISUNDERGRAD);
        
        
        List<TAData.TimeSlotPosition> list = taData.getTAOfficeHours(TestSave.OFFICE_SLOT_NAME);
      
        //Office hour slot
        assert(list.size() == 1);
        
        //test time slot positions
        TAData.TimeSlotPosition slot = list.get(0);       
        assert(slot.getDay().equals(TestSave.OFFICE_SLOT_DAY));     
        assert(slot.getHour() == TestSave.OFFICE_SLOT_START_HOUR);
        assert(slot.getMinute() == TestSave.OFFICE_SLOT_START_MINUTE);
    }
    
    /**
     *  Test the data loaded by RecitationFiles
     */
    /*@Test
    public void testLoadRecitationData() {
        
    }    
    
    /**
     *  Test the data loaded by ScheduleFiles
     */
    /*@Test
    public void testLoadScheduleData() {
        
    }
    
    /**
     * Test the data loaded by ProjectFiles
     */
    /*@Test
    public void testLoadProjectData() {
        
    }*/
}
   